import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync, readFileSync } from 'fs';
const fs = require('fs');
const axios = require('axios');
const domino = require('domino');

// SET GLOBAL BROWSER VARS
const DIST_FOLDER = join(process.cwd(), 'dist/cms/browser');
const template = readFileSync(join(DIST_FOLDER, 'index.html')).toString();
const win = domino.createWindow(template);
global['localStorage'] = win.localStorage;
global['window'] = win;
global['document'] = win.document;
global['Document'] = win.document;
global['DOMTokenList'] = win.DOMTokenList;
global['Node'] = win.Node;
global['Text'] = win.Text;
global['HTMLElement'] = win.HTMLElement;
global['navigator'] = win.navigator;

class ExpressEngine {

  private server = express();
  constructor() {}

  app(): express.Express {
    const distFolder = join(process.cwd(), 'dist/cms/browser');
    const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';
  
    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    this.server.engine('html', ngExpressEngine({
      bootstrap: AppServerModule,
    }));
  
    this.server.set('view engine', 'html');
    this.server.set('views', distFolder);
    this.server.use(express.json());
    this.server.use(express.urlencoded({ extended: false }));
  
    // Example Express Rest API endpoints
    // server.get('/api/**', (req, res) => { });
    // Serve static files from /browser
    this.server.get('*.*', express.static(distFolder, {
      maxAge: '1y'
    }));

    this.server.route('/sitemap.xml')
      .get(async (req, res) => {
        const protocol = req.secure ? 'https://' : 'http://';
        // let hostname = req.hostname.indexOf('localhost') !== -1 ? `localhost:3000` : `ngcc-cms-api.herokuapp.com`;
        let hostname = `localhost:3000`;
        const url = `${protocol}${hostname}/sitemap`;
        let sitemap = await this.createSitemap(url);
        let sitemaps = '';
        sitemap.forEach(site => {
          sitemaps += `<sitemap>
                        <loc>https://ngcc-cms.herokuapp.com/${site.url}</loc>
                        <lastmod>${site.last_updated}</lastmod>
                      </sitemap>`;
        });
        let str = `<?xml version="1.0" encoding="UTF-8"?>
          <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            ${sitemaps}
          </sitemapindex>`;
        // Write sitemap
        await fs.writeFile('./src/sitemap.xml', str, (e, file) => {
          if (e) throw e;
        });
        await fs.readFile('./src/sitemap.xml', (err, data) => {
          res.write(data);
          return res.end();
        });
      });
  
    // All regular routes use the Universal engine
    this.server.get('*', (req, res) => {
      res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
    });

    return this.server;
  }

  run(): void {
    const port = process.env.PORT || 4000;

    // Start up the Node server
    const server = this.app();
    server.listen(port, () => {
      console.log(`Node Express server listening on http://localhost:${port}`);
    });
  }

  async createSitemap(url) {
    const sitemap = await axios.get(url);
    return sitemap.data;
  }
}

// The Express app is exported so that it can be used by serverless Functions.
// export function app(): express.Express {
// }

// function run(): void {
// }

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
const app = new ExpressEngine;
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  app.run();
}

export * from './src/main.server';
