import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlePageComponent } from './article-page.component';
import { RouterModule } from '@angular/router';
import { ArticlePageRouting } from './article-page.routing';
import { NotFoundModule } from '../not-found/not-found.module';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { PipesModule } from '../../pipes/pipes.module';
import { AuthorModule } from '../../components/author/author.module';
import { LoadersModule } from '../../components/loaders/loaders.module';
import { FooterModule } from '../../components/footer/footer.module';
import { ArticleModule } from '../../components/articles/article/article.module';
import { TooltipModule } from '../../components/tooltip/tooltip.module';
import { DirectivesModule } from '../../directives/directives.module';
import { FieldsModule } from '../../components/acf/fields/fields.module';
import { DropdownModule } from '../../components/form-control/dropdown/dropdown.module';
import { PageBuilderComponent } from '../../components/page-builder/page-builder.component';
import { PageBuilderModule } from '../../components/page-builder/page-builder.module';
import { AcfToolbarModule } from '../../components/acf/acf-toolbar/acf-toolbar.module';
import { AlertModule } from '../../components/alert/alert.module';
import { ModalModule } from '../../shared/components/modal/modal.module';



@NgModule({
  declarations: [ArticlePageComponent],
  imports: [
    CommonModule,
    RouterModule,
    ArticlePageRouting,
    NavbarModule,
    PipesModule,
    AuthorModule,
    LoadersModule,
    FooterModule,
    NotFoundModule,
    ArticleModule,
    TooltipModule,
    DirectivesModule,
    FieldsModule,
    DropdownModule,
    PageBuilderModule,
    AcfToolbarModule,
    AlertModule,
    ModalModule,
  ]
})
export class ArticlePageModule { }
