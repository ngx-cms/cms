import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlePageEditComponent } from './article-page-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ArticlePageEditComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlePageEditRoutingModule { }
