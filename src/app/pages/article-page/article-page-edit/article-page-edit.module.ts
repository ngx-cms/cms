import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlePageEditRoutingModule } from './article-page-edit-routing.module';
import { ArticlePageEditComponent } from './article-page-edit.component';
import { RouterModule } from '@angular/router';
import { NavbarModule } from '../../../components/navbar/navbar.module';
import { PipesModule } from '../../../pipes/pipes.module';
import { ModalModule } from '../../../shared/components/modal/modal.module';
import { AlertModule } from '../../../components/alert/alert.module';
import { AcfToolbarModule } from '../../../components/acf/acf-toolbar/acf-toolbar.module';
import { PageBuilderModule } from '../../../components/page-builder/page-builder.module';
import { DropdownModule } from '../../../components/form-control/dropdown/dropdown.module';
import { FieldsModule } from '../../../components/acf/fields/fields.module';
import { DirectivesModule } from '../../../directives/directives.module';
import { TooltipModule } from '../../../components/tooltip/tooltip.module';
import { ArticleModule } from '../../../components/articles/article/article.module';
import { NotFoundModule } from '../../not-found/not-found.module';
import { FooterModule } from '../../../components/footer/footer.module';
import { LoadersModule } from '../../../components/loaders/loaders.module';
import { AuthorModule } from '../../../components/author/author.module';


@NgModule({
  declarations: [
    ArticlePageEditComponent
  ],
  imports: [
    CommonModule,
    ArticlePageEditRoutingModule,
    CommonModule,
    RouterModule,
    NavbarModule,
    PipesModule,
    AuthorModule,
    LoadersModule,
    FooterModule,
    NotFoundModule,
    ArticleModule,
    TooltipModule,
    DirectivesModule,
    FieldsModule,
    DropdownModule,
    PageBuilderModule,
    AcfToolbarModule,
    AlertModule,
    ModalModule,
  ],
  exports: [ArticlePageEditComponent],
})
export class ArticlePageEditModule { }
