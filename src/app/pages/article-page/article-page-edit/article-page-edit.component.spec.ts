import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlePageEditComponent } from './article-page-edit.component';

describe('ArticlePageEditComponent', () => {
  let component: ArticlePageEditComponent;
  let fixture: ComponentFixture<ArticlePageEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlePageEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlePageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
