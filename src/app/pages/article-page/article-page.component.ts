import { Component, HostListener, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ApiResponse } from '../../../shared/interfaces/api';
import { DocumentRef } from '../../dom-abstraction/document.service';
import { WindowRef } from '../../dom-abstraction/window.service';
import { cloneDeep, hasKey, isEqual, keys } from '../../helpers/string.helper';
import { Alert } from '../../interfaces/alert/alert';
import { SiteOptions } from '../../interfaces/article-page.interface';
import { RouteState } from '../../interfaces/route-state';
import { User } from '../../interfaces/user/user';
import { AppStateService } from '../../services/app-state/app-state.service';
import { ArticleService } from '../../services/article/article.service';
import { BackendService } from '../../services/backend/backend.service';
import { RoutingService } from '../../services/routing/routing.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.scss'],
})
export class ArticlePageComponent implements OnInit, OnChanges, OnDestroy {

  public article: any = null;
  public latestArticles: any[] = [];
  public user: User;
  public siteOptions: SiteOptions;
  public showNavbar = true;
  public articleFound: boolean = false;
  public articleConfig: any;
  public $404: string;
  public dateTooltips: unknown = {};
  public searchFields = [
    {
      type: 'search',
      name: 'search',
      attributes: [{
        placeholder: 'Search',
      }],
      value: null,
    },
  ];
  public search: any[] = [];
  public acfTools = [];
  public layouts = [];
  public hiddenLayouts=  [];
  public isEdit = false;
  public alert: Alert = null;
  public canEdit = false;
  public categories = [];
  public articleCategories = [];
  public modal: any = {
    enabled: false,
  }

  private window: Window;
  private isDestroyed$ = new Subject<void>();
  private document: Document;
  private tempLayouts = [];
  private isBrowser: boolean;
  private ARTICLE_STATE_KEY = makeStateKey('article');

  @HostListener('window:scroll')
  onScroll() {
    this.navbarStatus();
  }

  constructor(
    private appStateService: AppStateService,
    private articleService: ArticleService,
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
    private routingService: RoutingService,
    private backendService: BackendService,
    private docRef: DocumentRef,
    private transferStateService: TransferState,
  ) {
    this.window = winRef.nativeWindow;
    this.document = docRef.nativeDocument;
    this.isBrowser = appStateService.isBrowser;
  }

  ngOnChanges(changes) {
  }

  ngOnInit(): void {
    this.fetch();
    console.log('Platform: ', this.isBrowser ? 'Browser' : 'Server');
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  // http://opensourcehacker.com/2011/12/01/calculate-aspect-ratio-conserving-resize-for-images-in-javascript/
  calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return { width: srcWidth * ratio, height: srcHeight * ratio };
  }

  getTransferState(key, val: any = {}) {
    return this.transferStateService.get(key, val);
  }

  fetchPageConfig(routeState: RouteState): Observable<any> {
    this.article = null;
    const { category, type } = routeState;
    // Check if article exists in server side
    const articleState = this.getTransferState(this.ARTICLE_STATE_KEY);
    let url = `articles/${category}/${type}`;
    if (!type) {
      url = `articles/pages/${category}`;
    }
    let article$ = this.articleService.fetchArticle(url)
    if (this.isBrowser && hasKey(articleState)) {
      article$ = of(articleState);
      this.transferStateService.remove(this.ARTICLE_STATE_KEY);
    }
    return combineLatest(
      of(routeState),
      this.appStateService.getSiteOptions(),
      article$,
    )
  }

  setArticlesThumbnail(article) {
    if (article?.related_articles) {
      article.related_articles = article.related_articles.map(art => {
        return {
          ...art,
          size: this.calculateAspectRatioFit(170, 150, 150, 140),
        }
      });
    }
    return article;
  }

  setAuthorDetails(article, routeState) {
    const { author } = article;
    if (article.author) {
      author.link = `${routeState.origin}/authors/${author.id}`;
    }
    
    return article;
  }

  setPageConfig(config) {
    let [ routeState, siteOptions, article ] = config;
    article = this.setArticlesThumbnail(article);
    article = this.setAuthorDetails(article, routeState);
    if (article.latest_articles) {
      this.categories = keys(article.categories).sort();
      this.articleCategories = article.categories;
      this.articleService.setPageTitle('Article not found')
      this.article = null;
      this.latestArticles = article.latest_articles;
      return of([routeState, siteOptions, {}]);
    }
    this.latestArticles = null;
    this.article = article;
    if (!this.isBrowser) {
      this.transferStateService.set(this.ARTICLE_STATE_KEY, this.article);
    }
    this.articleService.initSEO(siteOptions, routeState, article);
    return of([routeState, siteOptions, article]);
  }

  fetch() {
    // const routeState = this.appStateService.getRoutedState();
    this.appStateService.getRouteState()
      .pipe(
        mergeMap(routeState => (
          this.fetchPageConfig(routeState)
            .pipe(switchMap((...args) => (
              this.setPageConfig.apply(this, args)
            )))
        )),
        distinctUntilChanged(),
        takeUntil(this.isDestroyed$)
      )
    .subscribe(([routeState, siteOptions, article]: [RouteState, any, any, User]) => {
      const user = this.userAuthService.getUserSession();
      if (!this.user && hasKey(user)) {
        this.user = user;
      }
      this.siteOptions = siteOptions;
      this.articleConfig = siteOptions[article.type];
      this.$404 = this.articleConfig && this.articleConfig['404_message'];
      if (Array.isArray(article)) {
        this.latestArticles = article;
      }
      this.acfTools = this.getAcfTools();
      this.tempLayouts = cloneDeep(this.article?.layouts) || [];
      this.canEdit = user && (user.id === article?.author?.id || user?.owner?.id === article.author?.owner?.id);
      this.navbarStatus();
      if (this.isBrowser) {
        this.document.querySelector('html').scrollIntoView();
      }
    });
  }

  getAcfTools() {
    const compKeys = keys(this.article?.components) || [];
    if (compKeys?.length) {
      const hasAuthor = compKeys.some(key => key.includes('author'));
      const hasDate = compKeys.some(key => key.includes('date'));
      if (hasAuthor && hasDate) {
        compKeys.push('author_and_date');
      }
    }
    return compKeys;
  }

  updateUserState(changes) {
    this.user = changes;
  }

  navbarStatus() {
    this.showNavbar = this.window.scrollY <= 310
  }

  onSearch(field) {
    const search = field[0]?.value.toLowerCase();
    if (search) {
      this.articleService.search(search)
        .subscribe((res: any[]) => {
          this.search = res.map(article => {
            return {
              value: `/${article.url}`,
              label: `${article.title}${article.id === this.article.id ? '(Current Page)' : ''}`,
            }
          })
        })
    }
  }

  onSelect(search) {
    this.searchFields[0].value = search.label;
    this.routingService.goto(search.value);
    this.search = [];
  }

  isValidImage(url) {
    return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
  }
  
  testImage(url, callback, timeout = 500) {
    var timedOut = false, timer;
    var img = new Image();
    img.onerror = img.onabort = () => {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, "error");
        }
    };
    img.onload = () => {
        if (!timedOut) {
            clearTimeout(timer);
            callback(url, 'success');
        }
    };
    img.src = url;
    timer = () => {
        timedOut = true;
        // reset .src to invalid URL so it stops previous
        // loading, but doesn't trigger new load
        img.src = url;
        callback(url, 'success');
    }; 
}

  onToolSelect(acf) {
    const exists = this.article.layouts.some(layout => layout.name === acf);
    if (!exists) {
      let content = this.article.components[acf];
      this.testImage(content, (e, i) => {
        if (i === 'success') {
          const conf = this.window.confirm(`Convert ${acf} to image?`);
          const layout = this.article.layouts.findIndex(l => l.name === acf);
          this.article.layouts[layout].type = 'image';
          if (conf) {
            this.article.layouts[layout].is_converted = true;
            this.layoutToImg(this.article.layouts[layout]);
          }
        }
      });

      this.pushLayout(acf, content, 'content', false);
    }
  }

  layoutToImg(layout) {
    const { content, name } = layout;
    this.article.components[name] = this.articleService.createImage(content);
    layout.is_converted = true;
  }

  imgToText(layout) {
    const { content, name } = layout;
    this.article.components[name] = content;
    layout.is_converted = false;
  }

  pushLayout(name, content, type, is_converted) {
    console.log(`Adding layout ${name}`);
    
    this.article.layouts.push({
      name,
      position: this.layouts.length + 1,
      content,
      type,
      is_converted,
    });
    setTimeout(() => this.document.querySelector(`#${name}`).scrollIntoView());
  }

  saveLayout() {
    this.article.config.layouts = this.article.layouts;
    this.article.config.extract = true;
    this.backendService.post(`${this.article.type}/update`, this.article.config)
      .subscribe((res: ApiResponse) => {
        this.alert = {
          type: res.code ? 'success' : 'danger',
          text: res.message,
        };
      })
  }

  toggleEdit() {
    if (this.isEdit && !isEqual(this.tempLayouts, this.article.layouts)) {
      const conf = this.window.confirm(`All unsaved changes will be discarded`);
      if (!conf) {
        return;
      }
    }
    this.isEdit = !this.isEdit;
  }

  onModalFieldOutput(output) {
    console.log('output: ', output)
  }

  onToolbarAction(params) {
    const { action, layout, index } = params;
    switch (action) {
      case 'edit':
        this.modal = {
          enabled: true,
          layout,
          fields: [
            {
              type: 'textarea',
              label: 'Custom CSS',
              value: '',
              name: 'css'
            }
          ]
        };
        break;
      case 'remove':
        this.article.layouts.splice(index, 1);
        break;
      case 'minimize':
        this.hiddenLayouts.push(index);
        break;
      case 'restore':
        this.hiddenLayouts.splice(this.hiddenLayouts.findIndex(f => f === index), 1);
        break;
      case 'up':
        this.article.layouts.splice((index - 1), 0, this.article.layouts.splice(index, 1)[0]);
        break;
      case 'down':
        this.article.layouts.splice((index + 1), 0, this.article.layouts.splice(index, 1)[0]);
        break;

      default:
    }
  }

  onModalAction(action) {
    switch (action) {
      case 'close':
        this.modal = {};
        break;
      case 'submit':
        this.modal.fields.forEach(field => {
          this.modal.layout[field.name] = field.value;
        });
        break;
        
        
      default:
    }
  }

}
