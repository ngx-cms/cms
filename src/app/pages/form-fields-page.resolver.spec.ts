import { TestBed } from '@angular/core/testing';

import { FormFieldsPageResolver } from './form-fields-page.resolver';

describe('FormFieldsPageResolver', () => {
  let resolver: FormFieldsPageResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(FormFieldsPageResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
