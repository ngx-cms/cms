import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorPageComponent } from './author-page.component';



@NgModule({
  declarations: [AuthorPageComponent],
  imports: [
    CommonModule
  ]
})
export class AuthorPageModule { }
