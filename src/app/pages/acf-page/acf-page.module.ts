import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcfPageRoutingModule } from './acf-page-routing.module';
import { AcfPageComponent } from './acf-page.component';
import { RouterModule } from '@angular/router';
import { AcfModule } from '../../components/acf/acf.module';
import { ButtonModule } from '../../components/button/button.module';
import { TableModule } from '../../components/table/table.module';
import { BackModule } from '../../components/back/back.module';
import { AlertModule } from '../../components/alert/alert.module';
import { LoadersModule } from '../../components/loaders/loaders.module';
import { ModalModule } from '../../shared/components/modal/modal.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';


@NgModule({
  declarations: [AcfPageComponent],
  imports: [
    CommonModule,
    RouterModule,
    AcfPageRoutingModule,
    AcfModule,
    ButtonModule,
    TableModule,
    BackModule,
    AlertModule,
    LoadersModule,
    ModalModule,
    NgxJsonViewerModule
  ],
  exports: [AcfPageComponent],
})
export class AcfPageModule { }
