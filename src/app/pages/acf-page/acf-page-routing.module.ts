import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcfPageComponent } from './acf-page.component';

const routes: Routes = [
  {
    path: '',
    component: AcfPageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcfPageRoutingModule { }
