import { cloneDeep } from "../../helpers/string.helper";
import { TableConfig } from "../../interfaces/table";

interface ACFPageConfig {
  [key: string]: {
    tableConfig: TableConfig,
  };
}

export const ACF_PAGE_CONFIG: ACFPageConfig = {
  pages: {
    tableConfig: {
      headers: [
        {
          label: 'Title',
          value: 'title'
        },
        {
          label: 'URL',
          value: 'url',
          html: true,
          transform: 'getArticleUrl',
        },
        {
          label: 'Category',
          value: 'page_category',
          transform: 'getArticleCategory',
        },
      ],
      actions: {
        export: true,
      },
      // sortable: false,
    }
  },
  posts: {
    tableConfig: {
      headers: [
        {
          label: 'Title',
          value: 'title'
        },
        {
          label: 'URL',
          value: 'url',
          html: true,
          transform: 'getArticleUrl',
        },
        {
          label: 'Category',
          value: 'post_category',
          transform: 'getArticleCategory',
        },
      ],
      actions: {
        export: true,
      }
    }
  },
  users: {
    tableConfig: {
      headers: [
        {
          label: 'ID',
          value: 'id',
        },
        {
          label: 'Profile Image',
          value: 'profile_image',
          transform: 'toImage',
        },
        {
          label: 'Email',
          value: 'email',
        },
        {
          label: 'Name',
          value: 'first_name',
          transform: 'getFullName',
        },
        {
          label: 'Role',
          value: 'role',
          transform: 'getRole',
        }
      ]
    }
  },
  collections: {
    tableConfig: {
      headers: [
        {
          label: 'Icon',
          value: 'icon',
          // transform: ''
        },
        {
          label: 'Name',
          value: 'name',
          transform: 'createLink',
        },
        {
          label: 'Key',
          value: 'key',
          // transform: ''
        },
      ]
    }
  }
}


export const FILE_UPLOAD_CONFIG = {
  uploadAPI: {
    url:"https://example-file-upload-api"
  }
}