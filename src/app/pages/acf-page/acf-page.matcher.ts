import { UrlMatchResult, UrlSegment, UrlSegmentGroup } from "@angular/router"

export function AcfPageMatcher(
  segments: UrlSegment[],
  group: UrlSegmentGroup,
): UrlMatchResult {

  // Template matcher, returns URL segments when url from `/shared/constants` sidebar navs matched
  const acfPages = [
    'pages',
    'posts',
    'users',
    'collections',
  ];
  const pageTypes = [
    'all',
    'new',
    'edit',
    'manage',
    // 'collections',
    // ''
  ];

  if (segments.length) {
    if (acfPages.includes(segments[0].path)) {
      if (segments.length > 1) {
        const pageType = segments[1]?.path;
        if (pageType && pageTypes.includes(pageType)) {
          return { consumed: segments };
        }
        return { consumed: [] };
      }
      return { consumed: segments }
    }
  }

  return null;
  
}
