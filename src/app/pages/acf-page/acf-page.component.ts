import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { detectMutation, sort } from '../../../shared/helpers/object.helper';
import { ApiResponse } from '../../../shared/interfaces/api';
import { ACFField, FieldInputs } from '../../components/acf/acf.interface';
import { TableSort } from '../../components/table/table.interface';
import { DocumentRef } from '../../dom-abstraction/document.service';
import { cloneDeep, flatMapDeep, hasKey, keys, mergeArrayObjects, removeQueryParams, strToWords, ucWords } from '../../helpers/string.helper';
import { Alert } from '../../interfaces/alert/alert';
import { SiteOptions } from '../../interfaces/article-page.interface';
import { RouteState } from '../../interfaces/route-state';
import { TableConfig } from '../../interfaces/table';
import { AcfService } from '../../services/acf/acf.service';
import { AppStateService } from '../../services/app-state/app-state.service';
import { BackendService } from '../../services/backend/backend.service';
import { RoutingService } from '../../services/routing/routing.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';
import { ACF_PAGE_CONFIG, FILE_UPLOAD_CONFIG } from './acf-page.config';

@Component({
  selector: 'cms-acf-page',
  templateUrl: './acf-page.component.html',
  styleUrls: ['./acf-page.component.scss']
})
export class AcfPageComponent implements OnInit, OnDestroy {

  public acf: ACFField[][];
  public routeState: RouteState;
  public tableData: any[] = [];
  public tableConfig: TableConfig = {};
  public acfs: any = null;
  public activeACF = '';
  public alert: Alert;
  public loader: any = { loading: true };
  public action = 'Create';
  public pagination: any;
  public importFile: any;
  public fileUploadConfig = FILE_UPLOAD_CONFIG;
  public isModal = false;
  public modal: any = {
    title: 'Import',
    action: {
      text: 'Import'
    },
    content_type: 'import',
  }
  public exports = [];
  public imports = [];
  public exportACF: ACFField | ACFField[][] = {
    name: 'name', label: 'File name', type: 'text', value: ''
  }
  public tableSelected = [];
  public inputs: FieldInputs;
  public activeSort: TableSort = {
    order: 'ascending',
    active: 'id',
  };

  private isDestroyed$ = new Subject<void>();
  private category: string;
  private tableActionDone = true;
  private document: Document;
  private uploadFile: File;
  private tableDataCopy = [];

  constructor(
    private userAuthService: UserAuthService,
    private acfService: AcfService,
    private appStateService: AppStateService,
    private routingService: RoutingService,
    private backendService: BackendService,
    private docRef: DocumentRef,
  ) {
    this.document = docRef.nativeDocument;
  }

  listenRoute() {
    return this.appStateService.getRouteState()
  }

  initLoader() {
    this.listenRoute()
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe((routeState) => {
        this.routeState = routeState;
        this.action = routeState.type === 'edit' ? 'Update' : 'Create';
        this.category = routeState.category;
        this.acfs = null;
        this.loader = { loading: true }
        this.tableData = null;
        this.tableConfig = null;
        this.alert = null;
        this.tableSelected = [];
        this.isModal = false;
        this.inputs = routeState;
      })
  }

  initEditTemplate(acf, records) {
    const template = acf.data.map(d => d.fields);
    return this.acfService.recordToEdit(template, records);
  }

  generateTableHeaders(tableData = []) {
    const headers = [
      {
        value: 'id',
        label: 'ID',
      }
    ];
    keys(tableData).forEach((key, idx) => {
      if (idx < 4 && key !== 'id') {
        headers.push({
          value: key,
          label: ucWords(strToWords(key)),
        });
      }
    });
    return headers;
  }

  ngOnInit(): void {
    const user = this.userAuthService.getUserSession();
    this.initLoader();
    this.listenRoute()
    .pipe(
      switchMap((routeState) => this.acfService.getACF(routeState, user)),
      switchMap(([routeState, acf]) => this.acfService.getRecord(routeState, acf)),
      takeUntil(this.isDestroyed$)
    )
    .subscribe(([routeState, acf, records, siteOptions]: [RouteState, ApiResponse, any, SiteOptions]) => {
      let { category, type, record } = routeState;
      this.loader = { loading: false };
      switch (type) {
        case 'all':
          this.tableDataCopy = cloneDeep(records.data)
          this.tableData = flatMapDeep(this.acfService.fieldstoACF(records.data, null, 'table').map(res => mergeArrayObjects(res)));
          this.tableConfig = Object.assign({}, ACF_PAGE_CONFIG[category]?.tableConfig, siteOptions[category]);
          if (!this.tableConfig.headers) {
            this.tableConfig.headers = this.generateTableHeaders(this.tableData[0]);
          }
          const { active, order } = this.activeSort;
          this.tableData = sort(this.acfService.transformTableData(
            this.tableData,
            this.tableConfig,
            siteOptions,
          ), active, order);
        return;
        case 'edit':
          this.acf = this.initEditTemplate(acf, records)
          if (!this.acf) {
            this.alert = { type: 'warning', text: `Record "${record}" not found.` };
            return;
          }
          break;
        case 'new':
          this.acf = this.acfService.fieldstoACF(acf.data, category);
          break;
      }
      this.acf = this.acf.map(fields => this.setAttrs(fields));

      this.acfs = this.acf.map((fields, i) => ({
        config: acf.data[i].config,
        fields: [fields],
      }));
    });
  }

  setAttrs(fields, type?: string) {
    const finalField = fields.map((field: ACFField) => {
      if (field) {
        field = this.assignAttrs(field);
        if (field.form_group) {
          const formGroup = this.setAttrs(field.form_group, 'group');
          field.form_group = formGroup;
        }
        if (field.fields && field.form_group) {
          field.fields = field.form_group;
        }
      }

      return field;
    });

    if (type === 'group') {
      return [finalField];
    }

    return finalField;
  }

  assignAttrs(field): ACFField {
    field = { ...field, ...mergeArrayObjects(field.attributes) };
    return field;
  }

  async onFieldAction(action) {
    this.alert = null;
    if (action.action === 'check-availability') {
      let invalidUniq: any = [];
      if (!action.field) {
        invalidUniq = this.checkUniqs();
      } else {
        const uniqStatus: any = await this.checkUniq(action.field, this.routeState.category, action.parent);
        if (!uniqStatus.available) {
          invalidUniq.push(action.field.label);
        }
      }
      const message = invalidUniq.length
        ? `<i class="fas fa-exclamation-circle"></i> ${invalidUniq.join(', ')} is already taken.`
        : `${action.field.label} is available`;
        this.alert = {
          type: invalidUniq.length ? 'warning' : 'success',
          text: message,
        };
    }  else if(action.action === 'generate') {
        action.field.value = this.acfService.defaultValue(action.field);
        this.checkUniqs();
    }
  }

  async onTableAction(action) {
    this.alert = null;
    this.isModal = false;
    this.modal = null;
    if (action.action.action === 'sort') {
      this.activeSort = {
        active: action.record,
        order: action.action.order === 'descending' ? 'ascending' : 'descending',
      };
      this.tableData = sort(
        this.tableData,
        action.record,
        this.activeSort.order
      );

    } else if (action.action === 'view') {
      this.acfService.getACF(this.routeState, this.userAuthService.getUserSession(), true)
        .subscribe(res => {
          const record = this.tableDataCopy.find(tableData => tableData.id === action.record.id);
          this.acf = this.initEditTemplate(res[1], [record]);
          this.isModal = true;
          this.modal = {
            title: 'View Details',
            action: false,
            content_type: 'view',
            record: action.record,
          }
        })
    } else if (action.action === 'select') {
      this.tableSelected = action.record;
    } else if (action.action === 'import') {
      this.isModal = true;
      this.modal = {
        title: `Import ${this.routeState.category}`,
        action: {
          text: 'Import'
        },
        content_type: 'import',
      }
    } else if (action.action === 'export') {
      this.isModal = true;
      this.modal = {
        title: `Export ${this.routeState.category}`,
        content_type: 'export',
        action: {
          text: 'Export'
        },
      };

      if (!Array.isArray(this.exportACF)) {
        this.exportACF = [[{...this.exportACF, value: this.routeState.category }]];
      }
      if (Array.isArray(action.record)) {
        this.exports = this.tableDataCopy.filter(tableData => action.record.find(rec => rec.id === tableData.id));
      } else {
        this.exports = this.tableDataCopy.find(tableData => tableData.id === action.record.id);
      }
    } else if (action.action === 'edit') {
      const { category, queryParams, href, url } = this.routeState;
      let link = `cms/${category}/edit/${action.record.id}`;
      let params = { queryParams: {}};
      if (hasKey(queryParams) && queryParams.category) {
        link = `${removeQueryParams(url)}`;
        params.queryParams = {
          ...queryParams,
          type: 'edit',
          record: action.record.id,
        };
      }
      this.routingService.goto(link, params);
    } else if (action.action === 'delete') {
      this.alert = null;
      if (!this.tableActionDone) {
        this.alert = {
          text: 'Action is too fast. Please wait for a moment.',
          type: 'warning',
        };
        return;
      }

      this.alert = {
        text: 'Please wait.',
        type: 'info'
      }

      this.tableActionDone = false;
      let index: number[] | number = [];
      if (Array.isArray(action.record)) {
        this.tableData.forEach((rec, i) => {
          action.record.forEach(actionRec => {
            if (actionRec.id === rec.id) {
              (<number[]>index).push(i)
            }
          })
        });
      } else {
        index = this.tableData.findIndex(rec => rec.id === action.record.id);
      }
      if (index === undefined) {
        this.alert = {
          text: `Error: Failed to delete, record "${index}" not found.`,
          type: 'warning'
        }
        return;
      }

      const deleteFunc$ = (
        Array.isArray(action.record)
          ? this.acfService.deleteMany(action.record, this.category)
          : this.acfService.delete(action.record, this.category)
      );

      deleteFunc$
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe((res: ApiResponse) => this.onDelete(res, action.record, index));
    }
  }

  onDelete(res, record, index) {
    this.alert = null;
    if (Array.isArray(index)) {
      index.forEach(i => {
        this.tableData.splice(i, 1);
      })
    } else {
      this.tableData.splice(index, 1);
    }
    if (this.category === 'collections' && !this.routeState.queryParams.length) {
      let cols = this.appStateService.getCollections();
      const idx = cols.findIndex(col => col.key === record.key);
      cols.splice(idx, 1);
      this.appStateService.setCollections(detectMutation(cols));
    }

    // Trigger table onChanges
    // Revise, improper to map
    this.tableData = detectMutation(this.tableData);
    setTimeout(() => {
      this.alert = {
        text: res.message,
        type: res.code ? 'success' : 'danger',
      };
    });
    this.tableActionDone = true;
  }

  async checkUniqs() {
    const fields = [];
    for (const field of flatMapDeep(this.acf)) {
      if (field.unique_value) {
        field.valid = null;
        const result: any = await this.checkUniq(field, this.routeState.category);
        if (result.available === false) {
          field.valid = false;
          fields.push(field.label);
        } else {
          field.valid = true;
        }
      }
    }

    return fields;
  }

  async checkUniq(field, category, parent?: string) {
    const params = parent && parent !== field.name
      ? { [`${parent}.${field.name}`]: field.value }
      : { [field.name]: field.value };
    return await this.backendService.get(
      `uniq/${category}`,
      { id: this.routeState.record, ...params },
      { v2: true }
    ).toPromise();
  }

  async onSubmit(e) {
    e.preventDefault();
    const { record, category } = this.routeState;
    const action = this.action.toLowerCase();
    let acf = this.acfService.rebuildACF({
      user: this.userAuthService.getUserSession(),
      fields: this.acf,
      type: action,
      id: record,
      extract: true,
    });
    this.alert = null;
    this.acfService.checkInvalidFields(this.acf);
    const invalidUniq: any = await this.checkUniqs();
    const alertText = (
      invalidUniq.length
        ? `${invalidUniq.join(', ')} is already taken.`
        : 'Please resolve required fields before submitting.'
    );

    if (acf.invalid.length || invalidUniq.length) {
      this.alert = {
        type: 'warning',
        text: `<i class="fas fa-exclamation-circle"></i> ${alertText}`,
      };

      return;
    }
    delete acf.invalid;
    if (keys(acf.config).length === 0) {
      delete acf.config;
    }
    this.backendService.post(`${category}/${action}`, acf)
      .subscribe((res: ApiResponse) => {
        if (category === 'collections' && !this.routeState.queryParams.length) {
          this.createSidebarCollection(acf.fields);
        }
        this.alert = {
          type: res.code ? 'success' : 'danger',
          text: res.message,
        }
        if (res.code) {
          this.ngOnDestroy();
          this.ngOnInit();
        }
      });
  }

  createSidebarCollection(fields) {
    fields = fields.map(field => {
      return {
        category: field.key,
        url: `/cms/${field.key}`,
        label: field.name,
        icon: field.icon,
        base_url: 'collections',
      }
    })
    const cols = this.appStateService.getCollections();
    this.appStateService.setCollections(cols.concat(fields));
  }

  onAlertClose(close) {
    if (close) {
      this.alert = null;
    }
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  export(category, record) {
    this.backendService.get(`collections/${category}`)
      .subscribe((res: ApiResponse) => this.acfService.readyExportFile(category, res.data))
  }

  import(e, category) {
    e.preventDefault();
  }

  onFileSelect(files: FileList) {
    this.uploadFile = files.item(0);
    if (!this.uploadFile) {
      this.alert = {
        text: 'No file selected.',
        type: 'warning'
      }
      return;
    }
      const fileReader = new FileReader();
      fileReader.readAsText(files.item(0), 'UTF-8');
      fileReader.onload = () => {
        try {
          this.imports =  JSON.parse(<string>fileReader.result);
        } catch (e) {
          this.alert = {
            text: 'Invalid file. File must be a valid json.',
            type: 'danger',
          }
        }
      }
    
  }

  upload() {
    this.backendService.post(`import/${this.routeState.category}`, this.imports)
      .subscribe((res: ApiResponse) => {
        if (res.code) {
          this.alert = {
            text: res.message,
            type: res.code ? 'success' : 'danger',
          }
          this.ngOnDestroy();
          this.ngOnInit();
          this.routingService.goto(`/cms/${this.routeState.category}/all`);
        }
        this.isModal = false;
      })
  }

  onModalAction(action) {
    if (action === 'export') {
      const name = flatMapDeep(<ACFField[][]>this.exportACF)[0].value;
      this.acfService.readyExportFile(this.routeState.category, this.exports, name || this.routeState.category);
    } else if (action === 'import') {
      this.upload();
    }
  }

  toggleModal(close) {
    this.isModal = !close;
  }
  

}
