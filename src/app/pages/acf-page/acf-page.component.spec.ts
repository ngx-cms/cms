import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcfPageComponent } from './acf-page.component';

describe('AcfPageComponent', () => {
  let component: AcfPageComponent;
  let fixture: ComponentFixture<AcfPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcfPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcfPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
