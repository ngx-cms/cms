import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForbiddenPageComponent } from './forbidden-page.component';


const routes: Routes = [
  {
    path: '',
    component: ForbiddenPageComponent,
    // resolve: {
    //   config: RouteStateResolver,
    // },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForbiddenPageRouting { }
