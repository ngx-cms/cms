import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForbiddenPageComponent } from './forbidden-page.component';
import { ForbiddenPageRouting } from './forbidden-page.routing';



@NgModule({
  declarations: [ForbiddenPageComponent],
  imports: [
    CommonModule,
    ForbiddenPageRouting,
  ]
})
export class ForbiddenModule { }
