import { Component, isDevMode, OnDestroy, OnInit } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApiResponse } from '../../../shared/interfaces/api';
import { ACFField } from '../../components/acf/acf.interface';
import { WindowRef } from '../../dom-abstraction/window.service';
import { hasKey } from '../../helpers/string.helper';
import { Alert } from '../../interfaces/alert/alert';
import { AppStateService } from '../../services/app-state/app-state.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  public isLoggedin: boolean;
  public alert: Alert = null;

  private USER_STATE_KEY = makeStateKey('user');
  private redirect = true;
  private window: Window;
  private isDestroyed$ = new Subject<void>();
  private origin = '';
  private isBrowser: boolean;
  private isServer: boolean;

  constructor(
    private transferStateService: TransferState,
    private route: ActivatedRoute,
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
    private appStateService: AppStateService,
  ) {
    this.window = winRef.nativeWindow;
    this.origin = this.route.snapshot.params.origin || (isDevMode() ? '/cms/dashboard' : '/cms');
    this.isBrowser = appStateService.isBrowser;
    this.isServer = appStateService.isServer;
  }

  ngOnInit(): void {
    const userState = this.transferStateService.get(this.USER_STATE_KEY, <any>{});
    // Get transferState('user') on server to avoid flickering on login page
    // Then remove it from server as it is already assigned in client
    if (this.isBrowser && hasKey(userState)) {
      this.isLoggedin = true;
      this.transferStateService.remove(this.USER_STATE_KEY);
    }
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  onLoginAction(loginAction){ 
    const { action, fields } = loginAction;
    switch (action) {
      case 'submit':
        this.login(fields);
        break;
      case 'forgot-password':
        break;
      default:
    }
  }

  login(inputs: ACFField[]): void {
    this.alert = null;
    const data = { username: '', password: '' };
    const emptyInputs = inputs.filter(input => input.value === '');

    if (emptyInputs.length) {
      this.alert = {
        text: 'Missing username / password',
        type: 'warning',
      };
      return;
    }
    inputs.forEach(input => data[input.name] = input.value);
    // For testing purpose only
    if ((data.username === 'developer' && data.password === 'password')) {
      this.userAuthService.userInfo({
        username: 'developer',
        email: 'ngx-cms@admin.com',
        role: 1,
      });
      if (this.redirect) {
        this.window.location.href = this.origin;
      }
    }

    this.userAuthService.login(data)
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe((res: ApiResponse) => {
        this.alert = {
          text: res.message,
          type: res.code ? 'success' : 'danger',
        }
        if (res.code) {
          // Set transferState('user') on SSR to avoid flickering on login page
          if (this.isServer)  {
            this.transferStateService.set(this.USER_STATE_KEY, res.data);
          }
          this.userAuthService.userInfo(res.data);
          if (this.redirect) {
            this.window.location.href = this.origin;
          }
        }
      }, (err) => {
        this.alert = {
          text: `Something went wrong, please try again later`,
          type: 'danger',
        }
      })
  }

}
