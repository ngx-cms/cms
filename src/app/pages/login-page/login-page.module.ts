import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { LoginRouting } from './login-page.routing';
import { RouterModule } from '@angular/router';
import { LoginModule } from '../../components/login/login.module';
import { AlertModule } from '../../components/alert/alert.module';



@NgModule({
  declarations: [LoginPageComponent],
  imports: [
    CommonModule,
    RouterModule,
    LoginRouting,
    LoginModule,
    AlertModule,
  ]
})
export class LoginPageModule { }
