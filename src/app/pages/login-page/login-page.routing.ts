import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingResolver } from '../../resolvers/router/routing/routing.resolver';
import { LoginPageComponent } from './login-page.component';


const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
    resolve: {
      data: RoutingResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRouting { }
