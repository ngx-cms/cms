import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseGuard } from '../../../guards/base/base.guard';
import { RouteStateResolver } from '../../../resolvers/router/route-state/route-state.resolver';
import { AcfPageMatcher } from '../../acf-page/acf-page.matcher';
import { BaseComponent } from './base.component';


const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    resolve: {
      config: RouteStateResolver,
    },
    children: [
      {
        path: 'forbidden',
        loadChildren: () => import('../../forbidden-page/forbidden-page.module').then(m => m.ForbiddenModule),
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../../../components/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'acf',
        loadChildren: () => import('../../form-fields-page/form-fields-page.module').then(m => m.FormFieldsPageModule),
      },
      {
        path: 'database',
        loadChildren: () => import('../../database-page//database-page.module').then(m => m.DatabasePageModule),
      },
      {
        path: 'site-options',
        loadChildren: () => import('../../site-options-page/site-options-page.module').then(m => m.SiteOptionsPageModule),
      },
      // {
      //   path: 'collections',
      //   loadChildren: () => import('../../site-options-page/site-options-page.module').then(m => m.SiteOptionsPageModule),
      // },
      {
        matcher: AcfPageMatcher,
        loadChildren: () => import('../../acf-page/acf-page.module').then(m => m.AcfPageModule),
      },
    ],
    canActivate: [BaseGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRouting { }
