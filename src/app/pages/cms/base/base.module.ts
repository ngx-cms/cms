import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base.component';
import { NavbarModule } from '../../../components/navbar/navbar.module';
import { RouterModule } from '@angular/router';
import { NotFoundModule } from '../../not-found/not-found.module';
import { SideNavbarModule } from '../../../components/navbar/side-navbar/side-navbar.module';
import { BaseRouting } from './base.routing';
import { LoginModule } from '../../../components/login/login.module';
import { DashboardModule } from '../../../components/dashboard/dashboard.module';



@NgModule({
  declarations: [
    BaseComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    BaseRouting,
    LoginModule,
    SideNavbarModule,
    NotFoundModule,
    NavbarModule,
    DashboardModule,
  ]
})
export class BaseModule { }
