import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WindowRef } from '../../../dom-abstraction/window.service';
import { cloneDeep } from '../../../helpers/string.helper';
import { RouteState } from '../../../interfaces/route-state';
import { SidebarMobileBars } from '../../../interfaces/sidebar';
import { User } from '../../../interfaces/user/user';
import { AppStateService } from '../../../services/app-state/app-state.service';
import { UserAuthService } from '../../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit, OnDestroy {
  title = 'NGX CMS';

  sidebarConfig = {
    minimized: false,
    classes: '',
    show: true,
    loaded: false,
  };
  private window: Window;
  isMobile = false;
  mobileBars: SidebarMobileBars[] = [
    {
      event: {
        type: 'click',
        function: 'toggleSidebar',
      },
      icon: '<i class="fas fa-bars"></i>'
    },
  ];

  loginPage: boolean = false;
  user: User = {
    is_logged_in: false,
  };
  is404 = false;
  category: string;
  public routeState$: Observable<RouteState>;
  public navCollections$: Observable<any[]>;

  @HostListener('window:resize', ['$event'])
  onResize($event) {
    this.sidebarState($event);
  }


  private isDestroyed$ = new Subject<void>();
  constructor(
    private appStateService: AppStateService,
    private winRef: WindowRef,
    private route: ActivatedRoute,
    private userAuthService: UserAuthService,
  ) {
    this.window = this.winRef.nativeWindow;
    this.loginPage = this.window.location.pathname.startsWith('/login');
    // this.navCollections = appStateService.getCollections();
  }

  ngOnInit() {
    this.sidebarState(this.window);
    this.route.data
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe(res => {
      const { config } = res;
      console.log('Session: ', this.userAuthService.getUserSession())
      
      if (config?.user?.authenticate === true) {
        this.setUser();
      }

      this.is404 = config?.page_not_found;
    });

    // this.category = this.window.location.pathname.split('/')[2];
    this.routeState$ = this.appStateService.getRouteState().pipe(takeUntil(this.isDestroyed$));
    // this.listenCollectionChange();
    this.navCollections$ = this.appStateService.listenCollections()
  }

  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  // TODO: Avoid using subscriber in `app.component.ts`
  setUser() {
    this.userAuthService.userSession
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe((user: User) => {
        this.loginPage = !user?.is_logged_in;
        this.user = user;
      })
  }

  sidebarState($event) {
    const width = $event?.target?.innerWidth || $event.innerWidth;
    this.sidebarConfig.show = width >= 768;
    this.isMobile = !this.sidebarConfig.show;
    this.appStateService.setViewStatus(width);
  }

  eventHelper(nav) {
    if (nav?.event?.function) {
      const func = this[nav?.event?.function];
      if (func) {
        this[nav?.event?.function]();
      } else {
        console.error(`Unknown function "${nav?.event?.function}"`);
      }
    }
  }

  toggleSidebar() {
    this.sidebarConfig.show = !this.sidebarConfig.show;
  }

  isMinimize(minimize) {
    this.sidebarConfig.minimized = minimize;
    if (minimize && this.isMobile) {
      this.toggleSidebar();
      return;
    }

    this.sidebarConfig.classes = minimize ? 'sidebar-minimized' : '';
  }

  routeChanged(change) {
    this.sidebarConfig.show = !(this.isMobile && change)
  }

}
