import { Component, OnInit } from '@angular/core';
import { cloneDeep, hasKey, isObject, toJSON } from '../../helpers/string.helper';
import { Alert } from '../../interfaces/alert/alert';
import { AcfService } from '../../services/acf/acf.service';
import { BackendService } from '../../services/backend/backend.service';

@Component({
  selector: 'cms-database-page',
  templateUrl: './database-page.component.html',
  styleUrls: ['./database-page.component.scss']
})
export class DatabasePageComponent implements OnInit {

  public alert: Alert = {}
  fields = [
    {
      label: 'ACF Type',
      type: 'radio',
      value: 'fields',
      name: 'type',
      options: [
        {
          value: 'config',
          label: 'Config'
        },
        {
          value: 'fields',
          label: 'Fields'
        }
      ]
    },
    {
      label: 'Add ACF',
      type: 'textarea',
      name: 'acf',
    },
  ];
  constructor(
    private backendService: BackendService,
    private acfService: AcfService,
  ) { }

  ngOnInit(): void {
    this.backendService.get('acf.config')
      .subscribe(res => {
        console.log('res: ', res)
      })
  }

  onSubmit(e, fields) {
    e.preventDefault();
    this.alert = { enabled: false };
    let field = fields.find(f => f.name === 'acf');
    const type = fields.find(f => f.name === 'type');
    if (!isObject(field.value)) {
      this.alert = {
        enabled: true,
        text: `Invalid JSON`,
        type: 'warning'
      };
      return;
    }
    field = JSON.parse(cloneDeep(toJSON(field.value)));
    if (!hasKey(field)) {
      this.alert = {
        enabled: true,
        text: 'Invalid empty JSON',
        type: 'danger',
      }
      return;
    }

    field.id = this.acfService.createID();
    this.backendService.post(`acf.${type.value}/create`, field)
      .subscribe(res => {
        console.log('submit: ', res)
      })
  }

}
