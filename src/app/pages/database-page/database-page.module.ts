import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatabasePageRoutingModule } from './database-page-routing.module';
import { DatabasePageComponent } from './database-page.component';
import { FieldsModule } from '../../components/acf/fields/fields.module';
import { AlertModule } from '../../components/alert/alert.module';


@NgModule({
  declarations: [
    DatabasePageComponent
  ],
  imports: [
    CommonModule,
    DatabasePageRoutingModule,
    FieldsModule,
    AlertModule,
  ]
})
export class DatabasePageModule { }
