import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatabasePageComponent } from './database-page.component';

const routes: Routes = [
  {
    path: '',
    component: DatabasePageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatabasePageRoutingModule { }
