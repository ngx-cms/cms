import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { distinctUntilChanged, switchMap, takeUntil } from 'rxjs/operators';
import { ApiResponse } from '../../../shared/interfaces/api';
import { ACFField } from '../../components/acf/acf.interface';
import { cloneDeep, mergeArrayObjects } from '../../helpers/string.helper';
import { Alert } from '../../interfaces/alert/alert';
import { SiteOptions } from '../../interfaces/article-page.interface';
import { RouteState } from '../../interfaces/route-state';
import { AcfService } from '../../services/acf/acf.service';
import { AppStateService } from '../../services/app-state/app-state.service';
import { BackendService } from '../../services/backend/backend.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-site-options-page',
  templateUrl: './site-options-page.component.html',
  styleUrls: ['./site-options-page.component.scss']
})
export class SiteOptionsPageComponent implements OnInit, OnDestroy {

  public acf: ACFField[][];
  public routeState: RouteState;
  public acfs: any = null;
  public activeACF = '';
  public alert: Alert;
  public loader: any = { loading: true };
  public action = 'Create';
  public category: string;

  private isDestroyed$ = new Subject<void>();
  private id: string;

  constructor(
    private userAuthService: UserAuthService,
    private acfService: AcfService,
    private appStateService: AppStateService,
    private backendService: BackendService,
  ) { }

  listenRoute() {
    return this.appStateService.getRouteState()
  }

  resetFields(routeState) {
    this.routeState = routeState;
    this.action = routeState.type === 'edit' ? 'Update' : 'Create';
    this.category = routeState.category;
    this.acfs = null;
    this.loader = { loading: true }
    this.alert = null;
  }

  initLoader() {
    this.listenRoute()
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe((routeState) => this.resetFields(routeState))
  }

  ngOnInit(): void {
    const user = this.userAuthService.getUserSession();
    this.initLoader();
    this.listenRoute()
    .pipe(
      switchMap((routeState) => this.acfService.getACF(routeState, user)),
      switchMap(([routeState, acf]) => this.acfService.getRecord(routeState, acf)),
      distinctUntilChanged(() => true), // Prevent reload when updateing acf
      takeUntil(this.isDestroyed$)
    )
    .subscribe(([routeState, acf, records, siteOptions]: [RouteState, ApiResponse, any, SiteOptions]) => {
      this.resetFields(routeState);
      this.id = records.data.find(e => e.id)?.id;
      this.action = !this.id ? 'Create' : 'Update';
      let { category } = routeState;
      this.loader = { loading: false };
      const template = this.acfService.fieldstoACF(acf.data, category);
      this.acf = this.acfService.recordToEdit(cloneDeep(template), [records.data[0]]);

      // SHOW ACF IF NO CURRENT SITE OPTIONS
      if (!this.acf) {
        this.acf = this.acfService.fieldstoACF(acf.data, category);
      }
      this.acf = this.acf.map(fields => this.setAttrs(fields));

      this.acfs = this.acf.map((fields, i) => ({
        config: acf.data[i].config,
        fields: [fields],
      }));
      
    });
  }

  setAttrs(fields, type?: string) {
    const finalField = fields.map((field: ACFField) => {
      if (field) {
        field = this.assignAttrs(field);
        if (field.form_group) {
          const formGroup = this.setAttrs(field.form_group, 'group');
          field.form_group = formGroup;
        }
        if (field.fields && field.form_group) {
          field.fields = field.form_group;
        }
      }

      return field;
    });

    if (type === 'group') {
      return [finalField];
    }

    return finalField;
  }

  assignAttrs(field): ACFField {
    field = { ...field, ...mergeArrayObjects(field.attributes) };
    return field;
  }

  onSubmit(e) {
    e.preventDefault();
    const { type, record, category } = this.routeState;
    const action = this.action.toLowerCase();
    const acf = this.acfService.rebuildACF({
      user: this.userAuthService.getUserSession(),
      fields: this.acf,
      type: action,
      id: this.id || null,
    });


    this.backendService.post(`${category}/${action}`, acf)
      .subscribe((res: ApiResponse) => {
        if (res.code) {
          this.action = 'Update';
          this.appStateService.updateSiteOptions(mergeArrayObjects(cloneDeep(acf).fields, false))
        }
        this.alert = {
          type: res.code ? 'success' : 'danger',
          text: res.message,
        }
      })
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
  

}
