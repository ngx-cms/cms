import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteOptionsPageRoutingModule } from './site-options-page-routing.module';
import { SiteOptionsPageComponent } from './site-options-page.component';
import { AcfModule } from '../../components/acf/acf.module';
import { BackModule } from '../../components/back/back.module';
import { ButtonModule } from '../../components/button/button.module';
import { LoadersModule } from '../../components/loaders/loaders.module';
import { AlertModule } from '../../components/alert/alert.module';
import { TableModule } from '../../components/table/table.module';


@NgModule({
  declarations: [SiteOptionsPageComponent],
  imports: [
    CommonModule,
    SiteOptionsPageRoutingModule,
    AcfModule,
    BackModule,
    ButtonModule,
    LoadersModule,
    AlertModule,
    TableModule,
  ]
})
export class SiteOptionsPageModule { }
