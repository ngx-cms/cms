import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteOptionsPageComponent } from './site-options-page.component';

const routes: Routes = [
  {
    path: '',
    component: SiteOptionsPageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteOptionsPageRoutingModule { }
