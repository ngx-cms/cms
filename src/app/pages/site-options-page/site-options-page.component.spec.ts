import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteOptionsPageComponent } from './site-options-page.component';

describe('SiteOptionsPageComponent', () => {
  let component: SiteOptionsPageComponent;
  let fixture: ComponentFixture<SiteOptionsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteOptionsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteOptionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
