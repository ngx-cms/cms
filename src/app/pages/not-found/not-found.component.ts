import { Component, Input, OnInit } from '@angular/core';
import { RoutingService } from '../../services/routing/routing.service';

@Component({
  selector: 'cms-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  @Input() action: boolean = true;
  @Input() message: string;

  constructor(
    private routingService: RoutingService,
  ) { }

  ngOnInit(): void {
  }

  back() {
    this.routingService.goto('/cms');
  }

}
