import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found.component';
import { PipesModule } from '../../pipes/pipes.module';



@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    PipesModule,
  ],
  exports: [
    NotFoundComponent,
  ]
})
export class NotFoundModule { }
