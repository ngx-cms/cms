import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { CustomFieldConfig, FormFields } from '../interfaces/custom-fields';
import { RouteState } from '../interfaces/route-state';

@Component({
  selector: 'cms-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, OnDestroy {

  private isDestroyed$ = new Subject<void>();
  routeState: RouteState = {};
  customFields: FormFields[][];
  customFieldsConfig: CustomFieldConfig = {
    development: false,
  };

  constructor(
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  getForm(form) {
  }

}
