import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormFieldsPageComponent } from './form-fields-page.component';

const routes: Routes = [
  {
    path: ':type',
    component: FormFieldsPageComponent,
    children: [
      {
        path: ':id',
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormFieldsPageRoutingModule { }
