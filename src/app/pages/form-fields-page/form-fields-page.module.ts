import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormFieldsPageRoutingModule } from './form-fields-page-routing.module';
import { FormFieldsPageComponent } from './form-fields-page.component';
import { AcfModule } from '../../components/acf/acf.module';
import { ButtonModule } from '../../components/button/button.module';
import { TableModule } from '../../components/table/table.module';
import { BackModule } from '../../components/back/back.module';
import { AlertModule } from '../../components/alert/alert.module';
import { ModalModule } from '../../shared/components/modal/modal.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';


@NgModule({
  declarations: [FormFieldsPageComponent],
  imports: [
    CommonModule,
    FormFieldsPageRoutingModule,
    AcfModule,
    ButtonModule,
    TableModule,
    BackModule,
    AlertModule,
    ModalModule,
    NgxJsonViewerModule,
  ]
})
export class FormFieldsPageModule { }
