import { sidebar } from "../../../shared/constants"

export const ACF_PAGE_CONFIG = {
  acf: {
    tableConfig: {
      headers: [
        {
          label: 'Form',
          value: 'form_name'
        },
        {
          label: 'Category',
          value: 'category',
          html: true,
          transform: (params, key) =>  {
            return params?.record?.category?.map(cat => {
              const label = sidebar.navs.find(nav => nav.url === cat);
              return `<a href="/cms/acf/${cat}">${label?.label || cat}</a>`
            })
          }
        },
      ],
      actions: {
        view: false
      }
    }
  },
}