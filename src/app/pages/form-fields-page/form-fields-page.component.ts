import { Component, OnDestroy, OnInit } from '@angular/core';
import { cloneDeep } from 'lodash';
import { switchMap, takeUntil } from 'rxjs/operators';
import { combineLatest, of, Subject } from 'rxjs';
import { ACF_PAGE_CONFIG } from './form-fields-page.config';
import { flatMap, flatMapDeep, mergeArrayObjects } from '../../helpers/string.helper';
import { ACFField } from '../../components/acf/acf.interface';
import { RouteState } from '../../interfaces/route-state';
import { Alert } from '../../interfaces/alert/alert';
import { BackendService } from '../../services/backend/backend.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';
import { AcfService } from '../../services/acf/acf.service';
import { AppStateService } from '../../services/app-state/app-state.service';
import { RoutingService } from '../../services/routing/routing.service';
import { SiteOptions } from '../../interfaces/article-page.interface';
import { ApiResponse } from '../../../shared/interfaces/api';
import { ACF_CONFIG } from '../../components/acf/mock/acf-config.acf';
import { ACF_FIELDS } from '../../components/acf/acf.mock';

@Component({
  selector: 'cms-form-fields-page',
  templateUrl: './form-fields-page.component.html',
  styleUrls: ['./form-fields-page.component.scss']
})
export class FormFieldsPageComponent implements OnInit, OnDestroy {

  public acf: ACFField[][] = [];
  public acfConfig: ACFField[][] = [];
  public routeState$;
  public tableData: any[] = [];
  public tableConfig = {};
  public routeState: RouteState;
  public acfs: any;
  public alert: Alert = null;
  public isModal: boolean;
  public exportACF: ACFField | ACFField[][];
  public exports = [];
  public modal = {
    title: 'Export',
    action: {
      text: 'Continue'
    }
  }

  private acfClone: ACFField[][] = [];
  private isDestroyed$ = new Subject<void>();
  private category: string;
  private tableActionDone = true;
  private tableDataCopy = [];

  constructor(
    private backendService: BackendService,
    private userAuthService: UserAuthService,
    private acfService: AcfService,
    private appStateService: AppStateService,
    private routingService: RoutingService,
  ) { }

  ngOnInit(): void {
    const user = this.userAuthService.getUserSession();
    this.appStateService.getRouteState()
    .pipe(
      switchMap((routeState) => this.acfService.getACF(routeState, user)),
      switchMap(([routeState, acf]) => this.acfService.getRecord(routeState, acf)),
      switchMap((res) => {
        return combineLatest(of(res), this.appStateService.fetchAcfConfig());
        // return combineLatest(of(res))
      }),
      takeUntil(this.isDestroyed$)
    )
    // .subscribe(([routeState, acf, records, siteOptions, t]: [RouteState, ApiResponse, any, SiteOptions]) => {
    .subscribe((
      [[routeState, acf, records, siteOptions],[acfConfig, acfFields]]:
      [[RouteState, ApiResponse, any, SiteOptions], [ACFField[], ACFField[]]]
    ) => {
      const { category, type } = routeState;
      this.acfConfig = cloneDeep(ACF_CONFIG);
      this.routeState = routeState;
      this.category = category;
      this.reset();
      this.acfClone = cloneDeep(ACF_FIELDS);
      switch (type) {
        case 'all':
          this.tableData = flatMap(this.acfService.fieldstoACF(records.data, null, 'table').map(res => mergeArrayObjects(res)));
          this.tableDataCopy = cloneDeep(this.tableData)
          this.tableConfig = Object.assign({},ACF_PAGE_CONFIG[category]?.tableConfig, siteOptions[category]);
          this.tableData = this.acfService.transformTableData(
            this.tableData,
            this.tableConfig,
            siteOptions,
          );

          break;
          // return;
        case 'edit':
          this.acfConfig = this.acfService.acfToEdit(records, 'config');
          this.acf = this.acfService.acfToEdit(records);
          if (!this.acf) return;
          break;
        case 'new':
          // this.acf = this.acfService.fieldstoACF(acf.data);
          if (acfFields.length) {
            this.acfClone = cloneDeep(ACF_FIELDS);
            this.acf = cloneDeep(ACF_FIELDS);
          }
          break;
      }
      this.initialiseACF();
    });
  }

  reset() {
    this.tableData = null;
    this.tableConfig = null;
    this.alert = null;
    this.acf = [];
    this.acfs = [];
  }

  initialiseACF() {
    this.acfs = this.acf;
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  onAlertAction(close) {
    if (close) {
      this.alert = null;
    }
  }

  onFieldAction(action) {
    const moveIndex = action.action === 'up' ? (action.index - 1) : (action.index + 1);
    if (action.action === 'up' || action.action === 'down') {
      action.type === 'config'
        ? this.acfConfig.splice(moveIndex, 0, this.acfConfig.splice(action.index, 1)[0])
        : this.acf.splice(moveIndex, 0, this.acf.splice(action.index, 1)[0])
    } else if (action.action === 'remove') {
      action.type === 'config'
      ? this.acfConfig.splice(action.index, 1)
      : this.acf.splice(action.index, 1);
    }
  }

  onTableAction(action) {
    if (action.action === 'export') {
      this.isModal = true;
      if (!Array.isArray(this.exportACF)) {
        this.exportACF = [[{...this.exportACF, value: this.routeState.category }]];
      }
      if (Array.isArray(action.record)) {
        this.exports = this.tableDataCopy.filter(tableData => action.record.find(rec => rec.id === tableData.id));
      } else {
        this.exports = this.tableDataCopy.find(tableData => tableData.id === action.record.id);
      }
    }
    if (action.action === 'edit') {
      this.routingService.goto(`cms/${this.category}/edit/${action.record.id}`);
    } else if (action.action === 'delete') {
      this.alert = null;
      if (!this.tableActionDone) {
        this.alert = {
          text: 'Action is too fast. Please wait for a moment.',
          type: 'warning',
        };
        return;
      }

      this.alert = {
        text: 'Please wait.',
        type: 'info'
      }

      this.tableActionDone = false;
      const index = this.tableData.findIndex(rec => rec.id === action.record.id);
      if (index === undefined) {
        this.alert = {
          text: `Error: Failed to delete, record not found.`,
          type: 'warning'
        }
        return;
      }

      this.acfService.delete(action.record, this.category)
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe((res: ApiResponse) => {
          this.alert = null;
          this.tableData.splice(index, 1);

          // Trigger table onChanges
          // Revise, improper to map
          this.tableData = this.tableData.map(rec => rec);
          setTimeout(() => {
            this.alert = {
              text: res.message,
              type: res.code ? 'success' : 'danger',
            };
          })
          this.tableActionDone = true;
        });
    }

  }

  toggleModal(close) {
    this.isModal = !close;
  }

  onModalAction(action) {
    const name = flatMapDeep(<ACFField[][]>this.exportACF)[0].value;
    if (action === 'continue') {
      this.acfService.readyExportFile(this.routeState.category, this.exports, name || this.routeState.category);
    }
  }

  add() {
    const invalid = this.acfService.checkInvalidFields(this.acf);
    if (invalid.length) {
      this.alert = {
        type: 'warning',
        // text: `<i class="fas fa-exclamation-circle"></i>
        //         Please resolve invalid fields before adding a new field.`,
        text: `<i class="fas fa-exclamation-circle"></i>
               Resolve invalid field number ${invalid.map(i => (i.index + 1)).join(', ')}`,
      };
      return;
    }
    this.acf.push(cloneDeep(this.acfClone[0]));
    this.acf = this.acf.map(f => f);
    this.initialiseACF()
  }

  onSubmit(event) {
    event.preventDefault();
    this.alert = null;
    const { type, record, category } = this.routeState;
    const action = type === 'edit' ? 'update' : 'create';
    const acf = this.acfService.rebuildACF({
      user: this.userAuthService.getUserSession(),
      config: this.acfConfig,
      fields: this.acf,
      type: action,
      id: record,
    });

    this.acfService.checkInvalidFields(this.acfConfig);
    this.acfService.checkInvalidFields(this.acf);
    if (acf.invalid.length) {
      this.alert = {
        type: 'warning',
        text: `<i class="fas fa-exclamation-circle"></i>
                Please resolve invalid fields before submitting.`,
      };

      return;
    }

    delete acf.invalid;
    this.backendService.post(`${category}/${action}`, acf)
      .subscribe((res: ApiResponse) => {
        this.alert = {
          type: res.code ? 'success' : 'danger',
          text: res.message,
        }
      });
  }

}
