import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

interface ModalConfig {
  title?: string;
  footer?: 'enabled' | 'disabled';
  action?: {
    text?: string;
  }
}

@Component({
  selector: 'cms-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  @Input() config: ModalConfig = {
    footer: 'enabled',
    action: {
      text: 'Submit'
    }
  };
  @Input() title: string;

  @Output() close = new EventEmitter();
  @Output() action = new EventEmitter();
  constructor(
  ) { }

  closeModal() {
    this.close.emit(true);
  }

  modalAction(action) {
    this.action.emit(action)
  }

}
