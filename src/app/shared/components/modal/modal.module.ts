import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { FormsModule } from '@angular/forms';
import { ModalSlideModule } from './modal-slide/modal-slide.module';



@NgModule({
  declarations: [ModalComponent],
  imports: [
    FormsModule,
    CommonModule,
    ModalSlideModule,
  ],
  exports: [
    ModalComponent,
  ]
})
export class ModalModule { }
