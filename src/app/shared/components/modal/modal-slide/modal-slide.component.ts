import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { cloneDeep, differenceWith, isEqual, merge } from 'lodash';
import { AppStateService } from '../../../../services/app-state/app-state.service';

@Component({
  selector: 'cms-modal-slide',
  templateUrl: './modal-slide.component.html',
  styleUrls: ['./modal-slide.component.scss']
})
export class ModalSlideComponent implements OnInit {

  @Input() config: {
    type?: string;
    emitOn?: string;
    index?: number;
    length?: number;
  } = {};
  @Input() data: any;
  @Input() title: string;
  @Input() table: string;
  @Input() prefill?: object;
  @Output() dataEmitter = new EventEmitter<any>();
  @Output() close = new EventEmitter<boolean>();
  private defaultConfig = {
    emitOn: 'change',
    type: 'filter',
  };
  private defaultData = [];

  @HostListener('window:click', ['$event'])
  onClick($event) {
    const isModal = this.el.nativeElement.contains($event.target);
    if (!isModal && !Array.from($event.target?.classList).includes('filter')) {
      this.emitClose();
    }
  }

  constructor(
    private appStateService: AppStateService,
    private el: ElementRef,
  ) { }

  ngOnInit(): void {
    this.config = merge(this.defaultConfig, this.config);
    if (!this.config.type) {
      this.config.type = this.appStateService.getRoutedState().category;
      console.log(`Warning: Modal type not given, using: ${this.config.type}`)
    }
    this.defaultData = cloneDeep(this.data);
    if (this.config.type === 'custom-fields') {
      // this.data = this.data.filter(field => field?.config?.advance_config === true);
    }

    console.log('Processing modal slide: ', cloneDeep(this.data))
  }

  emitData(data?: any) {
    if (this.config.emitOn === 'change') {
      if (!data) {
        this.dataEmitter.emit(this.data);
      } else {
        this.dataEmitter.emit(data);
      }
    }
  }

  disableButton() {
    return !differenceWith(this.data, this.defaultData, isEqual).length;
  }

  emitClick() {
    if (this.config.type === 'custom-fields') {
      this.dataEmitter.emit(this.data);
    } else if (this.config.type === 'filter') {
      this.data.forEach(dataKey => {
        this.dataEmitter.emit(dataKey);
      });
    }
    this.emitClose();
  }

  emitClose() {
    this.close.emit(true);
  }

}
