import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSlideComponent } from './modal-slide.component';
import { FormsModule } from '@angular/forms';
import { TableHelperModule } from '../../../../components/table/table-helper/table-helper.module';
import { AcfModule } from '../../../../components/acf/acf.module';



@NgModule({
  declarations: [ModalSlideComponent],
  imports: [
    CommonModule,
    FormsModule,
    TableHelperModule,
    AcfModule,
  ],
  exports: [ModalSlideComponent],
})
export class ModalSlideModule { }
