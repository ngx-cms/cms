import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from './modal/modal.module';
import { AlertModule } from '../../components/alert/alert.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ModalModule,
    AlertModule,
  ],
  exports: [
    ModalModule,
    AlertModule,
  ]
})
export class SharedComponentsModule { }
