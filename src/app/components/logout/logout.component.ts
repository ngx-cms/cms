import { Component } from '@angular/core';
import { WindowRef } from '../../dom-abstraction/window.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

  constructor(
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
  ) {
    this.userAuthService.logout();
    this.winRef.nativeWindow.location.href = '/login';
  }

}
