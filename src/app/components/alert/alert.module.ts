import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert.component';
import { PipesModule } from '../../pipes/pipes.module';



@NgModule({
  declarations: [AlertComponent],
  imports: [
    CommonModule,
    PipesModule,
  ],
  exports: [
    AlertComponent,
  ]
})
export class AlertModule { }
