import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Alert } from '../../interfaces/alert/alert';

@Component({
  selector: 'cms-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() alert: Alert;
  @Input() type;
  @Input() text: string;
  @Input() version: 'v1' | 'v2' = 'v2';
  @Input() position: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right' = 'bottom-right';

  @Output() close = new EventEmitter();


  public alertClass = '';

  ngOnInit() {
    if (!this.alert && (!this.type || !this.text)) {
      console.error('Failed to load ALERT COMPONENT. Missing type or text');
      return;
    }

    if (!this.text) {
      this.text = this.alert.text;
    }

    if (!this.type) {
      this.type = this.alert.type;
    }

    this.alert = {
      type: this.type,
      text: this.text,
    }

    this.alertClass = `alert-${this.alert.type} ${this.version} ${this.position}`;
  }

  emitAction() {
    this.close.emit(true)
  }
}