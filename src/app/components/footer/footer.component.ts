import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FOOTER, sidebar } from '../../../shared/constants';
import { isValidEmail } from '../../helpers/string.helper';
import { FormFields } from '../../interfaces/custom-fields';
import { Footer } from '../../interfaces/footer.interface';
import { ACFField } from '../acf/acf.interface';

@Component({
  selector: 'cms-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnChanges {

  @Input() footer: Footer = FOOTER;
  public subscribe: ACFField[] = [
    {
      type: 'email',
      label: '',
      name: 'email',
      attributes: [{
        placeholder: 'Enter email address',
      }]
    }
  ]
  

  constructor() {

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes) {
    if (changes.footer) {
      this.footer = changes.footer.currentValue || FOOTER;
    }
  }

  onFieldChange({ field }) {
    const validEmail = isValidEmail(field.value);
  }

}
