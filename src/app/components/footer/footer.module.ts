import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { RouterModule } from '@angular/router';
import { FieldsModule } from '../acf/fields/fields.module';



@NgModule({
  declarations: [FooterComponent],
  imports: [
    CommonModule,
    RouterModule,
    FieldsModule,
  ],
  exports: [
    FooterComponent,
  ]
})
export class FooterModule { }
