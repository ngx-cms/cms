import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { FieldsModule } from '../acf/fields/fields.module';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FieldsModule,
  ],
  exports: [LoginComponent]
})
export class LoginModule { }
