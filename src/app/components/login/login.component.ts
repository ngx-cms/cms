import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';
import { WindowRef } from '../../dom-abstraction/window.service';
import { Alert } from '../../interfaces/alert/alert';

@Component({
  selector: 'cms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  @Input() redirect = true;
  @Output() action = new EventEmitter();

  inputs = [
    {
      label: 'Username',
      name: 'username',
      value: '',
      type: 'text',
    },
    {
      label: 'Password',
      name: 'password',
      value: '',
      type: 'password',
    }
  ];
  alert: Alert = {}
  private window: Window;
  private isDestroyed$ = new Subject<void>();

  constructor(
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
    private route: ActivatedRoute,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  onAction(action) {
    this.action.emit({
      action,
      fields: this.inputs,
    });
  }

}
