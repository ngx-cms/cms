import { Component, Input, OnInit } from '@angular/core';
import { hasKey, removeQueryParams } from '../../helpers/string.helper';
import { RouteState } from '../../interfaces/route-state';
import { AppStateService } from '../../services/app-state/app-state.service';
import { RoutingService } from '../../services/routing/routing.service';

@Component({
  selector: 'cms-back',
  templateUrl: './back.component.html',
  styleUrls: ['./back.component.scss']
})
export class BackComponent implements OnInit {

  @Input() url: string;

  // private routeState: RouteState = {};
  constructor(
    private routingService: RoutingService,
    private appStateService: AppStateService,
  ) { }

  ngOnInit(): void {
  }

  back(): void {
    const routeState: RouteState = this.appStateService.getRoutedState();
    let url = this.url || `cms/${routeState.category}/all`;
    let params = { queryParams: {}};
    if (hasKey(routeState.queryParams) && routeState.queryParams.category) {
      url = `${removeQueryParams(routeState.url)}`;
      params.queryParams = { category: routeState.queryParams.category };
    }
    this.routingService.goto(url, params);
  }

  

}
