import { Component, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { Tooltip } from '../../interfaces/custom-fields';

@Component({
  selector: 'cms-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {

  @Input() tooltip: Tooltip;
  @Input() config: {
    version?: number;
    position?: 'top' | 'right' | 'bottom' | 'left';
    style?: {
      top?: string;
      right?: string;
      bottom?: string;
      left?: string;
      color?: string;
    }
  } = {
    position: 'bottom',
    version: 1,
  }
  id: string = '';
  textId: string = '';

  showTooltip = false;
  tooltipPositions = {};

  @HostListener('window:click', ['$event'])
  onClick($event) {
    if (this.$el.nativeElement) {
      try {
        const tooltip = this.$el.nativeElement.querySelector(`#${$event.target.id || this.generateId()}`);
        if (!tooltip) {
          this.showTooltip = false;
        }
      } catch (e) {}
    }
  }
  constructor(
    private $el: ElementRef
  ) { }

  ngOnInit(): void {
    this.generateId();
  }

  toggleTooltip($event?: any) {
    if (this.id === $event?.target.id || this.textId === `t${$event.target.id}`) {
      this.showTooltip = !this.showTooltip;
    }
  }

  generateId() {
    this.id = `tooltip-${Date.now() + Math.round(1E9)}`;
    this.textId = `t${this.id}`;
    return this.id;
  }

  getPositionClass(): string {
    let classes = this.config?.position;
    const title = this.tooltip?.title;

    switch (this.config?.position) {
      case 'top':
        return `${classes} bottom-arrow ${!title ? 'mb-1' : ''}`;
      case 'right':
        return `${classes} left-arrow ${!title ? 'ml-1' : ''}`;
      case 'bottom':
        return `${classes} top-arrow ${!title ? 'mt-1' : ''}`;
      case 'left':
        return `${classes} right-arrow ${!title ? 'mr-1' : ''}`;
      default:
        return classes;
    }
  }

}
