import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipComponent } from './tooltip.component';
import { PipesModule } from '../../pipes/pipes.module';



@NgModule({
  declarations: [
    TooltipComponent,
  ],
  imports: [
    CommonModule,
    PipesModule,
  ],
  exports: [
    TooltipComponent,
  ]
})
export class TooltipModule { }
