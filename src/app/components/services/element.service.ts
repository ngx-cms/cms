import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { DocumentRef } from '../../dom-abstraction/document.service';
import { addTrailingSlash } from '../../helpers/string.helper';

interface AddElementAttrs {
  name: string;
  value: string;
}

@Injectable({
  providedIn: 'root'
})
export class ElementService {

  private document: Document;
  private renderer: Renderer2;

  constructor(
    private documentRef: DocumentRef,
    private rendererFactory: RendererFactory2,
  ) {
    this.document = documentRef.nativeDocument;
    this.renderer = rendererFactory.createRenderer(this.document, null);
  }

  addElement(tag: string, attrs: AddElementAttrs | AddElementAttrs[]) {
    const el = this.renderer.createElement(tag);
    if (typeof attrs === 'object' && !Array.isArray(attrs)) {
      attrs = [attrs];
    }

    attrs.forEach((attr) => {
      // ensures href has a trailing slash
      if (attr.name === 'href' && attr.value) {
        attr.value = addTrailingSlash(attr.value);
      }
      this.renderer.setAttribute(el, attr.name, attr.value);
    });
    this.renderer.appendChild(this.document.head, el);
  }
}
