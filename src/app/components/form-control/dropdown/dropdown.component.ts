import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WindowRef } from '../../../dom-abstraction/window.service';

@Component({
  selector: 'cms-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  @Input() options: any;
  @Input() dropdown = {
    width: 'relative',
    display: 'label',
  };
  @Output() output = new EventEmitter();

  public isOpen = false;
  public label = '';
  public dropdownWidth = 'unset';
  public displayTexts = [];
  public position: 'top' | 'bottom' = 'bottom';
  private window: Window;

  constructor(
    private el: ElementRef,
    private winRef: WindowRef,
  ) {
    this.window = winRef.nativeWindow;
  }

  isVisible(el) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;

    return ((elemTop - 15 >= 0) && (elemBottom <= this.window.innerHeight));
  }

  ngOnInit(): void {
  }
  emit(field) {
    this.output.emit(field);
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
    }
  }

  itemClick(option) {
    this.label = option.label;
    this.emit(option);
  }
}
