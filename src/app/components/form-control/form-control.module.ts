import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from './inputs/inputs.module';
import { DropdownModule } from './dropdown/dropdown.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputsModule,
    DropdownModule,
  ],
  exports: [
    InputsModule,
    DropdownModule,
  ]
})
export class FormControlModule { }
