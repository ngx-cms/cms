import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { FormFields, FormControlInputs } from '../../../../interfaces/custom-fields';
import { wysiwygConfig } from './wysiwyg.config';

@Component({
  selector: 'cms-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss']
})
export class TextAreaComponent implements OnInit {

  @HostListener('keyup', ['$event'])
  onKeyUp($event) {
    this.onFieldChange(this.field, $event);
  }
  @Input() field: FormFields;
  @Input() inputs: FormControlInputs;
  @Output() emitField = new EventEmitter<{
    field: FormFields;
    $event: Event;
  }>();


  public wysiwygConfig = wysiwygConfig;
  public wysiwyg = {
    config: wysiwygConfig,
  }

  ngOnInit() {
    if (this.field.type === 'dynamic_content') {

    }
  }

  onFieldChange(field, $event) {
    this.emitField.emit({
      field,
      $event,
    });
  }

}
