import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextAreaComponent } from './text-area.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DirectivesModule } from '../../../../directives/directives.module';



@NgModule({
  declarations: [TextAreaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    AngularEditorModule,
  ],
  exports: [TextAreaComponent],
})
export class TextAreaModule { }
