import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputsComponent } from './inputs.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberModule } from './number/number.module';
import { TextModule } from './text/text.module';
import { TextAreaModule } from './text-area/text-area.module';
import { CheckboxModule } from './checkbox/checkbox.module';
import { RadioModule } from './radio/radio.module';
import { DropdownModule } from '../dropdown/dropdown.module';
import { DirectivesModule } from '../../../directives/directives.module';



@NgModule({
  declarations: [InputsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    NumberModule,
    TextModule,
    CheckboxModule,
    TextAreaModule,
    RadioModule,
    DropdownModule,
  ],
  exports: [
    InputsComponent,
    NumberModule,
    TextModule,
    TextAreaModule,
    CheckboxModule,
    DropdownModule,
  ]
})
export class InputsModule { }
