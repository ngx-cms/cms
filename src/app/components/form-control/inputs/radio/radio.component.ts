import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControlInputs, FormFields } from '../../../../interfaces/custom-fields';

@Component({
  selector: 'cms-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {

  @Input() field: FormFields;
  @Input() inputs: FormControlInputs;
  @Input() fieldIndex: number;
  @Output() emitField = new EventEmitter<{
    field: FormFields;
    $event: Event;
  }>();

  fieldValue: any;

  ngOnInit() {
    this.field.options.forEach(option => {
      option['id'] = `${this.field.attributes}-${option.value}`
    });
  }

  createId(field, option) {
    return `${field?.attributes?.id}-${option?.value}`;
  }

  isInline(field) {
    return field?.classes?.indexOf('inline') !== -1;
  }

  onFieldChange(field, $event) {
    field.value = $event;
    this.emitField.emit({
      field,
      $event,
    });
  }

}
