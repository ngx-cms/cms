import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormFields } from '../../../../interfaces/custom-fields';

@Component({
  selector: 'cms-input-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.scss']
})
export class NumberComponent {

  @Input() field: FormFields;
  @Input() inputs: FormControlInputs;
  @Output() emitField = new EventEmitter<{
    field: FormFields;
    $event: Event;
  }>();

  onFieldChange(field, $event) {
    this.emitField.emit({
      field,
      $event,
    });
  }

}
