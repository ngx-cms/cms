import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControlInputs, FormFields } from '../../../../interfaces/custom-fields';

@Component({
  selector: 'cms-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {

  @Input() field: FormFields;
  @Input() inputs: FormControlInputs;
  @Output() emitField = new EventEmitter<{
    field: FormFields;
    $event: Event;
  }>();

  onFieldChange(field, $event) {
    this.emitField.emit({
      field,
      $event,
    });
  }

}
