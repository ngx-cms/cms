import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { isValidEmail } from '../../../helpers/string.helper';
import { FormControlData, FormControlInputs } from '../../../interfaces/custom-fields';
import { FormControlService } from '../../../services/form-control/form-control.service';

@Component({
  selector: 'cms-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit, OnChanges {

  @Input() inputs: FormControlInputs = {};
  @Output() emitInputs = new EventEmitter<FormControlData>();

  constructor(private formControlService: FormControlService) {}

  ngOnInit() {
    this.setInputValues();
  }

  setInputValues() {
    if (!this.inputs?.name) {
      this.inputs.field.id = this.inputs.name = `${this.inputs.field.name}_${this.inputs.field_index}`;
    }

    if (!this.inputs.classes) {
      this.inputs.classes = this.formControlService.getFieldClasses(this.inputs.field);
    }
  }

  ngOnChanges(changes) {
    if (changes.inputs) {
      this.setInputValues();
    }
  }

  emitInput(inputs): void {
    // const isValid = this.validateFieldValue(inputs.field);
    // inputs.field.valid = this.validateFieldValue(inputs.field);
    this.emitInputs.emit(inputs);
  }

  validateFieldValue(field): boolean {
    switch (field.type) {
      case 'email':
        return this.isValidEmail(field);
      case 'password':
        return this.validatePassword(field);
      default:
        return true;
    }
  }

  isValidEmail(field): boolean {
    return isValidEmail(field.value);
  }

  validatePassword(field): boolean {
    return field.value.length > 6;
  }

}
