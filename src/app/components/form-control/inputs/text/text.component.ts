import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormFields, FormControlInputs } from '../../../../interfaces/custom-fields';

@Component({
  selector: 'cms-input-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent {

  @Input() field: FormFields;
  @Input() inputs: FormControlInputs;
  @Output() emitField = new EventEmitter<{
    field: FormFields;
    $event: Event;
  }>();

  onFieldChange(field, $event) {
    this.emitField.emit({
      field,
      $event,
    });
  }

}
