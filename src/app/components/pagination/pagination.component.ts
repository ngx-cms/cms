import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'cms-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() pagination: any;
  @Input() active: number;
  @Output() action = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  setActivePage(page, n = 0) {
    if (page < 0 || page > (this.pagination.pages - 1)) {
      return;
    }
    // this.checkAll.nativeElement.checked = false
    this.active = page;
    this.action.emit({
      type: 'page',
      active: this.active,
    });
  }

}
