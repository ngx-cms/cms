import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cms-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() card: {
    label: string;
    data: string;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
