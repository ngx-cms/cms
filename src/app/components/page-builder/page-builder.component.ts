import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { keys, ucWords } from '../../helpers/string.helper';

@Component({
  selector: 'cms-page-builder',
  templateUrl: './page-builder.component.html',
  styleUrls: ['./page-builder.component.scss']
})
export class PageBuilderComponent implements OnInit {

  @Input() components: any[] = [];
  @Output() onToolSelect = new EventEmitter();
  public tools = [];

  constructor() { }

  ngOnInit(): void {
    this.components = this.components
      .map(comp => {
        return {
          name: comp,
          label: `${ucWords(comp.replace(/_/gi, ' '))} (${comp})`
        }
      })
  }

  toolSelect(tool) {
    this.onToolSelect.emit(tool);
  }

}
