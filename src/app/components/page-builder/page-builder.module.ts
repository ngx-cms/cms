import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageBuilderComponent } from './page-builder.component';



@NgModule({
  declarations: [
    PageBuilderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PageBuilderComponent,
  ]
})
export class PageBuilderModule { }
