import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WysiwygComponent } from './wysiwyg.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DirectivesModule } from '../../directives/directives.module';



@NgModule({
  declarations: [WysiwygComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularEditorModule,
    DirectivesModule,
  ],
  exports: [WysiwygComponent],
})
export class WysiwygModule { }
