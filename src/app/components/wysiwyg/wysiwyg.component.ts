import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { WindowRef } from '../../dom-abstraction/window.service';
import { ACFField } from '../acf/acf.interface';
import { wysiwygConfig } from './wysiwyg.config';

@Component({
  selector: 'cms-wysiwyg',
  templateUrl: './wysiwyg.component.html',
  styleUrls: ['./wysiwyg.component.scss']
})
export class WysiwygComponent implements OnInit {

  @Input() field: ACFField;
  @Input() inputs: any;

  @Output() output = new EventEmitter();

  public wysiwygConfig = wysiwygConfig;
  public wysiwyg = {
    config: wysiwygConfig,
  }

  private window: Window;

  constructor(
    private winRef: WindowRef,
  ) {
    this.window = winRef.nativeWindow;
  }

  ngOnInit(): void {
  }

  onFieldChange(field, $event) {
    this.output.emit({
      field,
      $event,
    });
  }
}
