import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadersComponent } from './loaders.component';
import { PlaceholderLoaderComponent } from './placeholder-loader/placeholder-loader.component';



@NgModule({
  declarations: [LoadersComponent, PlaceholderLoaderComponent],
  imports: [
    CommonModule,
  ],
  exports: [
    LoadersComponent,
    PlaceholderLoaderComponent,
  ]
})
export class LoadersModule { }
