import { Component, Input, OnInit } from '@angular/core';
import { config } from 'rxjs';

@Component({
  selector: 'cms-placeholder-loader',
  templateUrl: './placeholder-loader.component.html',
  styleUrls: ['./placeholder-loader.component.scss']
})
export class PlaceholderLoaderComponent implements OnInit {

  @Input() config: {
    box?: number;
    circle?: number;
    filters?: number;
  } = {
    box: 3,
  };
  @Input() type: 'default' | 'table' | 'article' = 'default';

  box = [];
  filters = new Array(2);

  constructor() { }

  ngOnInit(): void {
    this.box.length = this.config?.box;
    if (this.config.filters) {
      this.filters = new Array(this.config.filters)
    }
  }

}
