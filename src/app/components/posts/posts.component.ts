import { Component, OnInit } from '@angular/core';
import { CustomFieldConfig, FormFields } from '../../interfaces/custom-fields';

@Component({
  selector: 'cms-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  routeState = {};
  customFields: FormFields[][];
  customFieldsConfig: CustomFieldConfig = {
    development: false,
  };

  constructor(
  ) { }

  ngOnInit(): void {
  }

}
