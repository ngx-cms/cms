import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { navbar } from '../../../shared/constants';
import { User } from '../../interfaces/user/user';
import { AppStateService } from '../../services/app-state/app-state.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Component({
  selector: 'cms-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  @Input() type: string = 'navbar'
  @Input() user: User = {};
  @Input() config: any;

  @Output() userState = new EventEmitter();
  @Output() action = new EventEmitter();


  public openModal = false;
  public customContent = true;
  public isMobile: boolean;

  private isDestroyed$ = new Subject<void>();

  showUserMenu = false;
  navbar = {
    user: {
      menu: navbar.userMenu,
    }
  }

  @HostListener('document:click', ['$event'])
  onClick($event) {
    if (!$event.path[0].classList.contains('user-span')) {
      this.showUserMenu = false;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize($event) {
    this.isMobile = this.appStateService.isMobile();
  }

  @ViewChild('NavbarCustomContent') customContentEl: ElementRef;

  constructor(
    private userAuth: UserAuthService,
    private appStateService: AppStateService,
  ) { }

  ngOnInit(): void {
    this.isMobile = this.appStateService.isMobile();
    if (!this.user) {
      this.userAuth.session()
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe(res => {
          this.user = res;
          this.userState.emit(res);
        });
    }
  }

  ngOnDestroy() {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }

  menuStatus() {
    this.showUserMenu = !this.showUserMenu;
  }

  getName() {
    // return `${this.user?.first_name}`
  }

  toggleCustomContent(action) {
    this.action.emit(action);
  }

}
