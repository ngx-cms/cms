import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavTileComponent } from './nav-tile.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    NavTileComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    NavTileComponent,
  ]
})
export class NavTileModule { }
