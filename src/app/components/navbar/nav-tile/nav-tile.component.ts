import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SidebarNavs } from '../../../interfaces/sidebar';

@Component({
  selector: 'cms-nav-tile',
  templateUrl: './nav-tile.component.html',
  styleUrls: ['./nav-tile.component.scss']
})
export class NavTileComponent implements OnInit {

  @Input() nav: SidebarNavs;
  @Input() isMinimize: boolean;
  @Input() activeChild: boolean;
  @Input() navType: 'parent' | 'child' = 'parent';
  @Input() lastActive: string;
  @Output() childNavClick = new EventEmitter<any>();
  @Output() linkClicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  emitCaretClick(nav, $event) {
    this.childNavClick.emit({
      nav,
      isOpen: true,
      $event,
    })
  }

  routeChanged(url, change) {
    this.linkClicked.emit(url);
  }

}
