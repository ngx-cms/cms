import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavbarComponent } from './side-navbar.component';
import { NavTileModule } from '../nav-tile/nav-tile.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SideNavbarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NavTileModule,
  ],
  exports: [
    SideNavbarComponent,
  ]
})
export class SideNavbarModule { }
