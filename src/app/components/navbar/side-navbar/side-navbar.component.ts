import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { sidebar } from '../../../../shared/constants';
import { cloneDeep } from '../../../helpers/string.helper';
import { SidebarNavs } from '../../../interfaces/sidebar';
import { RoutingService } from '../../../services/routing/routing.service';

@Component({
  selector: 'cms-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent implements OnChanges {

  @Input() isMobile = false;
  @Input() category: string;
  @Input() navCollections: any[] = [];

  @Output() minimize = new EventEmitter<any>();
  @Output() sidebarLoaded = new EventEmitter<boolean>();
  @Output() linkClicked = new EventEmitter();

  sidebar = sidebar;
  navs: SidebarNavs[] = cloneDeep(sidebar.navs);
  activeChild?: string | null;
  lastActive?: string | null;
  config: {
    icon_only?: boolean;
  } = {
    icon_only: false,
  };
  isMinimize = false;
  iconClickable = false;

  constructor(
    private routingService: RoutingService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.navCollections) {
      this.navCollections = changes.navCollections.currentValue;
      this.navs = cloneDeep(sidebar.navs);
      this.initNavs();
    }
  }
  
  initNavs() {
    this.navCollections = this.navCollections.map(col => {
      col.children = true;
      return {
        icon: col.icon,
        label: col.label,
        url: col.url,
        children: this.routingService.generateChildNav(col),
      }
    });
    this.navs = this.navs.map(nav => {
      if (!nav.event) {
        nav.url = `/cms/${nav.url}`;
      }

      if (!nav.url) {
        nav.url = nav.label.toLowerCase();
      }

      if (nav.event) {
        this.triggerChild(nav, false, { type: 'init', });
      }

      nav.children = this.routingService.generateChildNav(nav);
      return nav;
    });
    const siteOptionsIdx = this.navs.findIndex(nav => nav.url === 'site-options');
    this.navs.splice(siteOptionsIdx, 0, ...this.navCollections);
    this.activeChild = `/cms/${this.category}`;
  }

  toggleMenu(nav, params?: object) {
    const paramKeys = Object.keys(params);
    const value = nav?.event?.value;

    if (!this.isMobile) {
      this.isMinimize = value;
      this.iconClickable = value;
      paramKeys.forEach(paramKey => {
        const eventObject = this.navs.find(nav => nav[paramKey] === params[paramKey]);
        eventObject.is_hidden = false;
        nav.is_hidden = true;
      });
    }

    this.minimize.emit(value);
  }

  childNavClick(nav) {
    this.triggerChild(nav?.nav, nav?.isOpen, nav?.$event);
  }

  routeChanged(url) {
    this.linkClicked.emit(true);
  }

  triggerChild(nav: SidebarNavs, isCaretClick = false, $event?: { type: string; }): any {

    if (nav.event) {
      if ($event && $event.type === nav.event.type) {
        this[nav.event.function](nav, nav.event.params);
      }
      return;
    }

    if (!nav.children) {
      return;
    }

    if (this.isMinimize && !isCaretClick) {
      this.lastActive = this.activeChild;
      if (this.activeChild && !this.lastActive) {
        this.activeChild = null;
        return;
      }
    }

    if (isCaretClick || this.lastActive === nav.url) {
      this.lastActive = this.activeChild;
      this.activeChild = null;
      return;
    }

    if (this.activeChild === nav.url) {
      return;
    }

    this.activeChild = nav.url;
  }

}
