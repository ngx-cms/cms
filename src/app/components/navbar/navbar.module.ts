import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { RouterModule } from '@angular/router';
import { NavTileModule } from './nav-tile/nav-tile.module';
import { SideNavbarModule } from './side-navbar/side-navbar.module';
import { LoginModule } from '../login/login.module';
import { ModalModule } from '../../shared/components/modal/modal.module';



@NgModule({
  declarations: [
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SideNavbarModule,
    NavTileModule,
    ModalModule,
    LoginModule,
  ],
  exports: [
    NavbarComponent,
  ]
})
export class NavbarModule { }
