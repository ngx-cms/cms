import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'cms-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() type: 'button' | 'submit' = 'button';
  @Input() color: 'info' | 'warning' | 'success' | 'danger' = 'info';
  @Input() size: 'sm' | 'md' | 'lg' | 'xl';

}
