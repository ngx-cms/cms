export interface TableSort {
  order?: 'ascending' | 'descending';
  active?: string;
}