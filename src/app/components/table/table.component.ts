import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { isEqual } from '../../helpers/string.helper';
import { TableActions, TableConfig, TableHelpers } from '../../interfaces/table';
import { TableService } from '../../services/table/table.service';
import { TABLE_ACTIONS, TABLE_CONFIG, TABLE_HELPERS, TABLE_HELPERS_QUICK_SEARCH } from './table.config';
import { TableSort } from './table.interface';


@Component({
  selector: 'cms-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnChanges, AfterViewInit {

  @Input() tableData: any[];
  @Input() tableConfig: TableConfig;
  @Input() pagination;
  @Input() sort: TableSort = {};
  @Output() action = new EventEmitter<any>();
  @ViewChild('checkAll') checkAll: ElementRef;

  public isFilter: boolean = false;
  public tableHelpers: TableHelpers[] = TABLE_HELPERS;
  public tableHelperQuickSearch = TABLE_HELPERS_QUICK_SEARCH;
  public selected = [];
  public TABLE_ACTIONS = TABLE_ACTIONS;
  public lastActive = null;
  public active = 0;

  private tableDataCopy: any[];
  private defaultConfig = TABLE_CONFIG;
  private paginationDefaults = {
    enabled: true,
    record_per_page: 10,
    records: [],
    pages: 0,
    active: false,
    position: {
      fixed: true,
      align: 'left',
    },
    numbers_only: false,
  };

  @ViewChild('table') tableEl: ElementRef;


  constructor(
    private tableService: TableService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    const { tableConfig, tableData, pagination } = changes;
    if (tableData && !tableData.firstChange) {
      this.tableData = tableData.currentValue;
      if (tableConfig) {
        this.tableConfig = tableConfig.currentValue;
      }
      if (pagination) {
        this.pagination = pagination?.currentValue;
      }
    }
    this.setupTable();
  }

  ngAfterViewInit() {
    if (this.tableConfig.position === 'fixed') {
      const table = this.tableEl.nativeElement;
      this.tableEl.nativeElement.style.height = `${table.clientHeight}px`;
    }
  }

  setupTable() {
    this.tableConfig = Object.assign({}, this.defaultConfig, this.tableConfig);
    this.tableDataCopy = this.clone(this.tableData);

    // Remove should stateless
    this.tableHelpers = this.tableHelpers.map(helper => {
      if (helper.name === 'column') {
        helper.options = [...this.tableConfig.headers];
        helper.value = null;
      }

      return helper;
    })
    if (!this.pagination || !this.pagination.enabled) {
      this.pagination = {};
      this.pagination.records_per_page = (this.tableData.length + 1);
    }
    this.pagination = Object.assign({}, this.paginationDefaults, (this.pagination || {}));
    this.pagination.enabled = this.tableConfig.pagination;
    this.initPagination();
  }

  setActivePage(page, n = 0) {
    if (page < 0 || page > (this.pagination.pages - 1)) {
      return;
    }
    this.checkAll.nativeElement.checked = false
    this.active = page;
  }

  initPagination(tableData = this.tableDataCopy) {
    this.pagination.records = [];
    let { record_per_page, records } = this.pagination;
    let maxLen = record_per_page;
    let page = 0;
    tableData.forEach((rec, i) => {
      // Initialise page to array to
      // avoid undefined on pushing rec
      if (!records[page]) {
        records[page] = [];
      }

      // Push record to page
      if (rec) {
        records[page].push(rec);
      }

      // Add page whenever the record length per page is equal to rec index
      if (i === (maxLen - 1) && this.tableConfig.pagination === true) {
        page += 1;
      }

      // Pagination buttons
      if (i < maxLen) {
        if ((i === (maxLen - 1))) {
          maxLen += record_per_page;
        }
      }
    });

    this.pagination.pages = records.length
    this.pagination.active = true;
  }

  onSelect(event, record) {
    const isChecked = event.target?.checked;
    if (Array.isArray(record)) {
      record.forEach(rec => {
        rec.selected = isChecked;
        if (!isChecked) {
          // this.selected = [];
          this.selected.splice(this.selected.findIndex(item => isEqual(item, rec)), 1)
        } else {
          if (!this.selected.includes(rec)) {
            this.selected.push(rec);
          }
        }
      });
    } else {
      record.selected = isChecked;
      if (isChecked) {
        this.selected.push(record);
      } else {
        const recIndex = this.selected.findIndex(rec => isEqual(rec, record));
        this.selected.splice(recIndex, 1);
      }
    }

    this.action.emit({
      record: this.selected,
      action: isChecked ? 'select' : 'unselect',
    });
  }

  onPaginationAction(action) {
    this.setActivePage(action.active)
  }

  clone(obj) {
    const type = Array.isArray(obj) ? [] : {};
    return Object.assign(type, obj);
  }

  onAction(record, action: TableActions) {
    this.action.emit({
      record: this.selected.length ? this.selected : record,
      action,
    });
  }

  toggleFilter(changes) {
    this.isFilter = !this.isFilter;
  }

  onFilter(filter) {
    this.tableData = this.tableService.filters(
      this.tableDataCopy,
      this.tableConfig.headers,
      filter || this.tableHelpers
    );

    this.initPagination(this.tableData);
  }

}
