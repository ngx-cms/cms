import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'cms-table-helper',
  templateUrl: './table-helper.component.html',
  styleUrls: ['./table-helper.component.scss']
})
export class TableHelperComponent implements OnInit, OnChanges {

  @Input() helper: any;
  @Output() helperEmit = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    if (this.helper.type === 'select') {
      if (this.helper?.options?.length === 1) {
        this.helper.value = this.helper.options[0];
      }
    }
  }

  emitHelper(helper) {
    this.helperEmit.emit(helper);
  }

  inputHelperClass(helper) {
    const nonTextInput = [
      'checkbox',
      'radio',
    ];

    return !nonTextInput.includes(helper?.type) ? 'd-inline form-control form-control-sm' : 'form-check-input';
    // return !nonTextInput.includes(helper.type) ? ;
  }

  ngOnChanges(changes) {
    if (changes.helper) {
      this.helper = changes.helper.currentValue;
    }
  }

}
