import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableHelperComponent } from './table-helper.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [TableHelperComponent],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    TableHelperComponent
  ]
})
export class TableHelperModule { }
