import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { ModalSlideModule } from '../../shared/components/modal/modal-slide/modal-slide.module';
import { TooltipModule } from '../tooltip/tooltip.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsModule } from '../acf/fields/fields.module';
import { PipesModule } from '../../pipes/pipes.module';
import { PaginationModule } from '../pagination/pagination.module';



@NgModule({
  declarations: [TableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalSlideModule,
    PipesModule,
    TooltipModule,
    FieldsModule,
    PaginationModule,
  ],
  exports: [
    TableComponent,
  ]
})
export class TableModule { }
