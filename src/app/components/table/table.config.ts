import { TableConfig, TableHelpers } from "src/app/interfaces/table";
import { sidebar, userRoles } from "src/shared/constants";
import { ucFirst } from "../../helpers/string.helper";
import { TableSort } from "./table.interface";

export const TABLE_HELPERS: TableHelpers[] = [
  {
    type: 'search',
    label: 'Search',
    name: 'search',
    transform: 'findKey',
  },
  {
    type: 'select',
    label: 'Column',
    name: 'column',
    value: null,
    display: 'label',
  },
  {
    type: 'checkbox',
    label: 'Case Sensitive',
    name: 'case_sensitive',
    value: false,
  }
];

export const TABLE_HELPERS_QUICK_SEARCH: TableHelpers[] = [
  {
    type: 'text',
    label: '',
    name: 'search',
    placeholder: 'Quick Search'
  },
];

export const TABLE_CONFIG: TableConfig = {
  actions: {},
  sortable: true,
  enable_quick_search: true,
  headers: [],
  position: 'fixed',
  pagination: true,
};

export const TABLE_ACTIONS = [
  {
    label: 'Export',
    class: 'btn-info',
    action: 'export',
    transform: (action) => {
      return action === true;
    }
  },
  {
    label: 'View Details',
    action: 'view',
    class: 'btn-success',
    condition: false,
  },
  {
    label: 'Edit',
    action: 'edit',
    class: 'btn-success',
    condition: false,
  },
  {
    label: 'Delete',
    action: 'delete',
    class: 'btn-danger',
    condition: false,
  },
]

export const TABLE_CONFIG_FORM_FIELDS: TableConfig = {
  headers: [
    {
      value: 'form_name',
      label: 'Form Group'
    },
    {
      value: 'label',
      label: 'Field'
    },
    {
      value: 'is_permanent',
      label: 'Permanent',
      transform: (e) => e === true ? 'Yes' : 'No',
      tooltip: {
        text: 'Permanent Form fields cannot be deleted',
      }
    }
  ],
}

interface TableConfigDefault {
  [categories: string]: TableConfig;
}

export const TABLE_CONFIG_DEFAULT: TableConfigDefault = {
  pages: {
    ...TABLE_CONFIG,
    headers: [
      {
        value: 'title',
        label: 'Title',
      },
      {
        value: 'url',
        label: 'URL',
        html: true,
        transform: (url, config) => {
          console.log('config: ', config)
          const category = (
            config?.page_category && config?.page_category !== 'none'
              ? config?.page_category
              :'article'
            );
          return `<a href="${category}/${url}/" target="_blank">${url}</a>`
        },
      },
      {
        value: 'page_category',
        label: 'Category',
      },
      {
        value: 'content',
        label: 'Content',
      },
    ],
    actions: {
      edit: (res) => {
      }
    }
  },
  users: {
    headers: [
      {
        value: 'username',
        label: 'Username',
      },
      {
        value: 'role',
        label: 'Role',
        transform: (rec) => `${userRoles[rec]} (${rec})`,
      }
    ]
  },
  customfields: {
    headers: TABLE_CONFIG_FORM_FIELDS.headers,
  }
}

export const TABLE_CONFIG_DATABASE: TableConfig = {
  headers: [
    {
      label: 'Collection',
      value: 'collection',
      html: true,
      transform: (record) => {
        if (Array.isArray(record)) {
          return record.map(rec => ucFirst(rec));
        }
        
        if (sidebar.navs.some(nav => nav.url === record)) {
          return `<a href="/cms/${record}/all">${ucFirst(record)}</a>`;
        }
        
        return record;

      }
    },
    {
      label: 'Documents',
      value: 'documents',
      transform: (record) => {
        if (Array.isArray(record)) {
          return record.length;
        }

        return;
      }
    }
  ],
  actions: {
    edit: (args, action) => ActionHelper(args, action),
    delete: (args, action) => ActionHelper.bind(this, args, action)
  }
}


export function ActionHelper(args, action) {
  if (!action?.type) {
    return true;
  }
  if (action.type === 'delete') {
    if (args.user.role === 1) {
      return true;
    }
  }

  if (action.type === 'edit') {
    if (args.user.role <= 2) {
      return true;
    }
  }

  return false;
}