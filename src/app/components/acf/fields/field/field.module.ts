import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldComponent } from './field.component';
import { InputTextModule } from '../input-text/input-text.module';
import { SelectModule } from '../select/select.module';
import { InputNumberModule } from '../input-number/input-number.module';
import { InputCheckboxModule } from '../input-checkbox/input-checkbox.module';
import { RadioGroupsModule } from '../radio-groups/radio-groups.module';
import { TextareaModule } from '../textarea/textarea.module';
import { InputDisabledModule } from '../input-disabled/input-disabled.module';
import { ButtonModule } from '../../../button/button.module';
import { WysiwygModule } from '../../../wysiwyg/wysiwyg.module';
import { FieldHelpersModule } from '../field-helpers/field-helpers.module';



@NgModule({
  declarations: [FieldComponent],
  imports: [
    CommonModule,
    InputTextModule,
    InputNumberModule,
    InputCheckboxModule,
    TextareaModule,
    RadioGroupsModule,
    SelectModule,
    ButtonModule,
    InputDisabledModule,
    WysiwygModule,
    FieldHelpersModule,
  ],
  exports: [FieldComponent],
})
export class FieldModule { }
