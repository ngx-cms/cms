import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ACFField, FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {

  @Input() field: ACFField;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();
  @Output() action = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.inputs = {
      ...this.inputs,
      field_name: `${this.inputs?.fieldsIndex}-${this.field.name}-${this.inputs?.field_name}-${this.inputs?.oIndex}`,
    };
  }

  onFieldChange(field) {
    this.output.emit(field);
  }

  onFieldAction(action) {
    this.action.emit(action);
  }

}
