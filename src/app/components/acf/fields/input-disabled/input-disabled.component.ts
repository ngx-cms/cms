import { Component, Input } from '@angular/core';

@Component({
  selector: 'cms-input-disabled',
  templateUrl: './input-disabled.component.html',
  styleUrls: ['./input-disabled.component.scss']
})
export class InputDisabledComponent {
  @Input() field: any;
}
