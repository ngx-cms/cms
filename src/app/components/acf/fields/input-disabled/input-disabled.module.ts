import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputDisabledComponent } from './input-disabled.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldModule } from '../field/field.module';
import { FieldsModule } from '../fields.module';



@NgModule({
  declarations: [InputDisabledComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [InputDisabledComponent]
})
export class InputDisabledModule { }
