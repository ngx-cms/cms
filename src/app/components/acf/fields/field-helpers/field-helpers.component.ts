import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ACFField } from '../../acf.interface';

@Component({
  selector: 'cms-field-helpers',
  templateUrl: './field-helpers.component.html',
  styleUrls: ['./field-helpers.component.scss']
})
export class FieldHelpersComponent implements OnInit {

  @Input() field: ACFField;
  @Output() action = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onFieldAction(field, action) {
    this.action.emit({field, action});
  }

}
