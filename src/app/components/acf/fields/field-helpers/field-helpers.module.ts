import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldHelpersComponent } from './field-helpers.component';



@NgModule({
  declarations: [FieldHelpersComponent],
  imports: [
    CommonModule
  ],
  exports: [FieldHelpersComponent],
})
export class FieldHelpersModule { }
