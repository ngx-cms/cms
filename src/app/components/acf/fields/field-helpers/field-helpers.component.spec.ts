import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldHelpersComponent } from './field-helpers.component';

describe('FieldHelpersComponent', () => {
  let component: FieldHelpersComponent;
  let fixture: ComponentFixture<FieldHelpersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FieldHelpersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldHelpersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
