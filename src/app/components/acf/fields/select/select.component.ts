import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { WindowRef } from '../../../../dom-abstraction/window.service';
import { cloneDeep } from '../../../../helpers/string.helper';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Input() dropdown = {
    width: 'relative',
    display: 'both',
  };
  @Output() output = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
    onClick(target) {
      if (!this.el.nativeElement.contains(target)) {
        this.isOpen = false;
      }
    }

  @ViewChild('dropdownMenu') dropdownMenu;
  @ViewChild('button') button;

  public isOpen = false;
  public label = '';
  public dropdownWidth = 'unset';
  public displayTexts = [];
  public position: 'top' | 'bottom' = 'bottom';
  private window: Window;

  constructor(
    private el: ElementRef,
    private winRef: WindowRef,
  ) {
    this.window = winRef.nativeWindow;
  }

  isVisible(el) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;

    return ((elemTop - 15 >= 0) && (elemBottom <= this.window.innerHeight));
  }

  ngOnInit(): void {
    if (typeof this.field.options === 'string') {
      this.field.options = [];
    }
    if (this.field.display) {
      this.dropdown.display = this.field.display;
    }
    if (this.field.options?.length) {
      this.setDisplayText();
    }
  }

  setDisplayText() {
    if (this.field.value) {
      const label = this.field.options.find(option => option.value === this.field.value);
      const { display } = this.dropdown;
      if (label) {
        if (display === 'both') {
          this.label = `${label.label} (${label.value})`;
        } else {
          this.label = label[display];
        }
      }
    }

    return this.label;
  }

  getLabel() {
    if (!this.label) {
      this.setDisplayText();
    }
    return this.label;
  }

  emit(field) {
    this.output.emit(field);
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      if (this.dropdown.width === 'relative') {
        this.dropdownWidth = `${this.button.nativeElement.clientWidth}px`;
      }
    }
  }

  itemClick(option) {
    this.field.value = option.value;
    this.label = option.label;
    this.isOpen =  false;
    this.setDisplayText();
    this.emit(this.field);
  }

}
