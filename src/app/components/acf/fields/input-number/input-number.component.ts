import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss']
})
export class InputNumberComponent implements OnInit {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emit(field) {
    this.output.emit(field);
  }

}
