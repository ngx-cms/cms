import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  styleUrls: ['./input-checkbox.component.scss']
})
export class InputCheckboxComponent implements OnInit {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emit(field, el) {
    field.value = Boolean(el.checked);
    this.output.emit(field);
  }

}
