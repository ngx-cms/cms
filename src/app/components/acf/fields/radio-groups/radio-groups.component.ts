import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-radio-groups',
  templateUrl: './radio-groups.component.html',
  styleUrls: ['./radio-groups.component.scss']
})
export class RadioGroupsComponent {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  constructor() { }

  emit(value) {
    this.field.value = value;
    this.output.emit(this.field);
  }

}
