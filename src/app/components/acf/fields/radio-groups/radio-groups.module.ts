import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioGroupsComponent } from './radio-groups.component';
import { InputRadioModule } from '../input-radio/input-radio.module';



@NgModule({
  declarations: [RadioGroupsComponent],
  imports: [
    CommonModule,
    InputRadioModule,
  ],
  exports: [RadioGroupsComponent],
})
export class RadioGroupsModule { }
