import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  emit(field) {
    this.output.emit(field);
  }

}
