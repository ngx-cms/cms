import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent implements OnInit {

  @Input() field: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emit(field) {
    this.output.emit(field);
  }

}
