import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { cloneDeep, flatMap } from '../../../helpers/string.helper';
import { ACFField, FieldInputs } from '../acf.interface';
import { ACF, REPEATER, REPEATER_FIELDS } from '../acf.mock';
import { FIELD_TYPES } from '../mock/field-types.acf';

@Component({
  selector: 'cms-fields',
  templateUrl: './fields.component.html',
  styleUrls: ['./fields.component.scss']
})
export class FieldsComponent implements OnInit {

  @Input() fields: any;
  @Input() fieldsIndex: number;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();
  @Output() action = new EventEmitter();

  public hiddenFields = [];

  constructor() { }

  ngOnInit(): void {
    this.inputs = {
      ...this.inputs,
      fieldsIndex: this.fieldsIndex,
    };
  }

  onToolbarAction(action, field, ref, index) {
    switch (action) {
      case 'remove':
        field.fields.splice(index, 1);
        break;
      case 'minimize':
        this.hiddenFields.push(ref);
        break;
      case 'restore':
        this.hiddenFields.splice(this.hiddenFields.findIndex(f => f === ref), 1);
        break;
      default:
    }
  }

  onFieldChange(field) {
    this.output.emit({ field, fields: this.fields });
  }

  onFieldAction(parent, action) {
    this.action.emit({
      ...action,
      parent: parent.name,
    });
  }

  addRepeater(field, repeaterField) {
    // const repeater = REPEATER_FIELDS[0].push(REPEATER);
    // console.log('repeater: ', repeater)
    // REPEATER_FIELDS[0].push(REPEATER);
    field.fields.push(cloneDeep(ACF[0]));
    this.output.emit({ field, fields: this.fields });
  }

}
