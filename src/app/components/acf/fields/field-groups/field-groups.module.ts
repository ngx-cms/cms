import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldGroupsComponent } from './field-groups.component';
import { FieldModule } from '../field/field.module';
import { AcfToolbarModule } from '../../acf-toolbar/acf-toolbar.module';
import { ButtonModule } from '../../../button/button.module';
import { FieldLabelModule } from '../field-label/field-label.module';



@NgModule({
  declarations: [FieldGroupsComponent],
  imports: [
    CommonModule,
    FieldModule,
    AcfToolbarModule,
    ButtonModule,
    FieldLabelModule,
  ],
  exports: [FieldGroupsComponent],
})
export class FieldGroupsModule { }
