import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { cloneDeep, flatMapDeep } from '../../../../helpers/string.helper';
import { FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-field-groups',
  templateUrl: './field-groups.component.html',
  styleUrls: ['./field-groups.component.scss']
})
export class FieldGroupsComponent implements OnInit {

  @Input() field: any;
  @Input() fields: any;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();
  @Output() toolbarAction = new EventEmitter();

  private fieldGroups: Array<any>;
  public state: 'minimize' | 'expand' = 'minimize';

  constructor() { }

  ngOnInit(): void {
    this.fieldGroups = (
      cloneDeep(this.field.field_groups && this.field.field_groups[0])
        .map(field => {
          field.value = null;
          return field;
        })
    )
  }

  onFieldChange(field) {
    if (field.field_groups_disabled) return;
    field.value = (
      flatMapDeep(field.field_groups)
        .map(f => field.value_type === 'array' ? f.value : ([{ label: f.label, value: f.value }]))
    )
    this.output.emit(field);
  }

  onAction(action, index?: number) {
    console.log('cloneDeep(this.fieldGroups): ', cloneDeep(this.fieldGroups))
    if (action === 'add') {
      if (this.field.field_groups.length > 5) {
        // return;
      }
      this.field.field_groups.push(cloneDeep(this.fieldGroups));
    } else if (action === 'remove') {
      this.field.field_groups.splice(index, 1);
    }
    this.onFieldChange(this.field);
    console.log('this.field.field_groups: ', this.field)
    // this.toolbarAction.emit(action);
  }

}
