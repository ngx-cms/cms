import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldsComponent } from './fields.component';
import { FieldModule } from './field/field.module';
import { FieldGroupsModule } from './field-groups/field-groups.module';
import { ButtonModule } from '../../button/button.module';
import { AcfToolbarModule } from '../acf-toolbar/acf-toolbar.module';
import { TooltipModule } from '../../tooltip/tooltip.module';
import { FieldLabelModule } from './field-label/field-label.module';



@NgModule({
  declarations: [FieldsComponent],
  imports: [
    CommonModule,
    FieldModule,
    FieldGroupsModule,
    ButtonModule,
    AcfToolbarModule,
    TooltipModule,
    FieldLabelModule,
  ],
  exports: [FieldsComponent],
})
export class FieldsModule { }
