import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FieldLabelComponent } from './field-label.component';
import { TooltipModule } from '../../../tooltip/tooltip.module';



@NgModule({
  declarations: [FieldLabelComponent],
  imports: [
    CommonModule,
    TooltipModule,
  ],
  exports: [FieldLabelComponent],
})
export class FieldLabelModule { }
