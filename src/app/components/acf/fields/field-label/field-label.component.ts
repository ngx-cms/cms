import { Component, Input, OnInit } from '@angular/core';
import { ACFField, FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-field-label',
  templateUrl: './field-label.component.html',
  styleUrls: ['./field-label.component.scss']
})
export class FieldLabelComponent implements OnInit {

  @Input() field: ACFField;
  @Input() inputs: FieldInputs;
  @Input() id: string;
  constructor() { }

  ngOnInit(): void {
  }

}
