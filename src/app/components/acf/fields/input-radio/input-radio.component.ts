import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ACFField, FieldInputs } from '../../acf.interface';

@Component({
  selector: 'cms-input-radio',
  templateUrl: './input-radio.component.html',
  styleUrls: ['./input-radio.component.scss']
})
export class InputRadioComponent implements OnInit {

  @Input() field: ACFField;
  @Input() option: ACFField;
  @Input() inputs: FieldInputs;
  @Output() output = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emit(field) {
    this.output.emit(this.option.value);
  }

}
