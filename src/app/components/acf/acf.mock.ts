import { sidebar } from "../../../shared/constants";
import { cloneDeep, flatMapDeep } from "../../helpers/string.helper";
import { ACFField } from "./acf.interface";
import { ACF_CONFIG } from "./mock/acf-config.acf";
import { COMPARATORS } from "./mock/comparators.acf";
import { DATA_TYPES } from "./mock/data-types.acf";
import { FIELD_TYPES } from "./mock/field-types.acf";

export const acfConfig: ACFField[][] = ACF_CONFIG;

export const ACF_FIELDS: ACFField[][] = [
  [
    {
      label: 'Label',
      name: 'label',
      type: 'text',
      required: true,
    },
    {
      label: 'Auto name',
      name: 'auto_name',
      type: 'checkbox',
      value: true,
    },
    {
      label: 'Name',
      name: 'name',
      type: 'text',
      auto_values: [
        {
          field: 'label',
        }
      ],
      transform: 'autoName',
      depends: [
        {
          lvalue: 'auto_name',
          rvalue: false,
        }
      ]
    },
    {
      label: 'Placeholder',
      name: 'placeholder',
      type: 'text',
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ],
    },
    {
      name: 'type',
      label: 'Field type',
      type: 'select',
      required: true,
      value: 'text',
      // value: 'form_group',
      options: FIELD_TYPES,
    },
    {
      label: 'Dropdown Display',
      name: 'display',
      type: 'radio',
      value: 'label',
      options: [
        {
          label: 'Label',
          value: 'label',
        },
        {
          label: 'Value',
          value: 'value',
        },
        {
          label: 'Label + Value (both)',
          value: 'both',
        },
      ],
      depends: [
        {
          lvalue: 'type',
          rvalue: 'select',
        }
      ]
    },
    {
      label: 'Default value',
      name: 'default_value',
      type: 'radio',
      value: 'custom',
      options: [
        {
          label: 'Auto',
          value: 'auto'
        },
        {
          label: 'Custom',
          value: 'custom',
        }
      ],
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ],
    },
    {
      label: '',
      name: 'value',
      type: 'text',
      depends: [
        {
          lvalue: 'default_value',
          rvalue: 'custom'
        }
      ],
    },
    {
      label: '',
      name: 'default_value_type',
      type: 'select',
      required: true,
      options: [
        {
          label: 'ID',
          value: 'id()',
        },
        {
          label: 'Current User',
          value: 'current_user()',
        },
        {
          label: 'Date',
          value: 'date_now()',
        },
        {
          label: 'Date + Time',
          value: 'date_time_now()',
        }
      ],
      depends: [
        {
          lvalue: 'default_value',
          rvalue: 'auto'
        }
      ],
    },
    {
      name: 'unique_value',
      label: 'Unique Value',
      type: 'checkbox',
      value: false,
    },
    {
      name: 'editable',
      label: 'Editable',
      type: 'checkbox',
      value: true,
    },
    {
      name: 'enable_auto_values',
      label: 'Auto Value',
      type: 'checkbox',
      value: false,
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ],
    },
    {
      name: 'auto_values',
      label: 'Auto Value Field',
      required: true,
      type: 'select',
      depends: [
        {
          lvalue: 'enable_auto_values',
          rvalue: true,
        }
      ],
      transform: 'autoValueOptions',
    },
    {
      name: 'enable_auto_format',
      label: 'Auto Format',
      type: 'checkbox',
      value: false,
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ]
    },
    {
      name: 'format_type',
      label: 'Format Type',
      required: true,
      type: 'radio',
      value: 'simplified',
      options: [
        {
          label: 'Simplified',
          value: 'simplified',
        },
        {
          label: 'Custom',
          value: 'custom',
        }
      ],
      depends: [
        {
          lvalue: 'enable_auto_format',
          rvalue: true,
        }
      ],
    },
    {
      name: 'format',
      label: 'Format',
      required: true,
      type: 'select',
      display: 'value',
      options: [
        {
          value: 'url',
          label: 'URL',
        }
      ],
      depends: [
        {
          lvalue: 'format_type',
          rvalue: 'simplified',
        }
      ],
      matchAll: true,
    },
    {
      name: 'format',
      label: 'Format',
      required: true,
      type: 'textarea',
      depends: [
        {
          lvalue: 'format_type',
          rvalue: 'custom',
        }
      ],
    },
    {
      name: 'enable_transform',
      type: 'checkbox',
      label: 'Enable Transform',
      value_type: 'boolean',
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ]
    },
    {
      name: 'transform',
      type: 'textarea',
      value_type: 'function',
      value: (params) => {
        // Code here
      },
      depends: [
        {
          lvalue: 'enable_transform',
          rvalue: true,
        }
      ]
    },
    {
      name: 'enable_dependency',
      type: 'checkbox',
      label: 'Enable Dependency',
      tooltip: {
        enabled: true,
        text: 'Show this field when specific condition has met.'
      },
      // transform: params => (params.field.disabled = params.acf.length === 1),
      value_type: 'boolean',
    },
    {
      name: 'depends',
      type: 'field-groups',
      label: '',
      display: 'label',
      field_groups: [
        [
          {
            name: 'lvalue',
            label: 'Field',
            type: 'select',
            dynamic_values: [
              {
                field: 'name',
                name: 'value',
              },
              {
                field: 'label',
                name: 'label',
              }
            ]
          },
          {
            name: 'comparator',
            label: 'Comparator',
            type: 'select',
            value: 'eq',
            options: COMPARATORS,
          },
          {
            name: 'rvalue',
            label: 'Value',
            type: 'text',
          },
        ],
      ],
      depends: [
        {
          lvalue: 'enable_dependency',
          rvalue: true,
        },
      ]
    },
    {
      name: 'enable_tooltip',
      type: 'checkbox',
      label: 'Enable Tooltip',
      value_type: 'boolean',
    },
    { 
      name: 'tooltip',
      type: 'textarea',
      label: '',
      depends: [
        {
          lvalue: 'enable_tooltip',
          rvalue: true,
        }
      ]
    },
    // {
    //   name: 'enable_validations',
    //   type: 'checkbox',
    //   label: 'Enable Validations',
    // },
    {
      name: 'enable_excluded_category',
      type: 'select',
      label: 'Excluded Category',
      value: false,
      options: [
        {
          label: 'Yes',
          value: true,
        },
        {
          label: 'No',
          value: false,
        }
      ],
      tooltip: {
        enabled: true,
        text: 'When enabled, show the field group to all category with exception to this.'
      },
      depends: [
        {
          lvalue: 'inputs.index',
          comparator: 'eq',
          rvalue: 0,
        }
      ],
    },
    {
      label: 'Force value type',
      name: 'enable_value_type',
      type: 'checkbox',
      tooltip: {
        enabled: true,
        text: 'Transform output value'
      },
      value_type: 'boolean',
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ]
    },
    {
      label: 'Value type',
      name: 'value_type',
      type: 'select',
      tooltip: {
        enabled: true,
        text: 'Transform output value to specified data type (default depends on field type)'
      },
      options: DATA_TYPES,
      depends: [
        {
          lvalue: 'enable_value_type',
          comparator: 'eq',
          rvalue: true,
        }
      ],
      value: 'string',
    },
    {
      name: 'option_type',
      type: 'radio',
      label: 'Choices type',
      value: 'custom',
      options: [
        {
          label: 'Custom',
          value: 'custom',
        },
        {
          label: 'Auto',
          value: 'auto',
        }
      ],
      depends: [
        {
          lvalue: 'type',
          rvalue: 'select',
        }
      ]
    },
    {
      name: 'options',
      type: 'select',
      label: 'Field Choices',
      required: true,
      value_type: 'array',
      options: [
        {
          label: 'Users',
          value: 'users()',
        }
      ],
      depends: [
        {
          lvalue: 'option_type',
          comparator: 'eq',
          rvalue: 'auto'
        }
      ]
    },
    {
      name: 'options',
      type: 'field-groups',
      label: 'Field Choices',
      required: true,
      value_type: 'array',
      depends: [
        {
          lvalue: 'option_type',
          comparator: 'eq',
          rvalue: 'custom'
        }
      ],
      field_groups: [
        [
          {
            name: 'value',
            label: 'Value',
            type: 'text',
            required: true,
          },
          {
            name: 'label',
            label: 'Label',
            type: 'text',
            required: true,
          },
        ]
      ],
    },
    {
      label: 'Don\'t show on',
      name: 'category_excluded',
      type: 'field-groups',
      value: 'all',
      required: true,
      field_groups: [
        [
          {
            name: 'category_excluded',
            type: 'select',
            required: true,
            // options: sidebar.navs.filter(nav => !nav.event)
            //   .map(nav => ({ value: nav.url, label: nav.label}))
          },
        ]
      ],
      depends: [
        {
          lvalue: 'enable_excluded_category',
          comparator: 'eq',
          rvalue: true,
        },
        {
          lvalue: 'inputs.index',
          comparator: 'eq',
          rvalue: 0,
        }
      ],
    },
    {
      name: 'attributes',
      label: 'Attributes',
      type: 'form_group',
      repeater: false,
      fields: [
        [
          {
            name: 'required',
            type: 'checkbox',
            label: 'Required',
            value: false,
          },
          {
            label: 'Disabled',
            name: 'disabled',
            value: false,
            type: 'checkbox',
          }
        ]
      ],
      depends: [
        {
          lvalue: 'type',
          comparator: 'ne',
          rvalue: 'form_group'
        }
      ]
    },
    {
      name: 'toolbar',
      label: 'Toolbar',
      type: 'form_group',
      repeater: false,
      enable_value_type: true,
      value_type: 'object',
      fields: [
        [
          {
            name: 'enabled',
            type: 'checkbox',
            label: 'Enable',
            value: false,
          },
          {
            name: 'minimise',
            type: 'checkbox',
            label: 'Minimise',
            value: false,
          },
          {
            name: 'move',
            type: 'checkbox',
            label: 'Move',
            value: false,
          }
        ]
      ]
    }
  ]
];


export const FIELD_GROUPS: ACFField = {
  name: 'field_groups',
  type: 'field-groups',
  label: 'Field Groups',
  enable_value_type: true,
  value_type: 'array',
  depends: [
    {
      lvalue: 'type',
      comparator: 'eq',
      rvalue: 'field-groups',
    }
  ],
  required: true,
  field_groups: [
    [
      {
        name: 'label',
        label: 'Label',
        type: 'text',
        required: true,
      },
      {
        label: 'Name',
        name: 'name',
        type: 'text',
      },
      {
        name: 'type',
        label: 'Type',
        type: 'select',
        options: FIELD_TYPES,
        required: true,
      },
    ]
  ],
};

const CLONED_REPEATER = cloneDeep(ACF_FIELDS)
CLONED_REPEATER[0].push(FIELD_GROUPS);
export const REPEATER_FIELDS = cloneDeep(CLONED_REPEATER);

export const REPEATER: ACFField = {
  name: 'form_group',
  label: 'Form Groups',
  type: 'form_group',
  repeater: true,
  depends: [
    {
      lvalue: 'type',
      comparator: 'eq',
      rvalue: 'form_group',
    }
  ],
  fields: CLONED_REPEATER,
};

REPEATER.fields[0].push(cloneDeep(REPEATER));

ACF_FIELDS[0].push(FIELD_GROUPS);
ACF_FIELDS[0].push(REPEATER);
export const ACF = cloneDeep(ACF_FIELDS);

