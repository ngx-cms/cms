import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { cloneDeep, mergeArrayObjects } from '../../helpers/string.helper';
import { AcfService } from '../../services/acf/acf.service';
import { ACFField, FieldInputs } from './acf.interface';

@Component({
  selector: 'cms-acf',
  templateUrl: './acf.component.html',
  styleUrls: ['./acf.component.scss']
})
export class AcfComponent implements OnInit, OnChanges {

  @Input() acf: ACFField[][];
  @Input() type: 'fields' | 'config';
  @Input() toolbar: boolean = true;
  @Input() inputs: FieldInputs;
  @Output() action = new EventEmitter();

  public hiddenFields = [];

  constructor(
    private acfService: AcfService,
  ) { }

  ngOnInit(): void {
    // this.acf = this.acf.map(fields => this.setAttrs(fields));
    // this.acfService.initACF(this.acf);
  }

  ngOnChanges(changes) {
    if (changes.acf) {
      this.acfService.initACF(this.acf);
    }
  }

  onFieldAction(action) {
    this.action.emit(action);
  }

  onFieldChange(action) {
    if (action.field) {
      this.action.emit(action);
    }
    this.acfService.initACF(this.acf, action.field);
    // this.acfService.initFields(action.fields, this.acf);
  }

  toolbarAction(action, index) {
    if (action === 'minimize') {
      this.hiddenFields.push(index);
    } else if (action === 'restore') {
      this.hiddenFields.splice(this.hiddenFields.findIndex(f => f === index), 1);
    }
    this.action.emit({
      action,
      index,
      type: this.type,
    })
  }

}
