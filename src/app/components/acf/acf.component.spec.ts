import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcfComponent } from './acf.component';

describe('AcfComponent', () => {
  let component: AcfComponent;
  let fixture: ComponentFixture<AcfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
