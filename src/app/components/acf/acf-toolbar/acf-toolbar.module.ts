import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcfToolbarComponent } from './acf-toolbar.component';



@NgModule({
  declarations: [AcfToolbarComponent],
  imports: [
    CommonModule,
  ],
  exports: [AcfToolbarComponent],
})
export class AcfToolbarModule { }
