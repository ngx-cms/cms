import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcfToolbarComponent } from './acf-toolbar.component';

describe('AcfToolbarComponent', () => {
  let component: AcfToolbarComponent;
  let fixture: ComponentFixture<AcfToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcfToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcfToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
