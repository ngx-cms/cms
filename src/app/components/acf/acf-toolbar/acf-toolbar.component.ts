import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'cms-acf-toolbar',
  templateUrl: './acf-toolbar.component.html',
  styleUrls: ['./acf-toolbar.component.scss']
})
export class AcfToolbarComponent implements OnInit, OnChanges {

  @Input() type: 'field-groups' | 'acf' = 'acf';
  @Input() acf: number;
  @Input() hiddenFields = [];
  @Input() index: string;
  @Input() title: string;
  @Input() fieldId: string;
  @Input() config: {
    add?: boolean;
    remove?: boolean;
    edit?: boolean;
  } = { add: true, remove: true, edit: false, }
  @Output() action = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
    if (this.fieldId) {
      this.index = this.fieldId;
    }
    if (typeof this.title === 'boolean') {
      this.title = ''
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.title) {
      this.title = changes.title.currentValue;
    }
    if (typeof this.title === 'boolean') {
      this.title = ''
    }
  }

  onAction(type) {
    this.action.emit(type);
  }

}
