export const DATA_TYPES = [
  {
    value: 'string',
    label: 'String',
  },
  {
    value: 'number',
    label: 'Number',
  },
  {
    value: 'boolean',
    label: 'Boolean',
  },
  {
    value: 'array',
    label: 'Array',
  },
  {
    value: 'object',
    label: 'Object',
  },
];