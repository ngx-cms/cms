import { sidebar } from "../../../../shared/constants";
import { flatMapDeep } from "../../../helpers/string.helper";
import { ACFField } from "../acf.interface";

export const ACF_CONFIG: ACFField[][] = [
  [
    {
      label: 'Form Name',
      name: 'form_name',
      type: 'text',
      value: '',
      required: true,
      placeholder: 'My awesome form'
    },
    {
      label: 'Permanent Form',
      name: 'permanent',
      type: 'checkbox',
      value: false
    },
    {
      name: 'enable_category',
      type: 'checkbox',
      label: 'Category',
      value: false,
      tooltip: {
        enabled: true,
        text: 'When enabled, show the field group on specific page category only.'
      }
    },
    {
      label: '',
      name: 'category',
      type: 'field-groups',
      value: [],
      required: true,
      field_groups: [
        [
          {
            name: 'category',
            type: 'select',
            required: true,
            display: 'label',
            transform: 'sidebarOptions'
          }
        ]
      ],
      transform: 'categoryOptions',
      value_type: 'array',
      depends: [
        {
          lvalue: 'enable_category',
          comparator: 'eq',
          rvalue: true
        }
      ]
    }
  ]
]