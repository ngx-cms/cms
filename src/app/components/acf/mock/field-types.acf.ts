export const FIELD_TYPES = [
  {
    label: 'Text',
    value: 'text',
  },
  {
    label: 'Password',
    value: 'password',
  },
  {
    label: 'Url',
    value: 'url',
  },
  {
    label: 'Email',
    value: 'email',
  },
  {
    label: 'Number',
    value: 'number',
  },
  {
    label: 'Text Area',
    value: 'textarea',
  },
  {
    label: 'Dynamic Content',
    value: 'dynamic_content',
  },
  {
    label: 'Dropdown',
    value: 'select',
  },
  {
    label: 'Date',
    value: 'date',
  },
  {
    label: 'Date + time',
    value: 'datetime-local',
  },
  {
    label: 'Array',
    value: 'array',
  },
  {
    label: 'Form Group',
    value: 'form_group',
  },
  {
    label: 'Radio Group',
    value: 'radio',
  },
  {
    label: 'Checkbox',
    value: 'checkbox',
  },
  {
    label: 'Checkbox Group',
    value: 'checkbox-group',
  },
  {
    label: 'Hidden',
    value: 'hidden',
  },
];