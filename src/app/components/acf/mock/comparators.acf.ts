export const COMPARATORS = [
  {
    value: 'eq',
    label: 'Equal'
  },
  {
    value: 'ne',
    label: 'Not Equal'
  },
  {
    value: 'lt',
    label: 'Less Than'
  },
  {
    value: 'lte',
    label: 'Less Than Equal to'
  },
  {
    value: 'gt',
    label: 'Greater Than'
  },
  {
    value: 'gte',
    label: 'Greater Than Equal to'
  },
  {
    value: 'has',
    label: 'Has'
  },
]