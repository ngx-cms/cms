import { RouteState } from "../../interfaces/route-state";

export interface FieldInputs extends RouteState {
  oIndex?: number;
  fieldsIndex?: number;
  fieldIndex?: number;
  field_name?: string;
  routeState?: RouteState;
}

export interface ACFFieldOptions {
  label?: any;
  value?: any;
}

export interface ACFDepends {
  rvalue: string | number | boolean | any[];
  lvalue?: string | number;
  comparator?: string;
}

export interface FieldAttributes {
  disabled?: boolean;
  required?: boolean;
  accept?: Blob;
  alt?: string;
  autocomplete?: boolean;
  autofocus?: boolean;
  capture?: boolean;
  checked?: boolean;
  dirname?: string;
  id?: string;
  form?: any;
  formaction?: any;
  formmethod?: 'POST' | 'GET' | 'PUT' | 'PATCH' | 'DELETE';
  formenctype?: any;
  formnovalidate?: boolean;
  formtarget?: string;
  height?: string | number;
  list?: any;
  max?: number;
  maxlength?: number;
  min?: number;
  minlength?: number;
  multiple?: any;
  name?: string;
  pattern?: string;
  placeholder?: string;
  readonly?: boolean;
  size?: number;
  src?: string;
  step?: number;
  type?: string;
  value?: any;
  width?: number;
};

export interface ACFField {
  editable?: boolean;
  repeater?: boolean;
  valid?: boolean;
  required?: boolean;
  value_type?: string;
  hidden?: boolean;
  name?: string;
  value?: any;
  label?: string;
  field_groups?: FieldGroups[][];
  type?: 'select' | 'text' | 'email' | 'number' | 'radio' | 'checkbox' | 'form_group' | 'field-groups' | 'textarea' | 'dynamic_content';
  placeholder?: string;
  tooltip?: any;
  options?: ACFFieldOptions[];
  transform?: string;
  depends?: ACFDepends[];
  fields?: any;
  auto_values?: any[];
  display?: 'value' | 'label' | 'both';
  matchAll?: boolean;
  attributes?: FieldAttributes[];
  enable_value_type?: boolean;
  default_value?: any;
  unique_value?: boolean;
  dynamic_values?: DynamicFieldValues;
  option_type?: 'auto' | 'custom';
  form_group?: ACFField[];
  enable_tooltip?: boolean;
  disabled?: boolean;
}

export interface FieldOptions {
  value: string | number | boolean;
  label: string;
}

export interface FieldGroups {
  name: string | number;
  label?: string | number;
  type: string;
  value?: any;
  required?: boolean;
  dynamic_values?: DynamicFieldValues[];
  options?: FieldOptions[];
  is_hidden?: boolean;
  display?: 'value' | 'label' | 'both';
  transform?: string;
}

export interface DynamicFieldValues {
  field?: string;
  name?: string;
}