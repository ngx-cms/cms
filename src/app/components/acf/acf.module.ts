import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcfComponent } from './acf.component';
import { FieldsModule } from './fields/fields.module';
import { AcfToolbarModule } from './acf-toolbar/acf-toolbar.module';



@NgModule({
  declarations: [AcfComponent],
  imports: [
    CommonModule,
    FieldsModule,
    AcfToolbarModule,
  ],
  exports: [
    AcfComponent,
  ]
})
export class AcfModule { }
