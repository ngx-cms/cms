import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRouting } from './dashboard.routing';
import { TooltipModule } from '../tooltip/tooltip.module';
import { DashboardComponent } from './dashboard.component';
import { ModalSlideModule } from '../../shared/components/modal/modal-slide/modal-slide.module';
import { DropdownModule } from '../form-control/dropdown/dropdown.module';
import { PipesModule } from '../../pipes/pipes.module';
import { CardModule } from '../card/card.module';
import { DirectivesModule } from '../../directives/directives.module';
import { ModalModule } from '../../shared/components/modal/modal.module';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRouting,
    DirectivesModule,
    TooltipModule,
    ModalModule,
    ModalSlideModule,
    DropdownModule,
    PipesModule,
    CardModule,
  ],
  exports: [DashboardComponent],
})
export class DashboardModule { }
