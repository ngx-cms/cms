import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { sidebar } from '../../../shared/constants';
import { ApiResponse } from '../../../shared/interfaces/api';
import { FormFields } from '../../interfaces/custom-fields';
import { BackendService } from '../../services/backend/backend.service';

interface Cards {
  type?: string;
  value?: any;
  label?: string;
  data?: any;
}
@Component({
  selector: 'cms-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  modal = {
    open: false,
    config: {
      type: 'slide',
      title: 'Card Config'
    },
    data: [],
    category: '',
  };
  showTooltip = false;
  widgets = [
    {
      key: 'card',
      label: 'Card',
    },
    {
      key: 'note',
      label: 'Note',
    }
  ];

  cards: Cards[] = [
    {
      type: 'test',
      value: '4',
      label: 'Posts',
      data: '4'
    }
  ];

  
  private navs = sidebar.navs
  .filter(nav => !nav.event)
  .map(nav => ({ value: nav.url, label: nav.label}));

  private cardFields: FormFields[][] = [
    [
      {
        label: 'Category',
        type: 'select',
        name: 'category',
        options: this.navs,
      },
      {
        label: 'Label',
        type: 'text',
        name: 'label',
        tooltip: {
          text: 'If has value, will show on the card label else category label'
        }
      },
      {
        label: 'Color',
        type: 'text',
        name: 'color',
      }
    ]
  ]

  constructor(
    private backendService: BackendService,
  ) {}

  ngOnInit(): void {
    this.modal.data = this.widgets;
  }

  toggleModal() {
    this.modal.open = !this.modal.open;
    this.changeModalData();
    if (!this.modal.open) {
      if (!this.cards[this.cards.length - 1].value) {
        this.cards.splice((this.cards.length - 1), 1);
        this.modal.config.type = 'slide';
      }
    }
  }

  changeModalData() {
    if (this.modal.open) {
      console.log('modal type: ', this.modal.config.type)
      if (this.modal.config.type === 'slide') {
        this.modal.data = this.widgets;
      } else {
        this.modal.data = this.cardFields;
      }
    }
  }

  modalData(inputs) {
    console.log('modal data: ', inputs)
    if (inputs.key) {
      this.modal.category = inputs.key;
    }
    if (inputs) {
      switch (inputs.key || this.modal.category) {
        case 'card':
          this.initCard(inputs);
          break;
        default:
          this.processModalData(...inputs);
          break;
      }
    }

    this.changeModalData();
  }

  processModalData(...inputs) {
    const data = arguments;
    console.log('data: ', data)
  }

  initCard(inputs) {
    this.modal.config.type = 'default';
    console.log('add card: ', inputs)
    this.cards.push(inputs);
  }

  cardConfig(card, inputs) {
    this.navs.forEach(nav => {
      if (nav.value === inputs.field.value) {
        card.label = nav.label;
        card.value = nav.value;
      }
    })
  }

  getCardData(card) {
    if (card.data === undefined) {
      this.backendService.get(`collections/${card.value}`)
        .pipe(take(1))
        .subscribe((res: ApiResponse) => {
          card.data = res.data.length;
        });
    }

    return card.data;
  }

  inputs(type) {
    switch (type) {
      case 'card':
        return {
          field: {
            options: this.navs,
          }
        }
      default:
        return {};
    }
  }

}
