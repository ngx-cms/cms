import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleTileComponent } from './article-tile.component';



@NgModule({
  declarations: [ArticleTileComponent],
  imports: [
    CommonModule
  ],
  exports: [ArticleTileComponent]
})
export class ArticleTileModule { }
