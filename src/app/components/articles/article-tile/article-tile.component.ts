import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cms-article-tile',
  templateUrl: './article-tile.component.html',
  styleUrls: ['./article-tile.component.scss']
})
export class ArticleTileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
