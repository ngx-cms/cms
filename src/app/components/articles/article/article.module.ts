import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article.component';
import { ArticleTileModule } from '../article-tile/article-tile.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [ArticleComponent],
  imports: [
    CommonModule,
    ArticleTileModule,
    RouterModule,
  ],
  exports: [ArticleComponent]
})
export class ArticleModule { }
