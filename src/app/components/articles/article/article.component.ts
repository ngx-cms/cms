import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cms-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article;
  @Input() author: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }

}
