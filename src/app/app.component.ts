import { Component, isDevMode } from '@angular/core';
import { WindowRef } from './dom-abstraction/window.service';
import { AppStateService } from './services/app-state/app-state.service';
import { FirebaseService } from './services/firebase.service';

@Component({
  selector: 'cms-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private appStateService: AppStateService,
    private firebaseService: FirebaseService,) {
    console.log('Environment: ', isDevMode() ? 'Development' : 'Production');
    firebaseService.init();
    appStateService.init();
  }
}
