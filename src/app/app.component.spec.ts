import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AppStateService } from './services/app-state/app-state.service';

describe('AppComponent', () => {

  let appStateService;
  let windRef;
  beforeEach(async () => {
    const methods = Object.getOwnPropertyNames(AppStateService)
    appStateService = jasmine.createSpyObj(AppStateService.name, methods);
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('cms app is running!');
  });
});
