export function isValidString(str) {
  if (!str || typeof str !== 'string') {
    return false;
  }

  return true;
}

export function toUrl(url: string): string {
  return url.toString().toLowerCase().replace(/(-)|(_)|( )/gi, '-');
}

export function parseCollectionName(category: string | string[], replace: string = ''): string {
  const regex = /(-)|(_)|( )/g;
  if (Array.isArray(category)) {
    return category[0].replace(regex, replace);
  }
  if (typeof category === 'string') {
    return category.replace(regex, replace).toLowerCase();
  }

  return (<string>category).toLowerCase();
}

export function combineObjectsinArray(arr) {
  if (!arr || !Array.isArray(arr)) {
    return arr;
  }

  return arr.reduce((r, c) => Object.assign(r, c), {});
}

export function boolean(str): any {
  try {
    str = JSON.parse(str);
  } catch ($e) {

  }

  return str;
}

export function number(str): any {
  return Number(str);
}

export function string(str): any {
  return str.toString();
}

export function isEqual(a: any, b: any): boolean {
  if (typeof a === 'object' && typeof b === 'object') {
    return Object.is(JSON.stringify(a), JSON.stringify(b));
   }

   return Object.is(a, b);
}

export function isNumber(n: any): boolean {
    
  if (!n || Array.isArray(n) || typeof n === 'object') {
      return false;
  }

  if (typeof n === 'string') {
      try {
          return typeof JSON.parse(n) === 'number';
      } catch (e) {
          return false;
      }
  }

  return typeof n === 'number';
}

export function isArray(arr: any): boolean {
  return Array.isArray(arr);
}

export function isBoolean(bool: any): boolean {
  return typeof bool === 'boolean';
}

export function isFunction(func: any): boolean {
  return typeof func === 'function';
}

export function isObject(obj): boolean {
  if (isString(obj) && !isBoolean(obj) && (obj !== 'true' && obj !== 'false')) {
    try {
      JSON.parse(toJSON(obj));
    } catch (e) {
      return false;
    }
    return true;
  }
  return typeof obj === 'object' && !Array.isArray(obj);
}

export function type(source) {
  if (Array.isArray(source)) {
    return 'array';
  }

  return typeof source;
}

export function addTrailingSlash(string: string): string {
  return string.charAt(string.length - 1) !== '/' ? `${string}/` : string;
}

export function toJSON(obj) {
  return obj
	// Replace ":" with "@colon@" if it's between double-quotes
	.replace(/:\s*"([^"]*)"/g, function(match, p1) {
		return ': "' + p1.replace(/:/g, '@colon@') + '"';
	})

	// Replace ":" with "@colon@" if it's between single-quotes
	.replace(/:\s*'([^']*)'/g, function(match, p1) {
		return ': "' + p1.replace(/:/g, '@colon@') + '"';
	})

	// Add double-quotes around any tokens before the remaining ":"
	.replace(/(['"])?([a-z0-9A-Z_]+)(['"])?\s*:/g, '"$2": ')

	// Turn "@colon@" back into ":"
	.replace(/@colon@/g, ':')
}


export function hasKey(obj): boolean {
  return obj && typeof obj === 'object' && Object.keys(obj).length !== 0;
}

export function getKey(obj: object): string[] {
  return hasKey(obj) ? Object.keys(obj) : [];
}

export function isString(str) {
  if (!str || typeof str !== 'string') {
    return false;
  }

  return true;
}

export function ucFirst(str: string) {
  if (!isString(str)) {
    return str;
  }

  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function lcFirst(str: string) {
  if (!isString(str)) {
    return str;
  }

  return str.charAt(0).toLowerCase() + str.slice(1);
}

export function removeLast(str: string | string[]) {
  if (Array.isArray(str) && str.length === 0) {
    return str;
  } else if (!isString(str)) {
    return str;
  }

  return str.slice(0, -1);
}

export function flatMapObject(obj: object): object {
  if (typeof obj === 'object' && !Array.isArray(obj)) {
    let newObj = {};
    for (const o of Object.values(obj)) {
      for (const i in o) {
        newObj[i] = o[i];
      }
    }

    return newObj;
  }

  return obj;
}

export function flatMapDeep(arr: Array<any>): Array<any> {
  return arr && Array.isArray(arr) ? arr.flat(Infinity) : [];
}

export function flatMap(arr: Array<any>, depth: number = 1): any[] {
  return arr && Array.isArray(arr) ? arr.flat(depth) : [];
}

export function uniq(arr) {
  if (Array.isArray(arr)) {
    return [...new Set(flatMapDeep(arr))];
  }

  return arr;
}

export function keys(obj: object): Array<string> {
  if (typeof obj === 'object' && !Array.isArray(obj)) {
    return Object.keys(obj);
  }

  return obj;
}

export function entries(obj: object, flatten: boolean = false): any[][] {
  if (typeof obj === 'object' && !Array.isArray(obj)) {
    return flatten ? flatMap(Object.entries(obj)) : Object.entries(obj);
  }

  return obj;
}

/**
 * 
 * @param obj - Object items
 * @returns - Object values only
 */
export function values(obj: object): Array<string> {
  if (typeof obj === 'object' && !Array.isArray(obj)) {
    return Object.values(obj);
  }

  return obj;
}

/**
 * Finds key-pair value on object or array
 * @param obj object | Array
 * @param key string | string[]
 * @returns key pair values of `@key`
 */
export function pick(obj: object | any[], key?: string | string[]): object {
  if (!key || !key?.length) return obj;

  // Convert string -> array; 'a' -> ['a']
  if (typeof key === 'string') (key = [key]);
  if (typeof obj === 'object') {
    if (Array.isArray(obj)) {
      const newArr = [];
      obj.forEach(el => {
        const newObj = {};
        for (const k of key) {
          if (el[k]) (newObj[k] = el[k]);
        }
        if (Object.keys(newObj).length) {
          newArr.push(newObj);
        }
      });
  
      return newArr;
    }

    const newObj = {};
    for (const k of key) {
      if (obj[k]) (newObj[k] = obj[k]);
    }

    return newObj;
  }
  
  return obj;
}

/**
 * Finds key-pair value on object or array
 * @param obj object | Array
 * @param key string | string[]
 * @returns key pair values of `@key`
 */
export function omit(obj: object | any[], key?: string | string[]): object {
  if (!key || !key?.length) return obj;

  // Convert string -> array; 'a' -> ['a']
  if (typeof key === 'string') (key = [key]);

  if (typeof obj === 'object') {
    if (Array.isArray(obj)) {
      const newArr = [];
      obj.forEach(el => {
        for (const k of key) {
          if (el[k]) delete el[k];
        }
        newArr.push(el);
      });
  
      return newArr;
    }

    for (const k of key) {
      if (obj[k]) {
        delete obj[k]
      }
      // if (obj[k]) (newObj[k] = obj[k]);
    }

    return obj;
  }
  
  return obj;
}

/**
 * Merge objects inside given array, overrides latest object key value
 * @param arr object[] 
 * @returns obj
 */
export function mergeArrayObjects(arr: object[], override = true): any {
  if (!arr || !Array.isArray(arr)) {
    return arr;
  }

  if (override) {
    return arr.reduce((r, c) => Object.assign(r, c), {});
  }

  const newObj = {};
  arr.forEach(field => {
    for (let key in field) {
      newObj[key] = newObj[key] ? { ...newObj[key],   ...field[key]} : field[key];
    }
  });

  return newObj;
}


/**
 * Creates a deep clone of an object.
 * @param obj The item to recursively clone
 */
 export const cloneDeep = (item) => {
  if (!item) return item; // null, undefined values check
  const types = [Number, String, Boolean];
  let result;

  // normalizing primitives if someone did new String('aaa'), or new Number('444');
  types.forEach((type) => {
    if (item instanceof type) {
      result = type(item);
    }
  });

  if (typeof result === 'undefined') {
    if (Object.prototype.toString.call(item) === '[object Array]') {
      result = [];
      item.forEach((child, index) => (result[index] = cloneDeep(child)));
    } else if (typeof item === 'object') {
      // check if DOM
      if (item.nodeType && typeof item.cloneNode === 'function') {
        result = item.cloneNode(true);
      } else if (!item.prototype) {
        // check if literal
        if (item instanceof Date) {
          result = new Date(item);
        } else {
          // check if object literal
          result = {};
          for (const i in item) {
            result[i] = cloneDeep(item[i]);
          }
        }
      } else {
        result = item;
      }
    } else {
      result = item;
    }
  }

  return result;
};

export function isValidEmail(str) {
  const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
  const isValid = regularExpression.test(String(str).toLowerCase());
  return isValid;
}

export function parseValueType(value, type) {
  switch (type) {
    case 'boolean':
      return Boolean(value);
    case 'array':
      if (!Array.isArray(value)) {
        return value !== '' ? Array(value) : [];
      }
      return value;
    case 'object':
      if (Array.isArray(value)) {
        return { ...value[0] };
      }

      return { ...value[0] }
    case 'number':
      return Number(value);
    case 'string':
    case 'text':
      if (isObject(value)) {
        return JSON.stringify(value);
      }
      return value;
    default:
  }
}

export function hasProperty(obj, prop) {
  if (!isObject(obj)) return false;
  return obj.hasOwnProperty(prop);
}

export function ucWords(str) {
  if (!str) return '';
  return str.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter?.toUpperCase());
}

export function lastChar(str) {
  return str.charAt(str.length - 1);
}

export function formatCategory(str) {
  return lastChar(str) === 's' ? removeLast(str) : str;
}

export function strToWords(str) {
  return str.replace(/\W/gi, '' );
}

export function removeQueryParams(str) {
  return str.split('?')[0];
}