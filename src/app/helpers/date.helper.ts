export const dateNow = function() {
  return new Date(Date.now());
}

export const getYear = function() {
  return dateNow().getFullYear();
}

export const getMonth = function () {
  return dateNow().getMonth() + 1;
}

export const getDay = function() {
  return dateNow().getDate();
}
  
export const getDate = function() {
  return `${getYear()}-${getMonth()}-${getDay()}`;
}

export const getHour = function() {
  return dateNow().getHours();
}

export const getMinute = function() {
  return dateNow().getMinutes();
}

export const getSecond = function() {
  return dateNow().getSeconds();
}

export const getMS = function() {
  return dateNow().getMilliseconds();
}

export const getTime = function(config = { s: false, ms: false, }) {
  const { s, ms } = config;
  let time = `${getHour()}:${getMinute()}`;
  if (s) {
    time = `${time} ${getSecond()}`;
  }
  if (ms) {
    time = `${time} ${getMS()}`;
  }

  return time;
}
  
export const getDateTime = function(config = { s: false, ms: false }) {
  return `${getYear()}-${getMonth()}-${getDay()} ${getTime(config)}`;
}