import { Directive, ElementRef, Input } from '@angular/core';
import { ACFField } from '../../components/acf/acf.interface';
import { cloneDeep } from '../../helpers/string.helper';

@Directive({
  selector: '[cmsField]'
})
export class FieldDirective {

  @Input() field: ACFField;

  constructor(
    private el: ElementRef,
  ) { }

  ngOnInit(): void {
    if (this.field.attributes) {
      const attrs = this.field.attributes[0];
      for (let attr in attrs) {
        if (attrs[attr] && attr !== 'required') {
          this.el.nativeElement.setAttribute(attr, attrs[attr]);
        }
      }
    }
  }

}
