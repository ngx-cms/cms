import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { FormFields } from '../../interfaces/custom-fields';
import { FormControlService } from '../../services/form-control/form-control.service';

@Directive({
  selector: '[cmsFormControl]'
})
export class FormControlDirective implements OnInit {

  @Input() field: FormFields;

  constructor(
    private el: ElementRef,
    private formControlService: FormControlService,
  ) { }

  ngOnInit(): void {
    if (!this.field) {
      console.log(`Error: Cannot set attributes to ${this.el.nativeElement}, field missing.`);
    }

    this.formControlService.setElementAttributes(this.field, this.el.nativeElement)
  }
}
