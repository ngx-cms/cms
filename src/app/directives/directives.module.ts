import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverDirective } from './popover/popover.directive';
import { PopoverContentDirective } from './popover/popover-content.directive';
import { FormControlDirective } from './form-control/form-control.directive';
import { FieldDirective } from './field/field.directive';
import { HrefInterceptorDirective } from './href-interceptor/href-interceptor.directive';



@NgModule({
  declarations: [PopoverDirective, PopoverContentDirective, FormControlDirective, FieldDirective, HrefInterceptorDirective],
  imports: [
    CommonModule
  ],
  exports: [
    PopoverDirective,
    FormControlDirective,
    FieldDirective,
    HrefInterceptorDirective,
  ]
})
export class DirectivesModule { }
