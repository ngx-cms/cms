import { Directive, HostListener } from '@angular/core';
import { WindowRef } from '../../dom-abstraction/window.service';

@Directive({
  selector: '[cmsHrefInterceptor]'
})
export class HrefInterceptorDirective {

  private window: Window;

  constructor(
    private winRef: WindowRef,
  ) {
    this.window = winRef.nativeWindow;
  }
  
  @HostListener('click', ['$event'])
  onClick($event) {
    $event.preventDefault();
    const { tagName, href, src, innerText } = $event.target;
    if (tagName && (href || src)) {
      const conf = this.window.confirm(`Open "${innerText || 'Image'}" in new tab?`);
      if (conf) {
        this.window.open(src || href, '_blank');
      }
    }
  }

}
