import { ContentChild, Directive, ElementRef, HostListener, OnInit } from '@angular/core';
import { PopoverContentDirective } from './popover-content.directive';

@Directive({
  selector: '[cmsPopover]'
})
export class PopoverDirective implements OnInit {

  private template = `<span class="tooltiptext right left-arrow">Tooltip text</span>`;
  private position = 'right';

  @ContentChild(PopoverContentDirective) content: PopoverContentDirective;
  @HostListener('mouseenter', ['$event'])
  onHover($event) {
    const popover = document.createElement('span');
    this.el.nativeElement.parentNode.classList.add('popover-container-global', 'popover-tooltip', 'position-relative');
    popover.innerHTML = this.template;
    this.el.nativeElement.parentNode.append(popover);
    // console.log('is visible: ', this.isVisible(popover.firstChild))
    const popoverContent = <HTMLElement>popover.firstChild;
    if (!this.isVisible(popoverContent)) {
      if (this.position === 'right') {
        popoverContent.classList.add('left', 'right-arrow');
        popoverContent.classList.remove('right', 'left-arrow');
      }
    }
  }

  @HostListener('mouseleave', ['$event'])
  onLeave($event) {
    console.log('el: ', this.el)
    this.el.nativeElement.parentNode.lastChild.remove();
  }

  private popover;
  constructor(
    private el: ElementRef
  ) {
  }

  ngOnInit() {
    // this.createPopover();
  }

  isVisible(el): boolean {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    if (this.position === 'left' || this.position === 'right') {
      return !(rect[this.position] > window.innerWidth);
    } else if (this.position === 'top' || this.position === 'bottom') {
      return !((elemTop >= 0) && (elemBottom <= window.innerHeight));
    }

    return false;

    // isVisible = elemTop < window.innerHeight && elemBottom >= 0;
  }

  createPopover() {
    this.popover = document.createElement('div');
    this.popover.classList.add('popover-container', 'popover-tooltip');
    this.popover.innerHTML = this.template;
    this.popover.firstChild.classList.add('left')
  }

}
