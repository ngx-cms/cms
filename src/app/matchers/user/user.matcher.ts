import { UrlMatchResult, UrlSegment, UrlSegmentGroup } from "@angular/router"

export const userMatcher = (
  segments: UrlSegment[],
  group: UrlSegmentGroup,
): UrlMatchResult => {
  return { consumed: segments };
  
}