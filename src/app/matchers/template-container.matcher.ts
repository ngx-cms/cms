import { UrlMatchResult, UrlSegment, UrlSegmentGroup } from "@angular/router"

export function templateMatcher(segments: UrlSegment[],group: UrlSegmentGroup): UrlMatchResult {

  // Template matcher, returns URL segments when url from `/shared/constants` sidebar navs matched
  const templateUrls = [
    'pages',
    'posts',
    'navs',
    'custom-fields',
    'users',
    'site-options',
    'database',
  ];

  const pageTypes = [
    'all',
    'new',
    'edit',
    'manage',
    'collections'
  ];

  if (segments.length) {
    if (templateUrls.includes(segments[0].path)) {
      if (segments.length > 1) {
        const pageType = segments[1]?.path;
        if (pageType && pageTypes.includes(pageType)) {
          return { consumed: segments };
        }
        return { consumed: [] };
      }
      return { consumed: segments }
    }
  }

  return null;
  
}

export function tableMatcher(segments: UrlSegment[],group: UrlSegmentGroup): UrlMatchResult {
  const types = [
    'all',
    'new',
    'manage',
  ];

  if (segments.length > 1 && segments[1]?.path === 'all') {
    return { consumed: segments };
  }


  if (segments.length) {
    if (types.includes(segments[0].path)) {
      return { consumed: segments };
    }
  }

  return null;
  
}