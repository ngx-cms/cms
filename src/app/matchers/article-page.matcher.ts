import { UrlMatchResult, UrlSegment, UrlSegmentGroup } from "@angular/router"
import { AppStateService } from "../services/app-state/app-state.service";

export function ArticleMatcher(
  segments: UrlSegment[],
  group: UrlSegmentGroup,
): UrlMatchResult {

  // Template matcher, returns URL segments when url from `/shared/constants` sidebar navs matched
  console.log('matcher: ', segments)
  const categories = [
    'articles',
    'technology'
  ];

  if (categories.includes(segments[0].path)) {
    return {
      consumed: segments
    }
  }

  return null;
  
}