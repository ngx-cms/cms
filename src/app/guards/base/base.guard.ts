import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppStateService } from '../../services/app-state/app-state.service';
import { UserAuthService } from '../../services/user/user-auth/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class BaseGuard implements CanActivate {

  constructor(
    private userAuthService: UserAuthService,
    private router: Router,
    private appStateService: AppStateService,
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.appStateService.isBrowser) {
      if (this.userAuthService.getUserSession() === null) {
        this.router.navigate(['/login', { origin: state.url }]);
        return false;
      }
    
      // Init appState if user is logged in
      this.appStateService.init();
    }


    return true;
  }
  
}
