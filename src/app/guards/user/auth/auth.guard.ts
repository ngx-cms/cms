import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserAuthService } from '../../../services/user/user-auth/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private forbiddenRoutes = [
    'users',
  ];

  constructor(
    private userAuthService: UserAuthService,
    private router: Router,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const userSession = this.userAuthService.getUserSession();
    const category = state.url.split('/')[2];
    if (this.forbiddenRoutes.includes(category) && userSession.role !== 1) {
      this.router.navigate(['/cms/forbidden', { origin: state.url }])
      return false;
    }
    return true;
  }
  
}
