export interface CollectionConfig {
  [prop: string]: TableConfig;
}


export interface TableConfig {
  table: {
    data?: string; // API response key to pull data from
    columns?: TableColumns[]; // Table header key:pair on table.data
    actions?: TableActions; // enables edit | delete
    version?: number; // 1 | 2
  }
}

interface TableColumns {
  label: string;
  key: string;
  type?: 'array' | 'string' | 'number' | 'object'
}

interface TableActions {
  edit?: {
    enabled?: boolean;
    load_template?: boolean;
  }
}