export interface Alert extends AlertTypes {
  enabled?: boolean;
  text?: string;
};


export interface AlertTypes {
  type?: 'success' | 'warning' | 'danger' | 'info';
}