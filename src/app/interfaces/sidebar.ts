export interface Sidebar {
  label?: string;
  url?: string;
  icon?: string;
  event?: {
    type?: string;
    function?: string;
    params?: any;
    value?: any;
  };
  is_hidden?: boolean;
  options?: SidebarNavsOptions;
  clickable?: boolean;
  sitemap?: boolean;
  category?: any;
  base_url?: string;
}

export interface SidebarNavs extends Sidebar {
  children?: SidebarNavsChildren[] | boolean;
}

export interface SidebarMobileBars {
  event?: {
    type: string;
    function?: string;
  },
  icon?: string;
  label?: string;
}

export interface SidebarNavsOptions extends Sidebar {
  default_children?: boolean;
  children?: {
    default?: boolean;
    parent_url?: boolean;
  };
}

export interface SidebarNavsChildren extends Sidebar {
  label?: string;
  url?: string;
}