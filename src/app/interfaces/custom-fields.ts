export interface FormFields {
  value_type?: string;
  value_type_enabled?: boolean;
  name: string;
  type: string;
  label?: string;
  value?: string | number | boolean | string[];
  required?: boolean;
  options?: FieldOptions[];
  id?: string;
  classes?: string | object | string[];
  placeholder?: string;
  depends?: FieldDependency[];
  is_hidden?: boolean;
  field_groups?: FieldGroups[][];
  dynamic_label?: {
    field: string;
    prefix?: boolean;
    suffix?: boolean;
    label?: string;
  };
  config?: {
    show_on_field?: number;
    value_as_array?: boolean;
    depends?: {
      match_all?: boolean;
      match_false?: 'disable' | 'hide';
    }
    advance_config?: boolean;
  };
  dynamic_values?: any;
  auto_values?: any;
  fields?: any;
  field_class?: string | string[] | object;
  category?: string[];
  enable_dependency?: boolean;
  tooltip?: Tooltip;
  disabled?: boolean;
  form?: {
    id?: string;
    name?: string;
  };
  custom_field?: {
    id?: string | number;
    name?: string | number;
  };
  minimize?: boolean;
  repeater?: boolean;
  prevent_default?: boolean;
  attributes?: FieldAttributes;
  form_name?: string;
  valid?: boolean;
}

export interface ToolbarActions extends ToolbarActionState {
  inputs?: any;
}

export interface FieldGroupsActions extends ToolbarActions {
  field?: FormFields | FormFields[];
  fieldGroups?: FieldGroups | FieldGroups[];
  fieldIndex?: number;
}

export interface ToolbarActionState {
  action: 'remove' | 'add' | 'toggle';
}

export interface FormControlData {
  field: FormFields;
  $event: Event;
}

export interface FormControlInputs {
  field?: FormFields;
  referenceField?: FormFields[];
  field_index?: number;
  name?: string;
  classes?: string;
}

export interface FieldAttributes {
  disabled?: boolean;
  required?: boolean;
  accept?: Blob;
  alt?: string;
  autocomplete?: boolean;
  autofocus?: boolean;
  capture?: boolean;
  checked?: boolean;
  dirname?: string;
  id?: string;
  form?: any;
  formaction?: any;
  formmethod?: 'POST' | 'GET' | 'PUT' | 'PATCH' | 'DELETE';
  formenctype?: any;
  formnovalidate?: boolean;
  formtarget?: string;
  height?: string | number;
  list?: any;
  max?: number;
  maxlength?: number;
  min?: number;
  minlength?: number;
  multiple?: any;
  name?: string;
  pattern?: string;
  placeholder?: string;
  readonly?: boolean;
  size?: number;
  src?: string;
  step?: number;
  type?: string;
  value?: any;
  width?: number;
};

export interface Tooltip {
  enabled?: boolean;
  title?: string;
  text?: any;
}
export interface FieldOptions {
  value: string | number | boolean;
  label: string | number;
}

export interface FieldDependency {
  rvalue: string | number | boolean | any[];
  lvalue?: string | number;
  comparator?: string;
  eval?: boolean;
}

export interface FieldGroups {
  name: string | number;
  label?: string | number;
  type: string;
  value?: any;
  required?: boolean;
  dynamic_values?: DynamicFieldValues[];
  options?: FieldOptions[];
  is_hidden?: boolean;
}

export interface MissingFields {
  message?: string;
  type?: string;
}

export interface DynamicFieldValues {
  field?: string;
  name?: string;
}

export interface CustomFieldConfig {
  development?: boolean;
  is_edit?: boolean;
  emit?: {
    on?: string;
    is_preview?: boolean;
    is_code?: boolean;
  };
  load_template?: boolean;
  v2?: boolean;
  is_minimized?: boolean;
}

export interface FieldTiles {
  name?: string;
  tooltip?: Tooltip;
}

export interface RecordFields {
  config?: {
    id?: string;
    is_permanent?: boolean
  };
  fields?: Array<object>;
  id?: string;

}