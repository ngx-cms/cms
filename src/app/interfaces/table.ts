import { FormFields, Tooltip } from "./custom-fields";

export interface Table {
}

export type TableActions = 'edit' | 'delete' | 'export' | 'export-many' | 'sort';

export interface TableConfig {
  headers?: TableHeaders[];
  actions?: {
    edit?: boolean | Function;
    delete?: boolean | Function;
    export?: boolean | Function;
  } | boolean;
  sortable?: boolean;
  enable_quick_search?: boolean;
  position?: string;
  pagination?: boolean;
}

export interface TableHeaders {
  value: string;
  label: string;
  transform?: string | Function;
  html?: boolean;
  tooltip?: Tooltip;
}

export interface TableHelpers {
  label?: string;
  name?: string;
  type?: string;
  value?: any;
  options?: any[];
  transform?: string;
  placeholder?: string | number;
  display?: 'value' | 'label' | 'both';
}

export interface TableAction extends TableEdit {
  message?: string;
  type?: string;
}

export interface TableEdit {
  collection?: string;
  enabled?: boolean;
  template?: FormFields[][];
  data?: {
    fields?: FormFields[];
    id?: string | number;
  };
  loaded?: boolean;
  message?: string;
}

export interface TableForm extends CustomFieldForm {
  fields?: FormFields | FormFields[];
  id?: string | number;
  form_name?: string | number;
}

export interface CustomFieldForm {
  missing_fields?: string[] | FormFields[];
  type?: string;
  code?: string;
  category?: string[];
  is_hidden?: boolean;
}