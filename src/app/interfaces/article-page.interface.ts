import { Footer } from "./footer.interface";

export interface SiteOptions {
  footer_config?: Footer;
  article_page_config?: any;
}