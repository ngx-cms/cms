export interface RouteState extends WindowLocation {
  category?: string;
  queryParams?: any;
  slugs?: string[];
  type?: string;
  fragment?: any;
  url?: string;
  ssr?: boolean;
  record?: string | number;
  referrer?: string;
  snapshots?: any;
  parent_category?: string;
}

export interface WindowLocation {
  host?: string;
  hostname?: string;
  port?: string;
  href?: string;
  origin?: string;
}