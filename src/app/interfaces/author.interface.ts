export interface Author {
  profile_image?: string;
  name: string;
  title?: string;
  link?: string;
}