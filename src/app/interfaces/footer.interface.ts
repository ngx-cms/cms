export interface Sitemap {
  value?: string;
  label?: string;
}

export interface Footer {
  email?: string;
  about?: string;
  contact?: string;
  address?: string;
  sitemap?: Sitemap[];
}
