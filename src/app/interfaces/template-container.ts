import { FormFields } from "./custom-fields";

export interface TemplateContainer {
}


export interface Template {
  message?: string;
  isLoading?: boolean;
}

export interface TemplateFinalForm {
  fields?: FormFields[];
  id?: string | number;
  form_name?: string | number;
  missing_fields?: string[];
}