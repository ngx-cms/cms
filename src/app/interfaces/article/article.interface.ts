export interface Article {
  author: ArticleAuthor;
}

export interface ArticleAuthor {
  id: string;
}