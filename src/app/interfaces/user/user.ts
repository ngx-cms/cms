export interface User {
  is_logged_in?: boolean;
  role?: number;
  first_name?: string;
  last_name?: string;
  id?: string;
  username?: string;
  email?: string;
  owner?: {
    id: string;
  }
}
