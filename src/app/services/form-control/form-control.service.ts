import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { FormFields } from '../../interfaces/custom-fields';

@Injectable({
  providedIn: 'root'
})
export class FormControlService {

  private attributes = [
    // 'disabled',
    'required',
    'accept',
    'alt',
    'autocomplete',
    'autofocus',
    'capture',
    'checked',
    'dirname',
    'id',
    'form',
    'formaction',
    'formmethod',
    'formenctype',
    'formnovalidate',
    'formtarget',
    'height',
    'list',
    'max',
    'maxlength',
    'min',
    'minlength',
    'multiple',
    'name',
    'pattern',
    'placeholder',
    'readonly',
    'size',
    'src',
    'step',
    'type',
    'value',
    'width',
  ];
  constructor() { }

  setAttributes(field: FormFields) {
    cloneDeep(this.attributes).forEach(attr => {
      if (field[attr] && !field.attributes[attr]) {
        field.attributes[attr] = field[attr];
      }
    });

    return field;
  }

  setElementAttributes(field: any, $el?: Element) {
    if (!$el) {
      console.log($el, ' not defined');
      return;
    }
    cloneDeep(this.attributes).forEach(attr => {
      if (field.attributes && field.attributes[attr]) {
        $el.setAttribute(attr, field.attributes[attr])
      }

      if (field[attr]) {
        $el.setAttribute(attr, field[attr])
      }
      // if (field?.attrs?.hasOwnProperty(attr) && field?.attrs[attr] !== undefined) {
      //   $el.setAttribute(attr, field.attributes[attr])
      // }
    })
  }

  getFieldClasses(field: FormFields): string {
    if (field.type === 'select') {
      return 'form-select form-select-sm';
    }
    if (field.type !== 'radio' && field.type !== 'checkbox') {
      return 'form-control form-control-sm';
    }

    return 'ml-2 mt-2';
  }

  // setDefaults(obj, type) {
  //   if (!defaults[type]) {
  //     console.warn(`Undefined default value '${type}'`);
  //     return;
  //   }
  //   obj = merge(obj, defaults[type]);
  //   return obj;
  // }
}
