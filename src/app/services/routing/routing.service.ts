import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { formatCategory, toUrl } from '../../helpers/string.helper';
import { SidebarNavs } from '../../interfaces/sidebar';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  getUrl(): string {
    return this.router.url;
  }

  routerState() {
    return this.router.events;
  }

  goto(url, extras?: NavigationExtras) {
    return this.router.navigate([`${url}`], extras);
  }

  changeQueryParam(params) {
    const decodeParams = {};
    Object.keys(params).forEach(param => {
      decodeParams[param] = decodeURIComponent(params[param]);
    });

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: decodeParams,
      queryParamsHandling: 'merge',
    });
  }

  generateChildNav(nav: SidebarNavs): any {
    let navs = [];
    if ((!nav?.label) || !nav.children) {
      return null;
    }

    const label = nav.label?.toLowerCase();
    let url = nav.base_url || label.replace(' ', '-');
    let options = nav?.options || { children: {}};
    let childOptions = options?.children;
    if (!childOptions?.hasOwnProperty('parent_url')) {
      childOptions.parent_url = true;
    }
    const param = nav.category? { category: nav.category } : null;
    if (!url.startsWith('/cms')) {
      url = `/cms/${url}`;
    }

    if (childOptions?.default === true || nav.children === true) {
      navs = [
        {
          url: `${url}/new`,
          label: `Add ${formatCategory(label)}`,
          icon: `<i class="fas fa-plus"></i>`,
          params: param,
        },
        {
          url: `${url}/all`,
          label: `All ${label}`,
          icon: `<i class="fas fa-table"></i>`,
          params: param,
        },
      ];
    }

    // Execute `nav.options.children`
    if (childOptions) {

      if (childOptions.parent_url && Array.isArray(nav.children)) {
        nav.children = (<Array<any>>nav.children).map(child => {
          return {
            ...child,
            url: (!child.url
                    ? `${nav.url}/${child.url || toUrl(child.label)}`
                    : child.url
                 ),
          }
        });
      }

      // Add default child `add` / `new`
      if (childOptions.default || nav.children && Array.isArray(nav.children)) {
        Array.prototype.push.apply(navs, nav.children);
      }
    }

    return navs;
  }
}
