import { Injectable } from '@angular/core';
import { WindowRef } from '../../dom-abstraction/window.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private window: Window;

  constructor(
    private winRef: WindowRef,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  /**
   * Set value to window.localStorage
   * @param key - {string} value key
   * @param value - {any} value to save
   */
  setItem(key: string, value: any): void {
    if (typeof value !== 'string') {
      value = JSON.stringify(value);
    }
    this.window.localStorage.setItem(key, value);
  }

  /**
   * Get value from window.localStorage
   * @param key - {string} value key to retrieve
   * @param parse - {boolean} - whether automatically parse the retrieved data into its respective type
   * @returns saved parsed value
   */
  getItem(key: string, parse?: boolean): any {
    if (key === '') {
      console.log(`Key is empty`)
      return;
    }

    const item = this.window?.localStorage?.getItem(key);
    if (parse) {
      return JSON.parse(item)
    }

    return item;
  }


  /**
   * Set value to window.sessionStorage
   * @param key - {string} value key
   * @param value - {any} value to save
   */
  setSessionItem(key: string, value: any): void {
    if (typeof value !== 'string') {
      value = JSON.stringify(value);
    }
    this.window.sessionStorage.setItem(key, value);
  }

  /**
   * Get value from window.sessionStorage
   * @param key - {string} value key to retrieve
   * @param parse - {boolean} - whether automatically parse the retrieved data into its respective type
   * @returns saved parsed value
   */
  getSessionItem(key: string, parse?: boolean): any {
    if (key === '') {
      console.log(`Key is empty`)
      return;
    }

    const item = this.window.sessionStorage.getItem(key);
    if (item && parse) {
      return JSON.parse(item)
    }

    return item;
  }


  removeItem(key?: string): void {
    this.window.localStorage.removeItem(key);
  }

  removeSessionItem(key?: string): void {
    this.window.sessionStorage.removeItem(key);
  }

}
