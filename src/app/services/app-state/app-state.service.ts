import { Inject, Injectable, isDevMode, PLATFORM_ID } from '@angular/core';
import { Event, NavigationEnd, PRIMARY_OUTLET, Router, RouterEvent, UrlTree } from '@angular/router';
import { combineLatest, Observable, of, ReplaySubject } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, takeWhile } from 'rxjs/operators';
import { FormFields } from '../../interfaces/custom-fields';
import { RouteState } from '../../interfaces/route-state';
import { BackendService } from '../backend/backend.service';
import * as _ from 'lodash';
import { ApiResponse } from '../../../shared/interfaces/api';
import { WindowRef } from '../../dom-abstraction/window.service';
import { isEqual } from 'lodash';
import { SeoService } from '../seo/seo.service';
import { hasKey, keys, ucWords } from '../../helpers/string.helper';
import { sidebar } from '../../../shared/constants';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AppStateService {

  private routeState$ = new ReplaySubject<any>(1);
  private customFields$ = new ReplaySubject<any>(1);
  private recordFields$ = new ReplaySubject<any>(1);
  private siteOptions$ = new ReplaySubject<any>(1);
  private routedState: RouteState = {};
  private savedCustomFields = {};
  private viewState = 0;
  private window: any;
  private appStateInitialised = false;
  private sitemap$ = new ReplaySubject<any>(1);
  private articleCategories = [];
  private TRANSFER_KEY = makeStateKey('siteoptions');
  private browserState: boolean;
  private serverState: boolean;
  private sitemap: any[];
  private collections: any[];
  private collections$ = new ReplaySubject<any>(1);
  url = '';

  constructor(
    private router: Router,
    private backendService: BackendService,
    private winRef: WindowRef,
    private seoService: SeoService,
    private transferState: TransferState,
    @Inject(PLATFORM_ID) private platformId: Object,
  ) {
    this.window = this.winRef.nativeWindow;
    this.browserState = isPlatformBrowser(platformId);
    this.serverState = isPlatformServer(platformId);
  }

  get isBrowser() {
    return this.browserState;
  }

  get isServer() {
    return this.serverState;
  }

  getSlugs() {
    return this.window.location.pathname.split('/').filter(slug => slug !== '');
  }

  setViewStatus(viewState) {
    this.viewState = viewState;
  }

  setArticleCategories(categories) {
    this.articleCategories = categories;
  }

  getArticleCategories() {
    return this.articleCategories;
  }

  isMobile(): boolean {
    return ((this.viewState || this.window.innerWidth)) < 768;
  }

  isTablet(): boolean {
    return this.viewState >= 768 && this.viewState < 992;
  }

  init() {
    if (!this.appStateInitialised) {
      this.appStateInitialised = true;
      this.initRouter();
    }
  }

  test() {
    return this.backendService.get('site/siteoptions')
      .pipe(distinctUntilChanged((a,b) => isEqual(a, b)))
  }

  fetchSiteOptions() {
    const siteOptions = this.transferState.get(this.TRANSFER_KEY, <any>{});
    if (hasKey(siteOptions)) {
      this.siteOptions$.next(siteOptions);
      return;
    }
    this.backendService.fetchSiteOptions()
      .pipe(distinctUntilChanged((a,b) => isEqual(a, b)))
      .subscribe((res: any) => {
        this.transferState.set(this.TRANSFER_KEY, res);
        this.siteOptions$.next(res);
      });
  }

  fetchSitemap() {
    // return this.backendService.fetchSitemap().pipe(switchMap(sitemap => this.setSitemap(sitemap)));
  }

  setSitemap(sitemap) {
    this.sitemap = sitemap;
  }

  getSitemap() {
    return this.sitemap;
  }

  setCollections(collections) {
    this.collections = collections;
    this.collections$.next(collections);
  }

  getCollections() {
    return this.collections;
  }

  listenCollections(): Observable<any[]> {
    return this.collections$;
  }

  getSiteOptions() {
    return this.siteOptions$;
  }

  updateSiteOptions(siteOptionsConfig) {
    // this.siteOptions$.next({});
    console.log('update site options: ', siteOptionsConfig);
    
    this.siteOptions$.next(siteOptionsConfig);
  }

  initRouter() {
    this.fetchSiteOptions();
    this.fetchSitemap();
    this.routeState()
      .pipe(
        filter((e: Event) => e instanceof NavigationEnd),
      )
      .subscribe((event: RouterEvent) => {
        const urlTree: UrlTree = this.router.parseUrl(event.url);
        const paths: string[] = [];
        const  { location } = this.window;
        const qParams = urlTree.queryParams;
        if (event.url && event.url !== '/') {
          const { segments } = urlTree.root.children[PRIMARY_OUTLET];
          segments.forEach(segment => paths.push(segment.path));
        }
  
        this.url = event.url;
        const type = paths[0] === 'cms' ? paths[2] : paths[1];
        let category = paths[0] === 'cms' ? paths[1] : paths[0];
        if (category === 'database' && paths[2]) {
          category = paths[2];
        }
        if (category === 'collections' && hasKey(qParams)) {
          if (qParams.category) {
            category = qParams.category;
          }
        }

        this.routedState = {
          host: location.host,
          hostname: location.hostname,
          port: location.port,
          href: location.href,
          origin: location.origin,
          queryParams: qParams,
          slugs: paths,
          fragment: urlTree.fragment,
          url: event.url,
          category: category,
          type: type,
          ssr: isDevMode(),
          record: paths[3] || null,
          snapshots: this.convertSnapshot(event.url.split(';').filter(u => !u.startsWith('/'))),
        };

        keys(qParams).forEach(key => {
          this.routedState[key] = qParams[key];
        });

        this.window.routeState = this.routedState;
        // if (sidebar.navs.some(nav => nav.url === category)) {
        this.seoService.setPageTitle(`${ucWords(category) || 'CMS'} | NGCC-CMS`);
        // }
        
        this.routeState$.next(this.routedState);
      });
  }

  getACF(url, params?: {}) {
    // this.backendService.getACF(url, params)
  }

  convertSnapshot(snapshots: string[]) {
    // const snapshot = snapshots.split('=');
    const newSnapshots = {};
    snapshots.forEach(snapshot => {
      const snapshotArr = snapshot.split('=');
      newSnapshots[snapshotArr[0]] = snapshotArr[1];
    });

    return newSnapshots;
  }

  fetchAcfConfig() {
    return this.backendService.get('acf.config', {}, { v2: true })
      .pipe(switchMap(config => {
        return combineLatest(
          of(config),
          this.backendService.get('acf.fields', {}, { v2: true }),
        )
      }))
  }

  getApiUrl(routeState: RouteState, fetchRecord) {
    const { type, category, record, parent_category } = routeState;
    let apiUrl = `collections/customfields`;
    if (parent_category && parent_category  === 'database') {
      return `database/${category}`
    } else if (type === 'all') {
      return `collections/${category.replace('-', '')}`;
    } else if (fetchRecord) {
      return `collections/${category.replace(/(-)/gi, '')}?id=${record}`;
    } else if (type === 'edit') {
      // return `collections/${routeState.category}`;
    }

    return apiUrl;
  }

  fetchCustomFields(routeState?: RouteState, fetchRecord?: boolean) {
    this.getFields(routeState)
      .pipe(distinctUntilChanged((a, b) => isEqual(a, b)))
      .subscribe(customFields => this.customFields$.next(customFields))
  }

  preloadFormFields(routeState?: RouteState): Observable<FormFields[][]> {
    return this.getFields(routeState);
  }

  preloadRecordFields(routeState?: RouteState, fetchRecord?: boolean): Observable<FormFields[][]> {
    return this.getFields(routeState, fetchRecord)
  }

  fetchRecordFields(routeState: RouteState, fetchRecord?: boolean) {
    this.getFields(routeState, fetchRecord)
      .pipe(distinctUntilChanged((a, b) => isEqual(a, b)))
      .subscribe((res: FormFields[][]) => this.recordFields$.next(res));
  }

  getFields(routeState, fetchRecord?: boolean): Observable<any> {
    const apiUrl = this.getApiUrl(routeState, fetchRecord);
    return this.backendService
      .get(apiUrl)
      .pipe(
        distinctUntilChanged((a, b) => isEqual(a, b)),
        map((res: ApiResponse) => res.code ? this.filterFields(res, routeState) : [])
      );

  }

  private filterFields(res, routeState) {
    const invalidFields = [];
    return res.data.filter(formField => {
      const category = formField.config?.category;
      const excluded_category = formField.config?.category_excluded;

      formField.fields?.forEach(field => {
        field = _.merge(field, formField.config);
        // field.config = Object.assign(field.config, formField.config)
        // formField.config.category_excluded = field?.category_excluded;
      });

      // if (!formField.fields) {
      //   return null;
      // }

      if (routeState.category === 'custom-fields') {
        return formField;
      }

      if (excluded_category?.length) {
        return !excluded_category.includes(routeState.category);
      }

      if (!category) {
        return formField;
      }
        
      if (Array.isArray(category) && category.length) {
        if (category.includes(routeState.category)) {
          return formField;
        }

        return null;
      }

      if (routeState.type !== 'edit') {

        if (routeState?.slugs?.length > 2 && routeState?.slugs[3] === formField.id) {
          return formField;
        }

        if (!formField.fields) {
          invalidFields.push(formField);
          // console.log('Null field detected. Not initialised: ' + JSON.stringify(formField), 'color: red');
          return null;
        }

        return null;
      }

      return formField;

    });
  }

  routeState() {
    return this.router.events;
  }

  getRoutedState() {
    return this.routedState;
  }

  getRouteState(): Observable<any> {
    return this.routeState$.pipe(distinctUntilChanged());
  }

  getRecordFields(): Observable<FormFields[][]> {
    return this.recordFields$.pipe(distinctUntilChanged((a, b) => isEqual(a, b)));
  }

  getCustomFields(): Observable<FormFields[][]> {
    return this.customFields$.pipe(distinctUntilChanged((a, b) => isEqual(a, b)));
  }

  isOffline(): boolean {
    if (!this.window.navigator.onLine) {
      this.window.alert(`You're offline.`);
    }

    return !this.window.navigator.onLine;
  }

  isDev(): boolean {
    return this.routedState.hostname.indexOf('localhost') !== -1;
  }

  routeChanged(changed: boolean) {
    return 
  }
}
