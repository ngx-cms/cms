import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { apiVersion } from '../../../shared/configs/configs';
import { WindowRef } from '../../dom-abstraction/window.service';
import { cloneDeep } from '../../helpers/string.helper';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private apiUrl = environment.api;
  private window: Window;
  private STATE_KEY = makeStateKey('sitemap');

  constructor(
    private http: HttpClient,
    private winRef: WindowRef,
    private transferState: TransferState,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  async getSiteOption() {
    const siteOption = await (await fetch(`${this.apiUrl}api/v1/collections/siteoptions`)).json();
    if (siteOption.api_url) {
      this.apiUrl = siteOption.api_url;
    }
  }

  fetchSitemap() {
    const sitemap = this.transferState.get(this.STATE_KEY, <any>[]);
    if (sitemap.length) {
      return of(sitemap);
    }
    return this.http.get(`${this.apiUrl}sitemap`);
  }

  fetchSiteOptions(params?: {}) {
    // return await (await fetch(`${this.apiUrl}api/v1/collections/siteoptions`)).json();
    return this.http.get(`${this.apiUrl}siteoptions`, {params});
  }

  setApiUrl() {
    if (environment.production) {
      this.apiUrl = environment?.api;
    } else if (environment?.ssr) {
      this.apiUrl = `${this.window.location.origin}/`
    }
  }

  getApiVersion(): string {
    return `v${apiVersion}`;
  }

  get(url, params?: {}, configs = { v2: false }) {
    let apiUrl = this.apiUrl;
    if (url.indexOf('http') !== -1) (apiUrl = url);
    const apiVersion = configs?.v2 ? 'v2' : this.getApiVersion();

    return this.http.get(`${apiUrl}api/${apiVersion}/${url}`, {params});
  }

  getCustom(url, params, conigs = { v2: false }) {
    return this.http.get(`${this.apiUrl}${url}`, {params});
  }

  getACF(url, params?: {}) {
    return this.http.get(`${this.apiUrl}${url}`, { params });
  }

  getArticle(url, params?: {}) {
    return this.http.get(`${this.apiUrl}${url}`);
  }

  post(url, params, configs?: {}) {
    let apiUrl = this.apiUrl;
    if (url.indexOf('http') !== -1) {
      apiUrl = '';
    }

    return this.http.post(`${apiUrl}${url}`, params).pipe(
      catchError(err => {
        if (err) {
          return this.http.post(`http://localhost:3000/${url}`, params);
        }
      })
    );
  }
}
