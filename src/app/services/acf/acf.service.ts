import { Injectable } from '@angular/core';
import { cloneDeep, flatMap, keys } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import generateID from 'generate-unique-id';
import { AppStateService } from '../app-state/app-state.service';
import { UserAuthService } from '../user/user-auth/user-auth.service';
import {
  flatMapDeep,
  hasKey,
  hasProperty,
  isArray,
  isBoolean,
  isFunction,
  isNumber,
  isObject,
  isString,
  mergeArrayObjects,
  parseValueType
} from '../../helpers/string.helper';
import { BackendService } from '../backend/backend.service';
import { User } from '../../interfaces/user/user';
import { ACFField } from '../../components/acf/acf.interface';
import { ApiResponse } from '../../../shared/interfaces/api';
import { ACF, acfConfig } from '../../components/acf/acf.mock';
import { DocumentRef } from '../../dom-abstraction/document.service';
import { TransformService } from '../transform/transform.service';
import { dateNow, getDate, getDateTime, getYear } from '../../helpers/date.helper';
import { FormatService } from '../transform/format.service';


@Injectable({
  providedIn: 'root'
})
export class AcfService {

  private idConfig = {
    useLetters: false,
    length: 10,
  };

  private user: User;
  private document: Document;
  private FIELD_WITH_OPTIONS = [
    'select',
    'radio',
  ];

  constructor(
    private backendService: BackendService,
    private appStateService: AppStateService,
    private userAuthService: UserAuthService,
    private docRef: DocumentRef,
    private transformService: TransformService,
    private formatter: FormatService,
  ) {
    this.user = this.userAuthService.getUserSession();
    this.document = docRef.nativeDocument;
  }

  createID() {
    return `${getYear()}${generateID(this.idConfig)}`;
  }

  defaultValue(field) {
    switch (field.default_value_type) {
      case 'id()':
        if (field.value) return field.value;
        return `${getYear()}${generateID(this.idConfig)}`;
      case 'current_user()':
        return this.user.id;
      case 'date_now()':
        return getDate();
      case 'date_time_now()':
        return getDateTime();
      default:
        break;
    }
  }

  initACF(acf: ACFField[][], refField?: ACFField) {
    if (!acf) {
      console.error('INVALID ACF: ', acf)
      return;
    }
    acf.forEach((fields: ACFField[], fieldsIndex: number) => {
      fields.forEach(async (field: ACFField) => {
        if (field.unique_value) {
          delete field.valid;
        }
        if (!field.value && (!isBoolean(field.value) && !isNumber(field.value))) {
          field.value = '';
        }

        if (field.default_value === 'auto' && !field.value) {
          field.value = this.defaultValue(field);
        }

        // if (field.default_value === 'auto' && field.value) {
        //   field.value = this.defaultValue(field);
        // }

        this.formatValue(field);

        // Init value types
        if (field.enable_value_type === true) {
          field.value = parseValueType(field.value, field.value_type)
        }

        if (field.depends) {
          this.initDepends(fields, field);
        }

        if (!field.hidden || field.auto_values) {

          if (field.field_groups) {
            this.doCheckFieldGroups(field, acf, fieldsIndex);
          }
      
          // auto values
          if (refField?.name !== field.name && field.auto_values) {
            if (isString(field.auto_values)) {
              field.auto_values = [{ field: field.auto_values }];
            }
            if (!field.options) {
              field.options = [];
            }
            this.doCheckAutoValues(field, fields, acf);
          }
      
          if (field.dynamic_values) {
            this.doCheckDynamicValue(field, acf, fieldsIndex)
          }

          if (field?.transform) {
            try {
              field = this.transformService[field.transform]({
                field,
                fields,
                acf,
              });
            } catch (e) {
              console.error(`Transform error: unknown transform ${field.label}:${field.transform}.`)
            }
          }

          if (field.type === 'select' && field.option_type && !isArray(field.options)) {
            field.options = await this.initOptions(field);
          }

          if (!this.FIELD_WITH_OPTIONS.includes(field.type) && field.options) {
            delete field.options;
          }
        }

        if (field.type === 'form_group') {
          if (!field.fields) {
            field.fields = cloneDeep(field.form_group);
          }
          this.initACF(field.fields || field.form_group);
        }
      });
    });
  }

  async initOptions(field) {
    switch (field.option_type) {
      case 'auto':
        let users: ApiResponse = await this.getUsers().toPromise();
        users = users.filter(user => user.id !== this.user.id);
        const sessionUser = [{
          label: (
            (this.user.first_name && this.user.last_name)
              ? `${this.user.first_name} ${this.user.last_name}`
              : this.user.username || this.user.email
          ) + `(Current User)`,
          value: this.user.id,
        }];
        const newUsers = sessionUser.concat(users.filter(rec => rec.fields).map(rec => {
          let user = mergeArrayObjects(rec.fields);
          let userKeys = {};
          if (user) {
            for (let userKey in user) {
              if (isArray(user[userKey])) {
                userKeys = { ...userKeys, ...mergeArrayObjects(user[userKey]) };
                delete user[userKey];
              }
            }
            user = { ...user, ...userKeys };
            return {
              label: (
                (user.first_name && user.last_name)
                  ? `${user.first_name} ${user.last_name}`
                  : user.username || user.email
              ),
              value: user.id,
            }
          }
          return {};
        }));
        // console.log('newUsers: ', newUsers)
        return newUsers;
      default:
    }
  }

  private getUsers() {
    return this.backendService.get('users', {}, { v2: true })
  }

  private formatValue(field) {
    if (field.enable_auto_format === true) {
      if (field.format_type === 'simplified') {
        field.value = this.formatter[field.format](field.value);
      } else if (field.format_type === 'custom') {
        try {
          const tryer = field.value[field.format_type];
          if (tryer) {
            field.value = field.value[field.format_type];
          }
        } catch (e) {
          console.log(`${field.format} is not a valid formatter.`)
        }
      }
    }
  }

  private doCheckFieldGroups(field, acf, fieldsIndex) {
    let disabled = false;
    field.field_groups.forEach(fieldGroups => {
      fieldGroups.forEach(fieldGroup => {

        if (fieldGroup.transform) {
          fieldGroup = this.transformService[fieldGroup.transform]({
            field: fieldGroup,
            fields: fieldGroups,
            acf,
          });
        }

        if (fieldGroup.dynamic_values) {
          this.doCheckDynamicValue(fieldGroup, acf, fieldsIndex);
        }

        if (hasProperty(fieldGroup, 'options') &&
            !fieldGroup.options.length) {
          disabled = true;
        }
      });

      field.field_groups_disabled = disabled;
    });

    if (field.field_groups_disabled) {
      flatMapDeep(field.field_groups).forEach(fieldGroup => {
        fieldGroup.disabled = true;
      });
    }
  }

  private doCheckAutoValues(field, fields, acf) {
    field.auto_values.forEach(values => {
      const autoField = fields.find(f => f.name === values.field);
      if (autoField) {
        field.value = autoField.value;
        this.formatValue(field);
      }
    });
  }

  private doCheckDynamicValue(field, acf, index) {
    if (field.type === 'select') {
      field.options = [];
    }
    acf.forEach(fields => {
      let option = {};
      field.dynamic_values.forEach(values => {
        fields.forEach(f => {
          if (f.name === values.field) {
            option[values.name] = f.value;
            switch (field.type) {
              case 'text':
              case 'number':
                field.value = f.value;
                break;
              case 'select':
                option[values.name] = f.value;
                break;
              default:
                break;
            }
          }
        });
      });

      if (field.type === 'select') {
        field.options.push(option);
      }
    });

    field.options.splice(index, 1);
  }

  private initDepends(fields, field) {
    const maxMatch = field.depends.length - 1;
    let match = 0;
    field.depends.forEach(dep => {
      let { lvalue, comparator, rvalue, transform } = dep;
      const dependent = fields.find(f => f.name === lvalue);
      if (dependent && !dependent.hidden) {
        lvalue = dependent.value;
        const matched = this.initDependsComparator(lvalue, comparator, rvalue);
        if (matched) match += 1;
      }
    });
    field.hidden = field.matchAll ? match === maxMatch : match < 1;
  }

  private initDependsComparator(lvalue, comparator, rvalue) {
    if (!comparator) comparator = 'eq';
    lvalue = lvalue?.toString();
    rvalue = rvalue?.toString();
    switch (comparator) {
      case 'eq':
        return lvalue == rvalue;
      case 'ne':
        return lvalue != rvalue;
      case 'gt':
        return lvalue > rvalue;
      case 'gte':
        return lvalue >= rvalue;
      case 'lt':
        return lvalue < rvalue;
      case 'lte':
        return lvalue <= rvalue;
      case 'has':
        return rvalue?.indexOf(lvalue) !== -1;
      default:
        console.log(`Unknown comparator ${comparator}. Returning true.`);
        return true;
    }
  }

  buildEditTemplate(template, field) {
    return template.map(temp => {
      const fieldTemp = field[temp.name];
      if (fieldTemp || (isBoolean(fieldTemp) || isNumber(fieldTemp))) {
        temp.value = fieldTemp;
        if (temp.field_groups || temp.type === 'field-groups') {
          field[temp.name] = this.initFieldGroups(temp, fieldTemp);
        }

        if (temp.type === 'form_group') {
          field[temp.name] = this.initFormGroup(temp, fieldTemp);
        }
      }

      return temp;
    });
  }

  private initFieldGroups(template, field) {
    if (!Array.isArray(field)) return [];
    field.forEach(opt => {
      template.field_groups.push(
        cloneDeep(template.field_groups[0])
          .map(fg => {
            fg.value = opt[fg.name]
            return fg;
          }));
    });

    // Remove first empty option
    template.field_groups.splice(0, 1);
    return template;
  }
  
  private initFormGroup(template, values) {
    let tempFields = cloneDeep(template.fields[0]);
    values.forEach(value => {
      template.fields.push(cloneDeep(tempFields).map(tempField => {
        if (tempField.type === 'field-groups' && value[tempField.name]) {
          tempField = this.initFieldGroups(tempField, value[tempField.name]);
        }

        if (tempField.type === 'form_group' && value[tempField.name]) {
          tempField = this.initFormGroup(tempField, value[tempField.name]);
        }
        tempField.value = value[tempField.name];
        return tempField;
      }));
    });
    template.fields.splice(0, 1);
    return template;
  }

  acfToEdit(data, type = 'fields') {
    const acfs = [];
    data.forEach(res => {
      const fields = type === 'fields' ? res.fields : [res.config];
      const acf = fields.map(field => {
        const [template]: ACFField[][] = cloneDeep(type === 'fields' ? ACF : acfConfig);
        return this.buildEditTemplate(template, field);
      });
      acfs.push(acf);
    });
    return flatMap(acfs);
  }

  recordToEdit(template, record) {
    const acf = ACF[0];
    record = cloneDeep(record[0]) // This will always Array[0]
    if (!record) return;
    template = template.map(temp => {
      return temp.map(t => {
        const rec = mergeArrayObjects(record.fields, false);
        if (rec && rec[t.name]) {
          t.value = rec[t.name];
        }
        if (t.form_group) {
          t.form_group.forEach(rField => {
            if (rec[t.name]) {
              if (Array.isArray(rec[t.name])) {
                rField.value = rec[t.name][0][rField.name];
              } else {
                rField.value = rec[t.name][rField.name]
              }
            }
          });
          t.fields = [t.form_group];
        }
        for (const templateKey in t) {
          const acfField = acf.find(acfField => acfField.name === templateKey);
          if (acfField) {
            if (acfField.enable_value_type) {
              t[acfField.name] = parseValueType(t[acfField.name], acfField.value_type);
            }
          }
        }
        return t;
      });
    });
    return template;
  }

  fieldstoACF(data, category?: string, type?: string) {
    return data.filter(res => {
      if (type === 'table') return res;
      if (!res.config.category) return res;
      if (res.config.category && res.config.category.includes(category)) return res;
      return null;
    }).map(res => {
      if (res.fields) {
        return res.fields.map(field => {
          if (field.form_group) {
            field.fields = [field.form_group];
            field.form_group = field.form_group;
          }
          return {...field, ...res.config, id: res.id }
        });
      }
      return res; 
    });
  }

  fetchACF(routeState, params: any): Observable<any> {
    return combineLatest(
      of(routeState),
      this.backendService.getACF(`acf`, params),
      // this.backendService.get(`collections/${routeState.category}`),
    )
  }

  getACF(routeState, user, force = false) {
    if (routeState.type === 'all' && !force) {
      return combineLatest(of(routeState))
    }
    return this.fetchACF(routeState, { 'category': routeState.category })
  }

  fetchRecord(routeState, params: any, v2 = false): Observable<any> {
    return this.backendService.get(`${routeState.category}`, params, { v2 })
  }

  getSiteOptions() {
    return this.appStateService.getSiteOptions();
  }

  getRecord(routeState, acf) {
    const { type, category, record } = routeState;
    if (type === 'new' && category !== 'site-options') {
      return combineLatest(
        of(routeState),
        of(acf),
        of(null),
        this.getSiteOptions(),
      );
    }

    let params: any = {};
    let v2 = false;
    if (type === 'edit') {
      params.id = record;
      v2 = true;
    }
    return combineLatest(
      of(routeState),
      of(acf),
      this.fetchRecord(routeState, params, v2),
      this.getSiteOptions(),
    )
  }

  fetchCategoryACF(routeState) {
    return this.backendService.get(`collections/${routeState.category}`)
  }

  private setInvalidFields(field, val) {
    if (!val && (!isBoolean(val))) {
      return field;
    }
  }

  rebuildACF(params) {
    let invalids = [];
    let { user, config, fields, type, id, extract } = params;
    if (!type) type = 'create';
    // Rebuild fields
    let newFields = [];

    // Rebuild config
    let configObj = {};
    if (config) {
      config.forEach(fields => {
        const invalidFields = [];
        fields.forEach((field: ACFField) => {
          if (!field.hidden) {
            if (field.required) {
              const invalidField = this.setInvalidFields(field, field.value);
              if (invalidField) {
                invalidFields.push(invalidField);
              }
            }
            configObj[field.name] = field.value
          }
        });


        if (invalidFields.length) {
          invalids.push(invalidFields);
        }
      });
    }

    const build = this.buildACFFields(fields);
    invalids = invalids.concat(build.invalids);
    newFields = newFields.concat(build.newFields);
    const finalBuild = {
      fields: newFields,
      config: configObj,
      id: id || `${new Date().getFullYear()}${generateID(this.idConfig)}`,
      owner: { id: user.id },
      invalid: invalids,
      extract,
    };
    console.log('ACF: ', cloneDeep(finalBuild))
    return finalBuild;
  }

  private buildACFFields(fields) {
    let build = {
      invalids: [],
      newFields: [],
    };

    fields.forEach(fields => {
      const fieldObj = {};
      fields.forEach((field: ACFField) => {
        if (field.attributes) {
          field = {...field, ...mergeArrayObjects(field.attributes)}
        }
        const invalidFields = [];
        if (!field.hidden || field.name === 'name') {

          if (field.required) {
            const invalidField = this.setInvalidFields(field, field.value);
            if (invalidField) {
              invalidFields.push(invalidField);
            }
          }

          fieldObj[field.name] = field.value;
          if (field.field_groups) {
            const groups = [];
            field.field_groups.forEach(fieldGroups => {
              let group = {};
              fieldGroups.forEach(fieldGroup => {
                group[fieldGroup.name] = fieldGroup.value;
              });
  
              groups.push(group)
            });
            fieldObj[field.name] = groups;
          }
          
          if (field.value_type === 'function') {
            delete fieldObj[field.name];
            try {
              const func = eval(field.value);
              if (isFunction(func)) {
                fieldObj[field.name] = func;
              } else {
                invalidFields.push(field);
              }
            } catch (e) {
              console.log(`${field.value} is not a valid function`)
            }
          }

          // Recursive if `form_group`
          if (field.type === 'form_group') {
            const rebuild = this.buildACFFields(field.fields);
            fieldObj[field.name] = rebuild.newFields;
            if (rebuild.invalids.length) {
              build.invalids.push(rebuild.invalids);
            }
          }
        }

        if (invalidFields.length) {
          build.invalids.push(invalidFields);
        }
      });
      if (keys(fieldObj).length) {
        build.newFields.push(fieldObj);
      }
    });

    return build;
  }


  // TRANSFORMS TABLE DATA
  transformTableData(data, config, siteOptions): any {
    if (!hasKey(config) || !data.length) return data;
    return data.map(record => {
      let newRec = record;
      for (const rec in record) {
        if (isArray(record[rec])) {
          newRec = { ...mergeArrayObjects(record[rec]), ...newRec};
        }
      }
      config.headers.forEach(header => {
        for (const rec in newRec) {
          if (header.transform && isString(header.transform) && header.value === rec) {
            newRec[rec] = this.transformService[header.transform](
              newRec[rec],
              {
                data,
                record: newRec,
                siteOptions,
              }
            );
          }
        }
      });

      return newRec;
    });
  }

  delete(record, category) {
    return this.backendService.post(`api/${category}/delete`, { id: record.id })
  }

  deleteMany(record, category) {
    return this.backendService.post(`api/${category}/delete`, { id: { '$in': record.map(rec => rec.id )} })
  }

  checkInvalidFields(acf) {
    const invalidFields = [];
    acf.forEach((fields, idx) => {
      fields.forEach(field => {
        if (!field.hidden && field.required) {
          field.valid = !(
            !field.value &&
            (!isBoolean(field.value) || isNumber(field.value))
          );

          if (field.valid === false) {
            invalidFields.push({
              field,
              index: idx,
            });
          }
        }

        if (field.type === 'form_group' && field.form_group) {
          this.checkInvalidFields(field.form_group);
        }
      });
    })
    return invalidFields;
  }

  readyExportFile(category, json, name?: string) {
    const a = this.document.createElement('a');
    const blob = new Blob([JSON.stringify(json)], { type: 'application/json' });
    a.href = window.URL.createObjectURL(blob)
    a.setAttribute('download', `${name || category}-${getDate()}.json`);
    this.document.body.append(a);
    a.click();
    this.document.body.removeChild(a);
  }
}
