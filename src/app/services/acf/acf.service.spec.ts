import { TestBed } from '@angular/core/testing';

import { AcfService } from './acf.service';

describe('AcfService', () => {
  let service: AcfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
