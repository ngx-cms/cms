import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  private patterns = {
    url: /[^a-zA-Z0-9-\/ -]+/gi,
  }

  constructor() { }
  
  url(value) {
    const last = value.slice(-1);
    if (this.patterns.url.test(last)) {
      value = value.substring(0, value.length - 1)
    }
    return value.toString().toLowerCase().replace(this.patterns.url, '-');
  }
}
