import { Injectable } from '@angular/core';
import { sidebar, userRoles } from '../../../shared/constants';
import { cloneDeep, flatMapDeep } from '../../helpers/string.helper';
import { AppStateService } from '../app-state/app-state.service';

@Injectable({
  providedIn: 'root'
})
export class TransformService {

  constructor(
    private appStateService: AppStateService,
  ) { }

  autoName(params) {
    const { fields } = params;
    const autoName = fields.find(field => field.name === 'auto_name');
    if (autoName.value) {
      params.field.disabled = true;
      params.field.auto_values = [{ field: 'label'}]
    } else {
      params.field.disabled = false;
      delete params.field.auto_values;
    }
    const value = params.field?.value?.toLowerCase()?.replace(/\W|-/gi, '_');
    params.field.value = value;
    return params.field;
  }

  autoValueOptions(params) {
    params.field.options = [];
    params.acf.forEach(fields => {
      const option = {
        label: fields.find(field => field.name === 'label').value,
        value: fields.find(field => field.name === 'name').value,
      };
      if (option.value !== '') {
        params.field.options.push(option);
      }
    });

    params.field.options.splice(params.acf_index, 1);

    if (params.field.value) {
      params.field.auto_values = [{
        field: params.field.value,
      }];
    }

    return params.field;
  }

  categoryOptions(params) {
    const { field } = params;
    flatMapDeep(field.field_groups).forEach((fieldGroup, i) => {
      fieldGroup.value = field.value[i]
    });
    return field;
  }

  sidebarOptions(params) {
    params.field.options = sidebar.navs.filter(nav => !nav.event)
      .map(nav => ({ value: nav.url, label: nav.label}))
      .concat(
        this.appStateService.getCollections()
          .map(col => {
            return {
              value: col.category,
              label: col.label,
            }
          })
      )
    return params.field;
  }

  toImage(value, params) {
    return `<img src="${value}" />`;
  }

  getFullName(value, params) {
    const { first_name, last_name } = params.record;
    if (first_name && last_name) {
      return `${first_name} ${last_name}`;
    }

    return value;
  }

  getRole(value) {
    return `${userRoles[value]} (${value})`;
  }

  getArticleUrl(value, params) {
    const { siteOptions, record } = params;
    const category = record.post_category || record.page_category || '';
    const baseUrl = !category ? '' : (siteOptions.website_url.localhost || '/')
    return `<a href="${baseUrl}${category}/${value}" target="_blank">${value}</a>`
  }

  getArticleCategory(value, params) {
    const { record } = params;
    return record.post_category || record.page_category;
  }
  
  createLink(value) {
    return `<a href="/cms/collections/all?category=${value}">${value}</a>`;
  }
}
