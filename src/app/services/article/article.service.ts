import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { DocumentRef } from '../../dom-abstraction/document.service';
import { hasKey, ucFirst, ucWords } from '../../helpers/string.helper';
import { RouteState } from '../../interfaces/route-state';
import { BackendService } from '../backend/backend.service';
import { SeoService } from '../seo/seo.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private renderer: Renderer2;

  constructor(
    private backendService: BackendService,
    private seoService: SeoService,
    private rendererFactory: RendererFactory2,
    private docRef: DocumentRef,
  ) {
    this.renderer = rendererFactory.createRenderer(docRef.nativeDocument, null);
  }

  initSEO(siteOptions, routeState: RouteState, article) {
    const { category, type, origin, href } = routeState;
    let { author, last_updated, published_date, title } = article;
    const pageTitle = type ? `${title} | ${category}` : title;
    if (hasKey(author)) {
      this.seoService.setPageAuthor(author);
    }
    console.log('routeState: ', routeState)
    this.setPageTitle(ucWords(pageTitle));
    this.seoService.setPageDescription(siteOptions.site_description || ucFirst(category));
    if (last_updated || published_date) {
      this.seoService.setContentDate(last_updated || published_date);
    }
    this.seoService.setPublisher(`${origin}/${category}`);
    this.seoService.setCanonicalUrl(href)
    this.setOG(article, siteOptions);
    this.seoService.addMeta([
      {
        name: 'robots',
        content: 'index',
      },
      {
        property: 'type',
        cotent: 'article'
      },
    ]);
  }

  setPageTitle(title) {
    this.seoService.setPageTitle(title);
  }

  setOG(article, siteOptions) {
    this.seoService.addMeta([
      {
        property: 'og:type',
        cotent: 'article'
      },
      {
        property: 'og:url',
        content: article.url,
      },
      {
        property: 'og:description',
        cotent: siteOptions.site_description,
      },
      {
        property: 'og:site_name',
        cotent: 'NGCC-CMS'
      }
    ])
  }

  fetchAuthor(id) {
    return this.backendService.get('users', { id }, { v2: true })
  }

  fetchArticle(url) {
    return this.backendService.getArticle(url);
  }

  fetchLatestArticles(category, max: number = 5) {
    return this.backendService.get(category, { length: max }, { v2: true });
  }

  search(q) {
    return this.backendService.getCustom('articles', { title: q, content: q, category: q })
  }

  createImage(src) {
    const img = this.renderer.createElement('img');
    img.src = src;
    return img.outerHTML.replace(/\\/gi, '');
  }

  imgToText(img) {
    const container = this.renderer.createElement('div');
    container.innerHTML = img;
    const image = container.querySelector('img');
    return image.src;
  }


}
