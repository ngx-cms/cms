import { Injectable } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { ApiResponse } from '../../../../shared/interfaces/api';
import { WindowRef } from '../../../dom-abstraction/window.service';
import { User } from '../../../interfaces/user/user';
import { BackendService } from '../../backend/backend.service';
import { StorageService } from '../../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {
  
  private user: User = {};
  private authUser: boolean;
  private session$ = new ReplaySubject<{}>(1);
  private window: Window;

  constructor(
    private backendService: BackendService,
    private storageService: StorageService,
    private winRef: WindowRef,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  isLoggedIn() {
    return this.authUser;
  }

  userInfo(info) {
    this.user = info;
    this.authUser = true;
    this.user.is_logged_in = true;
    console.log(`Logged in: `, info)
    this.storageService.setItem('user', this.window.btoa(JSON.stringify(this.user)));
    this.session$.next(info);
  }

  parseUser(user): User {
    try {
      return <User>JSON.parse(this.window.atob(user));
    } catch (e) {
      // console.log('Error parsing user: ', e);
    }
  }

  getUserSession(): User {
    let user = this.storageService.getItem('user');
    if (user) {
      return this.parseUser(user);
    }

    return null;
  }

  get userSession(): Observable<any> {
    return of(this.getUserSession()) || this.session$;
  }

  session(): Observable<User> {
    return this.session$;
  }

  login(creds?: { username?: string, password?: string}): Observable<ApiResponse> {
    return this.backendService.post(`user/login`, creds)
  }

  fetchUserInfo() {
    const user = this.parseUser(this.storageService.getItem('user'));
    if (user?.id) {
      return this.backendService.get(`users`, { id: user.id }, { v2: true });
    }

    return of({});
  }

  logout() {
    console.info(`Logging out`)
    this.storageService.removeItem('user');
  }
}
