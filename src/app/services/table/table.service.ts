import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { collectionConfigs } from '../../../shared/configs/configs';
import { ApiResponse } from '../../../shared/interfaces/api';
import { parseCollectionName, ucFirst } from '../../helpers/string.helper';
import { TableHeaders, TableHelpers } from '../../interfaces/table';
import { AppStateService } from '../app-state/app-state.service';
import { BackendService } from '../backend/backend.service';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(
    private backendService: BackendService,
    private appStateService: AppStateService,
  ) { }
  
  parseCollectionName(category: string | string[]): string {
    return parseCollectionName(category);
  }

  getCollectionData(collection, params): Observable<any> {
    return this.backendService.get(
      `collections/${collection}`,
      params,
    )
  }

  getCollectionConfig(collection) {
    return collectionConfigs[collection];
  }

  deleteCollectionRecord(collection, record: any): Observable<any> {
    console.log('record: ', record)
    const params = record.id ? { id: record.id } : { collection: record.collection }
    return this.backendService.post(`api/${this.parseCollectionName(collection)}/delete`, params);
  }

  updateForm(collection, form) {
    return this.backendService.post(`api/${this.parseCollectionName(collection)}/update`, { ...form })
  }

  fetchCollectionData() {
    const inputs: any = {};
    return this.appStateService.getRouteState()
      .pipe(
        switchMap(routeState => {
          inputs.collections = this.parseCollectionName(routeState.category);
          inputs.tableConfig = this.getCollectionConfig(inputs.collections)?.table;
          if (!inputs.tableConfig) {
            inputs.tableConfig.actions = {
              edit: {},
              delete: {},
            }
            console.error(`No table config for ${inputs.collections}`);
          }
          inputs.routeState = routeState;
          let params: {
            format?: boolean;
            id?: string;
            config?: {
              id: string;
            }
          } = {};
          // Get collection by id if it is edit
          if (routeState.slugs.includes('edit')) {
            inputs.edit.enabled = true;
            inputs.edit.collection = inputs.collections;
            params = { id: routeState.slugs[3] };
          }


          // table versioning
          if (!inputs.tableConfig?.version) {
            inputs.tableConfig.version = 1;
          }

          if (inputs.tableConfig?.version === 2) {
            const data = routeState.queryParams?.q;
            let parsedData: { data?: any } = {};
  
            if (data) {
              let isDataValid = false;
              try {
                parsedData = JSON.parse(atob(data));
                console.log('Parsed Data: ', parsedData)
                if (!!Object.keys(parsedData)?.length) {
                  isDataValid = true;
                }
              } catch (e) {
                console.error(`Invalid queryParam: `, {
                  queryParams: routeState.queryParams,
                  parsedParams: parsedData,
                });
              }
  
              // table version 2
              if (isDataValid) {
                return combineLatest(
                  of({ data: [parsedData.data] }),
                  of(inputs.tableConfig),
                );
              }
            }
          }

          return combineLatest(
            this.getCollectionData(inputs.collections, params),
            of(inputs.tableConfig),
          );
        }),
        // takeUntil(this.isDestroyed$),
        take(1),
      )
      .subscribe(([collection, config]: [ApiResponse, any]) => {
        const column = inputs.tableHelpers.findIndex(helper => helper.name === 'column');
        inputs.tableHelpers[column].options = inputs.tableConfig.columns;
        if (!collection?.data?.length) {
          inputs.table.message = collection.message;
          console.log(`Data not process`,
            { 
              collection: collection?.data,
              length: collection.data.length
            }
          )
          inputs.tableData = [];
          return;
        }

        if (!inputs.tableConfig?.columns?.length) {
          const newColumns = [];
          const { fields } = collection.data[0];
          fields.forEach(field => {
            if (newColumns.length < 2) {
              const col = Object.keys(field)[0];
              newColumns.push({
                key: col,
                label: this.formatTableColumn(col),
              });
            }
          });
          console.log(`No config found for: '${inputs.routeState.category}'. \nUsing `, newColumns);
          inputs.tableConfig.columns = newColumns;
        }

        this.processData(collection.data, config);
        inputs.edit.loaded = inputs.edit?.enabled;
        console.log('table: ', this)
        return inputs;
      }, err => { throw new Error(err) });
  }

  formatTableColumn(column) {
    const regex = /\W|_|-/g;
    return ucFirst(column?.replace(regex, ' '));
  }

  processData(collectionData, inputs) {
    if (!collectionData) {
      inputs.tableData = collectionData;
      return;
    }

    if (collectionData) {
      inputs.tableData = [];
      collectionData.forEach(collection => {
        if (collection?.fields && collection?.fields[0]) {
          if (inputs.routeState.type === 'edit') {
            inputs.tableData.push({...collection, data: collection.fields[0]});
          } else {
            inputs.tableData.push({
              id: collection?.config?.id,
              form_name: collection.form_name,
              ...collection.fields[0]
            });
          }
        }
      });
    }

    if (inputs.edit?.enabled && inputs.tableData) {
      inputs.edit.data = inputs.tableData[0];
    }

    inputs.showHelpers = true;
    return inputs;
  }

  filters(tableData: any[], headers: TableHeaders[], filters: TableHelpers[]) {
    const keyword = <string>(filters.find(helper => helper.name === 'search').value);
    if (keyword) {
      const newData = [];
      const caseSensitive = filters.find(helper => helper.name === 'case_sensitive')?.value || false;
      const column = <string>(filters.find(helper => helper.name === 'column')?.value);
      return tableData.filter(record => {
          for (const header of headers) {
            let rec = column ? record[column] : record[header.value];
            if (rec && typeof rec === 'string') {
              if (caseSensitive === false) {
                rec = rec.toLowerCase();
              }

              if (rec.includes(keyword)) {
                newData.push(record);
                return newData;
              }
            }
          }
      });
    }

    return tableData;
  }
}
