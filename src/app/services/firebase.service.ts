import { Injectable } from '@angular/core';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private firebaseConfig = {
    apiKey: "AIzaSyAJT_u0XzA0b3FJ0Om9DKZJaEOHHPUuR24",
    authDomain: "ngcc-cms.firebaseapp.com",
    projectId: "ngcc-cms",
    storageBucket: "ngcc-cms.appspot.com",
    messagingSenderId: "961744703570",
    appId: "1:961744703570:web:9f8c8ac60b4cb822710f8b",
    measurementId: "G-7S94JV1R0F"
  };
  

  constructor() { }

  init() {
    // Initialize Firebase
    // const app = initializeApp(this.firebaseConfig);
    // const analytics = getAnalytics(app);
  }
}
