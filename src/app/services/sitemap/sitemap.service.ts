import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Route, Router } from '@angular/router';
import { combineLatest, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { cloneDeep, keys, mergeArrayObjects, uniq } from '../../helpers/string.helper';
import { AppStateService } from '../app-state/app-state.service';
import { BackendService } from '../backend/backend.service';

@Injectable({
  providedIn: 'root'
})
export class SitemapService {

  private articleModule = import('../../pages/article-page/article-page.module').then(m => m.ArticlePageModule);
  private acfPageModule = import('../../pages/acf-page/acf-page.module').then(m => m.AcfPageModule);
  
  private STATE_KEY = makeStateKey('sitemap');
  constructor(
    private router: Router,
    private backendService: BackendService,
    private appStateService: AppStateService,
    private transferState: TransferState,
  ) {}

  public load() {
    return this.backendService.fetchSitemap()
      .pipe(
        switchMap(sitemap => (
          combineLatest(of(sitemap),
          this.backendService.get('collections', {}, { v2: true }))
        )),
        switchMap(([sitemap, collections]: [any, any[]]) => {
          collections = collections.map(col => ({
            category: col.key,
            url: `/cms/${col.key}`,
            label: col.name,
            icon: col.icon,
            base_url: 'collections'
          }));
          this.transferState.set(this.STATE_KEY, <any>sitemap);
          return this.createRoutes(sitemap, collections);
        })
    );
  }

  // private createRoutes(sitemap: string[]) {
  private createRoutes(sitemap, collections) {
    let defaultRoutes = cloneDeep(this.router.config);
    const wildcardIdx = defaultRoutes.findIndex(route => route.path === '**');
    // const categories = uniq(routes.map((route: any) => route.url.split('/').filter(r => r)[0]));
    // this.appStateService.setArticleCategories(categories);
    this.appStateService.setSitemap(sitemap);
    this.appStateService.setCollections(collections);
    return this.getUniqRoutes(sitemap, collections).map((route, i) => {
      defaultRoutes.splice(wildcardIdx, 0, route);
      return defaultRoutes;
    }).map((route, i) => this.router.resetConfig(route));
  }

  private getUniqRoutes(sitemap, collections) {
    const wildcards = [];
    const newRoutes: Route[] = [];
    for (let category in sitemap) {
      sitemap[category].forEach(route => {
        if (!wildcards.includes(category)) {
          wildcards.push(category);
          newRoutes.push({
            path: `${route.category}/:slug`,
            loadChildren: () => this.articleModule,
          });
        }
        
        if (route.type === 'pages') {
          newRoutes.push({
            path: route.url,
            loadChildren: () => this.articleModule,
          });
        }
      })
    }

    collections.forEach(col => {
      this.router.config[7].children.push({
        path: col.url,
        loadChildren: () => this.acfPageModule,
      })
      // console.log('router: ', cloneDeep())
      // cloneDeep(this.router.config).findIndex(r => r.path === '**');
      // newRoutes.push()
    });
    return newRoutes;
  }
}
