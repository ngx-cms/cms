import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ElementService } from '../../components/services/element.service';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    private titleService: Title,
    private elementService: ElementService,
  ) { }

  attrs(tags) {
    const attrs = [];
    for (const key in tags) {
      attrs.push({ name: key, value: tags[key] })
    }
    return attrs;
  }

  addMeta(tags: any) {
    if (Array.isArray(tags)) {
      tags.forEach(tag => {
        this.elementService.addElement('meta', this.attrs(tag))
      })
    } else {
      this.elementService.addElement('meta', this.attrs(tags))
    }
  }

  setPageTitle(title) {
    this.titleService.setTitle(title);
  }

  setPageDescription(description) {
    this.addMeta({ name: 'description', content: description });
  }

  setPageAuthor(author) {
    if (author) {
      this.addMeta({ name: 'author', content: author.name });
    }
  }

  setCanonicalUrl(url) {
    this.elementService.addElement('link',this.attrs({
      rel: 'canonical',
      href: url,
    }));
  }

  setContentDate(article) {
    this.addMeta({
      property: 'article:published_time',
      content: `${article.last_updated || article.published_date}`,
    });
  }

  setPublisher(publisher) {
    this.addMeta({
      name: 'publisher',
      content: publisher,
    });
  }
}
