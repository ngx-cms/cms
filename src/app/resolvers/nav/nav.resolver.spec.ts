import { TestBed } from '@angular/core/testing';

import { NavResolver } from './nav.resolver';

describe('NavResolver', () => {
  let resolver: NavResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(NavResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
