import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WindowRef } from '../../../dom-abstraction/window.service';
import { User } from '../../../interfaces/user/user';
import { UserAuthService } from '../../../services/user/user-auth/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteStateResolver implements Resolve<boolean> {

  private window: Window;
  private isDestroyed$ = new Subject<void>();
  user: User;

  constructor(
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
     this.userAuthService
      .userSession
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe(user => {
        if (!user) {
          this.window.location.href = '/login';
          // this.window.location.href;
        }

        this.user = user;
      });

    return of({
      user: {
        authenticate: true,
      },
      page_not_found: route.routeConfig.path === '**',
    });
  }
}
