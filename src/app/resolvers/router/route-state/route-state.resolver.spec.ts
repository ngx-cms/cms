import { TestBed } from '@angular/core/testing';

import { RouteStateResolver } from './route-state.resolver';

describe('RouteStateResolver', () => {
  let resolver: RouteStateResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(RouteStateResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
