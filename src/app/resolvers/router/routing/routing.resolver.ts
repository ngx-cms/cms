import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WindowRef } from '../../../dom-abstraction/window.service';
import { RoutingService } from '../../../services/routing/routing.service';
import { UserAuthService } from '../../../services/user/user-auth/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoutingResolver implements Resolve<any> {

  window: Window;
  private isDestroyed$ = new Subject<void>();
  constructor(
    private userAuthService: UserAuthService,
    private winRef: WindowRef,
    private routingService: RoutingService,
  ) {
    this.window = this.winRef.nativeWindow;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    if (state.url.startsWith('/login')) {
      this.userAuthService.userSession
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe(user => {
          console.log('user: ', user)
          route.data = user;
          if (user && !!Object.keys(user).length) {
            this.routingService.goto('/cms')
          }
        })
    }

    if (state.url.indexOf('logout') !== -1) {
      this.userAuthService.logout();

      // Return observable
      return this.userAuthService.userSession;
    }

    return of({});
  }
}
