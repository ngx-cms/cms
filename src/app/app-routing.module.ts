import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, FactoryProvider, NgModule } from '@angular/core';
import { Routes, Router, RouterModule, ROUTES } from '@angular/router';
import { LogoutComponent } from './components/logout/logout.component';
import { ArticleMatcher } from './matchers/article-page.matcher';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { RoutingResolver } from './resolvers/router/routing/routing.resolver';
import { AppStateService } from './services/app-state/app-state.service';
import { SitemapService } from './services/sitemap/sitemap.service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'cms',
    loadChildren: () => import('./pages/cms/base/base.module').then(m => m.BaseModule),
  },
  {
    path: 'login',
    loadChildren:() => import('./pages/login-page/login-page.module').then(m => m.LoginPageModule),
  },
  {
    path: 'articles',
    loadChildren:() => import('./pages/article-page/article-page.module').then(m => m.ArticlePageModule),
  },
  {
    path: 'authors',
    loadChildren: () => import('./pages/user-profile-page/user-profile-page.module').then(m => m.UserProfilePageModule),
  },
  {
    path: 'logout',
    component: LogoutComponent,
  },

  // TODO: Temporary, needs refactor and other method of doing this (maybe using a matcher)
  {
    path: '**',
    component: NotFoundComponent,
    // resolve: {
    //   res: RoutingResolver,
    // },
  }
];

export function routeLoader(router: Router, sitemap: SitemapService) {
  const dynamicRoutes = sitemap.load().toPromise();
  console.log('router: ', router)
  return () => dynamicRoutes;
}

/**
 * Explicit Factory Provider for DynamicRouteFactory
 */
export const dynamicRouteFactoryProvider: FactoryProvider = {
  provide: APP_INITIALIZER,
  useFactory: routeLoader,
  deps: [Router, SitemapService],
  multi: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'disabled'
})],
  exports: [RouterModule],
  providers: [dynamicRouteFactoryProvider],
})
export class AppRoutingModule { }
