import { Injectable } from '@angular/core';


function _document(): any {
  if (typeof document !== 'undefined') {
    return document;
  } else {
    return {
      createElement: () => null,
      documentElement: { clientWidth: '' },
      getElementById: () => null,
      querySelector: () => null,
      querySelectorAll: () => null,
    };
  }
}

@Injectable({
  providedIn: 'root'
})
export class DocumentRef {
  get nativeDocument(): any {
    return _document();
  }
}
