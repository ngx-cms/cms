import { Injectable } from '@angular/core';


function _window(): any {
  if (typeof window !== 'undefined') {
    return window;
  } else {
    return {
      firebase: { apps: {} },
      scrollTo: () => {},
      scrollIntoView: () => {},
      open: () => {},
      location: { host: '', href: '', pathname: '', hash: '' },
      screen: { height: 0, width: 0},
      localStorage: {
        getItem: () => '',
        setItem: () => {},
        removeItem: () => {},
      },
      sessionStorage: {
        getItem: () => '',
        setItem: () => {},
        removeItem: () => {},
      },
      navigator: {
        userAgent: {
          search: () => {}
        }
      },
      innerWidth: 0
    };
  }
}


@Injectable({
  providedIn: 'root'
})
export class WindowRef {
  get nativeWindow(): any {
    return _window();
  }
}

