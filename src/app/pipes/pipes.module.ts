import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from './safe/safe.pipe';
import { StringPipe } from './string/string.pipe';



@NgModule({
  declarations: [
    SafePipe,
    StringPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SafePipe,
    StringPipe,
  ]
})
export class PipesModule { }

export {
  SafePipe,
};