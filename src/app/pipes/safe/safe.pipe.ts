import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe',
})
export class SafePipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer) {}

  transform(content: string, type: string): any {
    if (content == null) {
      return '';
    }

    switch (type) {
      case 'url':
        return this.domSanitizer.bypassSecurityTrustResourceUrl(content);
      case 'html':
        return this.domSanitizer.bypassSecurityTrustHtml(content);
      case 'css':
        return this.domSanitizer.bypassSecurityTrustStyle(content);
      case 'js':
        return this.domSanitizer.bypassSecurityTrustScript(content);
      case 'url':
        return this.domSanitizer.bypassSecurityTrustUrl(content);
      case 'res':
        return this.domSanitizer.bypassSecurityTrustResourceUrl(content);
      default:
        console.log('Invalid type passed into SafePipe');
        return content;
    }
  }
}
