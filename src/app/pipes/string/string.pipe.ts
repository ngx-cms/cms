import { Pipe, PipeTransform } from '@angular/core';
import { ucWords, formatCategory } from '../../helpers/string.helper';

@Pipe({
  name: 'string'
})
export class StringPipe implements PipeTransform {

  transform(value: any, type: any): any {
    switch (type) {
      case 'ucwords':
        return ucWords(value);
      case 'category':
        return formatCategory(value);
      default:
    }
    return value;
  }

}
