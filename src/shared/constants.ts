import { SidebarNavs } from "../app/interfaces/sidebar";

export const userRoles = {
  '1': 'Super Admin',
  '2': 'Admin',
  '3': 'Editor',
  '4': 'Viewer',
};

export const FOOTER = {
  sitemap: [
    {
      label: 'Homepage',
      value: ''
    },
    {
      label: 'Articles',
      value: 'articles'
    },
    {
      label: 'Documentation',
      value: 'documentation'
    },
    {
      label: 'About us',
      value: 'about-us'
    }
  ]
}

export const dataTypes = [
  {
    value: 'string',
    label: 'String',
  },
  {
    value: 'number',
    label: 'Number',
  },
  {
    value: 'boolean',
    label: 'Boolean',
  },
  {
    value: 'array',
    label: 'Array',
  },
  {
    value: 'object',
    label: 'Object',
  },
]

export const navbar: {
  userMenu: SidebarNavs[];
} = {
  userMenu: [
    {
      label: 'Account',
      icon: ``,
      url: '/account'
    },
    {
      label: 'Logout',
      url: '/logout'
    }
  ]
};

export const sidebar: {
  navs: SidebarNavs[];
  options: object;
} = {
  navs: [
    // {
    //   label: 'Hide Menu',
    //   icon: `<i class="fas fa-arrow-alt-circle-left"></i>`,
    //   event: {
    //     type: 'click',
    //     function: 'toggleMenu',
    //     params: { label: 'Show Menu' },
    //     value: true,
    //   }
    // },
    // {
    //   label: 'Show Menu',
    //   icon: `<i class="fas fa-arrow-alt-circle-right"></i>`,
    //   event: {
    //     type: 'click',
    //     function: 'toggleMenu',
    //     params: { label: 'Hide Menu' },
    //     value: false,
    //   },
    //   is_hidden: true,
    // },
    {
      label: 'Dashboard',
      url: 'dashboard',
      icon: `<i class="fas fa-tachometer-alt"></i>`,
    },
    {
      label: 'Posts',
      url: 'posts',
      icon: `<i class="fas fa-newspaper"></i>`,
      children: true,
    },
    {
      label: 'Pages',
      url: 'pages',
      icon: `<i class="fas fa-file"></i>`,
      children: true,
    },
    {
      label: 'Users',
      url: 'users',
      icon: `<i class="fas fa-users"></i>`,
      children: [
        {
          label: 'Profile',
          url: '/cms/user/',
          // icon: '<i class="fas fa-wrench"></i>'
        }
      ],
      options: {
        children: {
          default: true,
        }
      }
    },
    {
      label: 'ACF',
      url: 'acf',
      icon: `<i class="fas fa-columns"></i>`,
      children: true,
    },
    {
      label: 'Collections',
      url: 'collections',
      icon: '<i class="fas fa-database"></i>',
      children: true,
    },
    {
      label: 'Site Options',
      url: 'site-options',
      clickable: true,
      icon: `<i class="fas fa-cogs"></i>`,
    },
  ],
  options: {
    navs: '',
  }
};

export const dbConfig = {
  username: 'admin',
  password: 'ibsuccess',
  db_name: 'ibsuccess',
};

export const mailer = {
  email: 'ibsuccess.website@gmail.com',
  password: 'ibsuccess!123',
  service: 'gmail',
}