export interface ApiResponse {
  code?: string | number;
  [data: string]: any;
  message?: string;
  status?: string;
}
