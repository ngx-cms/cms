import { CollectionConfig } from "src/app/interfaces/collection-config";

export const collectionConfigs: CollectionConfig = {
  customfields: {
    table: {
      data: 'fields',
      // columns: [
      //   {
      //     label: 'ID',
      //     key: 'id',
      //   },
      //   {
      //     label: 'Form name',
      //     key: 'form_name',
      //   },
      //   {
      //     label: 'Category',
      //     key: 'category',
      //     type: 'array',
      //   }
      // ],
      actions: {
        edit: {
          // enabled: false,
          load_template: false,
        }
      }
    },
  },
  pages: {
    table: {
      data: 'fields',
      columns: [
        {
          label: 'Page url',
          key: 'url',
        }
      ],
      actions: {
        edit: {
          // enabled: false,
        }
      },
      version: 1,
    }
  },
  posts: {
    table: {
      data: 'fields',
      // columns: [
      //   {
      //     label: 'Url',
      //     key: 'nullanimals',
      //   }
      // ],
      actions: {
        edit: {
          // enabled: false,
        }
      },
      version: 1,
    }
  },
  siteoptions: {
    table: {
      data: 'fields',
      columns: [
        {
          label: 'Table Version',
          key: 'table_version',
        }
      ],
      actions: {
        edit: {
          // enabled: false,
        }
      },
      version: 1,
    }
  },
  users: {
    table: {
      data: 'fields',
      // columns: [
      //   {
      //     label: 'Username',
      //     key: 'username',
      //   }
      // ],
      actions: {
        edit: {
          
        }
      }
    }
  },
  navs: {
    table: {
      data: 'fields',
      columns: [
        {
          label: 'Label',
          key: 'label',
        },
        {
          label: 'Url',
          key: 'url',
        }
      ],
      actions: {
        edit: {

        }
      },
    }
  }
}


export const apiVersion = 1;