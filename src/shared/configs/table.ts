export const templatedCategories = [
  'pages',
  'posts',
  'users',
];

export const recordsActions = [
  {
    key: 'edit',
    classes: 'btn-info btn-sm',
    icon: 'pencil-alt',
    fn: 'editData',
    condition: {
      role: 2,
    }
  },
  {
    key: 'delete',
    classes: 'btn-danger btn-sm ml-1',
    icon: 'trash',
    fn: 'deleteData',
    condition: {
      role: 1,
    }
  }
];