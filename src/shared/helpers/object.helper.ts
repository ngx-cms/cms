import { isArray } from "../../app/helpers/string.helper";

export function sort(data, key, order) {
  return data.sort((a, b) => {
    return order === 'ascending'
      ? a[key] > b[key] ? 1 : -1
      : a[key] < b[key] ? 1 : -1;
  });
}

export function detectMutation(data): any {
  if (isArray(data)) {
    return data.map(rec => rec);
  }

  return data;
}