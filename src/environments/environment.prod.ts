export const environment = {
  production: true,
  ssr: true,
  api: 'https://ngcc-cms-api.herokuapp.com/',
};
